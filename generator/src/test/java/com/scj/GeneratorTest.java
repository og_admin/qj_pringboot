package com.scj;

import com.scj.dao.SysGeneratorDao;
import com.scj.utils.GenUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeneratorApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class GeneratorTest {
    @Autowired
    private SysGeneratorDao sysGeneratorDao;

    @Test
    public void GeneratorCode() {
        String tableName = "wx_mp_user";
        //查询表信息
        Map<String, String> table = sysGeneratorDao.queryTable(tableName);
        //查询列信息
        List<Map<String, String>> columns = sysGeneratorDao.queryColumns(tableName);
        //生成路径
        GenUtils.generatorCode(table, columns, "F:/projects/build");
    }
}
