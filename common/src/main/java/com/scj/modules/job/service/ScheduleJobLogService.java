/**
 * Copyright 2018 人人开源 http://www.guoruiinfo.com
 */

package com.scj.modules.job.service;


import com.baomidou.mybatisplus.service.IService;
import com.scj.common.utils.PageUtils;
import com.scj.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author runnable@sina.cn
 * @since 1.2.0 2016-11-28
 */
public interface ScheduleJobLogService  extends IService<ScheduleJobLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}
