package com.scj.modules.job.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.scj.modules.job.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 * @author runnable@sina.cn
 */
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
}
