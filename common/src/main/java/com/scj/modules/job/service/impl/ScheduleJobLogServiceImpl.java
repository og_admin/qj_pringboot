///**
// * Copyright 2018 人人开源 http://www.guoruiinfo.com
// */
//
//package com.scj.modules.job.service.impl;
//
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//import com.baomidou.mybatisplus.plugins.Page;
//import com.baomidou.mybatisplus.service.impl.ServiceImpl;
//import com.scj.common.utils.PageUtils;
//import com.scj.common.utils.Query;
//import com.scj.common.utils.TryParse;
//import com.scj.modules.job.dao.ScheduleJobLogDao;
//import com.scj.modules.job.entity.ScheduleJobLogEntity;
//import com.scj.modules.job.service.ScheduleJobLogService;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.stereotype.Service;
//
//import java.util.Map;
//
///**
// * @author runnable@sina.cn
// */
//@Service("scheduleJobLogService")
//public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogDao, ScheduleJobLogEntity> implements ScheduleJobLogService {
//
//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//        String jobId = TryParse.parseString(params.get("jobId"));
//        Page<ScheduleJobLogEntity> page = this.selectPage(
//                new Query<ScheduleJobLogEntity>(params).getPage(),
//                new EntityWrapper<ScheduleJobLogEntity>().like(StringUtils.isNotBlank(jobId),"job_id", jobId)
//        );
//        return null;
//    }
//}
