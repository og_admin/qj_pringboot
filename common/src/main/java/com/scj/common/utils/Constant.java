package com.scj.common.utils;

/**
 * <pre>
 * </pre>
 */
public class Constant {
    /**
     * 超级管理员ID
     */
    public static final int SUPER_ADMIN = 1;

    /**
     * 数据权限过滤
     */
    public static final String SQL_FILTER = "sql_filter";
    /**
     *
     */
    public static class Job{
        // 停止计划任务
        public static String STATUS_RUNNING_STOP = "stop";
        // 开启计划任务
        public static String STATUS_RUNNING_START = "start";
        
    } 
    
    /**
     *
     */
    public static class Generator{
        // 自动去除表前缀
        public static String AUTO_REOMVE_PRE = "true";
        
    } 
    
    public static class Sys{
        // 部门根节点id
        public static Long DEPT_ROOT_ID = 0L;
    }

    /**
     * 定时任务状态
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
        NORMAL(0),
        /**
         * 暂停
         */
        PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
