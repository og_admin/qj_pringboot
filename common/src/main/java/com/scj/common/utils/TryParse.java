package com.scj.common.utils;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * @author runnable@sina.cn
 */
public final class TryParse {

    public static int parseInt(Object value) {
        return parseInt(value, 0);
    }

    public static int parseInt(Object value, int defaultValue) {
        try {
            return null == value ? defaultValue : Integer.parseInt(value.toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static BigDecimal parseBigDecimal(Object value) {
        return parseBigDecimal(value, BigDecimal.ZERO);
    }

    public static BigDecimal parseBigDecimal(Object value, BigDecimal defaultValue) {
        try {
            return null == value ? defaultValue : BigDecimal.ZERO;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static String parseString(Object value) {
        return parseString(value, "");
    }

    public static String parseString(Object value, String defaultValue) {
        try {
            return null == value ? defaultValue : value.toString();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static boolean isNotBlank(Object value) {
        return StringUtils.isNotBlank(TryParse.parseString(value, ""));
    }
}
