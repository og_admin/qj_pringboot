
package com.scj.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author Runnable
 * @email runnable@sina.cn
 * @date 2016年10月27日 下午9:59:27
 */
public class R extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    public R() {
        put("code", ResultCode.SUCCESS.getCode());
        put("msg", ResultCode.SUCCESS.getDesc());
    }

    public static R error() {
        return error(ResultCode.ERROR.getCode(), ResultCode.ERROR.getDesc());
    }

    public static R error(String msg) {
        return error(ResultCode.ERROR.getCode(), msg);
    }

    public static R error(Integer code, String msg) {
        R r = new R();
        r.put("code", code);
        if (msg != null && !msg.equals("")) {
            r.put("msg", msg);
        } else {
            r.put("msg", ResultCode.ERROR.getDesc());
        }
        return r;
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }


    public static R all(String message) {
        if (StringUtils.isBlank(message)) {
            return R.ok();
        }
        return R.error(message);
    }

    public static R all(boolean flag) {
        if (flag) {
            return R.ok();
        }
        return R.error("出现异常错误");
    }

    @Override
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
