package com.scj.common.utils;

import lombok.Getter;

/**
 * @author runnable@sina.cn
 */

@Getter
public enum ResultCode {
    SUCCESS(0, "访问成功"),
    OTHER(250, "其他异常"),
    ERROR(500, "未知异常，请联系管理员"),
    TOKEN(1000, "token失效，请重新登录"),
    TYPE(1500, "类型丢失"),
    POWER(403, "没有权限"),
    IMSI(2500, "IMSI手机串号异常");
    private Integer code;

    private String desc;

    private ResultCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
