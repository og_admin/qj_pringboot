package com.scj.common.validator;

import com.scj.common.exception.RRException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Map;
import java.util.Set;

/**
 * hibernate-validator校验工具类
 * 参考文档：http://docs.jboss.org/hibernate/validator/5.4/reference/en-US/html_single/
 * @author runnable@sina.cn
 */
public class ValidatorUtils {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    /**
     * 校验对象
     * @param object        待校验对象
     * @param groups        待校验的组
     * @throws RRException  校验不通过，则报RRException异常
     */
    public static void validateEntity(Object object, Class<?>... groups)
            throws RRException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            ConstraintViolation<Object> constraint = (ConstraintViolation<Object>)constraintViolations.iterator().next();
            throw new RRException(constraint.getMessage());
        }
    }

    public static void validateParams(Map<String, Object> params, String[] filterNames) {
        for (String name : filterNames) {
            if (!params.containsKey(name)) {
                throw new RRException("参数丢失，请联系管理员");
            }
        }
    }

    /**
     * 是否为空串或0
     */
    public static boolean isEmpty(Long value) {
        return null == value || value.equals(0L);
    }

    /**
     * 是否为空串或0
     */
    public static boolean isNull(String value) {
        return null == value;
    }
    /**
     * 是否为空串或0
     */
    public static boolean isNull(Integer value) {
        return null == value;
    }
    /**
     * 是否为空串或0
     */
    public static boolean isNull(Long value) {
        return null == value;
    }
    /**
     * 是否为空串或0
     */
    public static boolean isEmpty(Integer value) {
        return null == value || value.equals(0);
    }
}
