/**
 * Copyright 2018 人人开源 http://www.guoruiinfo.com
 */

package com.scj.common.validator.group;

/**
 * 新增数据 Group
 * @author runnable@sina.cn
 * @email runnable@sina.cn
 * @date 2017-03-16 0:04
 */
public interface AddGroup {
}
