/*
Navicat MySQL Data Transfer

Source Server         : 公司腾讯云测试数据库
Source Server Version : 50629
Source Host           : 119.29.177.249:3306
Source Database       : scj

Target Server Type    : MYSQL
Target Server Version : 50629
File Encoding         : 65001

Date: 2018-08-19 11:49:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_user`
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `realName` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `lastLoginTime` datetime DEFAULT NULL,
  `uname` varchar(20) DEFAULT NULL,
  `passwd` varchar(100) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', '孤傲苍狼', '13822290843', '2018-04-01 21:59:50', '2018-05-02 21:06:51', 'gacl', 'xdp', 'oy03r0BhZIURZGTNfHZfwijCXkYQ');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `k` varchar(100) DEFAULT NULL COMMENT '键',
  `v` varchar(1000) DEFAULT NULL COMMENT '值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `kvType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('2', 'oss_qiniu', '{\"AccessKey\" : \"8-HMj9EgGNIP-xuOCpSzTn-OMyGOFtR3TxLdn4Uu\",\"SecretKey\" : \"SjpGg3V6PsMdJgn42PeEd5Ik-6aNyuwdqV5CPM6A\",\"bucket\" : \"ifast\",\"AccessUrl\" : \"http://p6r7ke2jc.bkt.clouddn.com/\"}', '七牛对象存储配置', '2018-04-06 14:31:26', '4300');
INSERT INTO `sys_config` VALUES ('3', 'author', 'gacl', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('4', 'email', '290603672@qq.com', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('5', 'package', 'com.scj', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('6', 'autoRemovePre', 'true', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('7', 'tablePrefix', '', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('8', 'tinyint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('9', 'smallint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('10', 'mediumint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('11', 'int', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('12', 'integer', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('13', 'bigint', 'Long', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('14', 'float', 'Float', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('15', 'double', 'Double', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('16', 'decimal', 'BigDecimal', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('17', 'bit', 'Boolean', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('18', 'char', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('19', 'varchar', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('20', 'tinytext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('21', 'text', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('22', 'mediumtext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('23', 'longtext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('24', 'date', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('25', 'datetime', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('26', 'timestamp', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentId` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `delFlag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('6', '0', '研发部', '1', '1');
INSERT INTO `sys_dept` VALUES ('7', '6', '研发一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('8', '6', '研发二部', '2', '1');
INSERT INTO `sys_dept` VALUES ('9', '0', '销售部', '2', '1');
INSERT INTO `sys_dept` VALUES ('11', '0', '产品部', '3', '1');
INSERT INTO `sys_dept` VALUES ('12', '11', '产品一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('13', '0', '测试部', '5', '1');
INSERT INTO `sys_dept` VALUES ('14', '13', '测试一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('15', '13', '测试二部', '2', '1');
INSERT INTO `sys_dept` VALUES ('16', '9', '销售一部', '0', '1');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序（升序）',
  `parentId` bigint(64) DEFAULT '0' COMMENT '父级编号',
  `createBy` int(64) DEFAULT NULL COMMENT '创建者',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(64) DEFAULT NULL COMMENT '更新者',
  `updateDate` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `delFlag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`),
  KEY `sys_dict_label` (`name`),
  KEY `sys_dict_del_flag` (`delFlag`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '正常', '0', 'del_flag', '删除标记', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('3', '显示', '1', 'show_hide', '显示/隐藏', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('4', '隐藏', '0', 'show_hide', '显示/隐藏', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('5', '是', '1', 'yes_no', '是/否', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('6', '否', '0', 'yes_no', '是/否', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('7', '红色', 'red', 'color', '颜色值', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('8', '绿色', 'green', 'color', '颜色值', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('9', '蓝色', 'blue', 'color', '颜色值', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('10', '黄色', 'yellow', 'color', '颜色值', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('11', '橙色', 'orange', 'color', '颜色值', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('12', '默认主题', 'default', 'theme', '主题方案', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('13', '天蓝主题', 'cerulean', 'theme', '主题方案', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('14', '橙色主题', 'readable', 'theme', '主题方案', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('15', '红色主题', 'united', 'theme', '主题方案', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('16', 'Flat主题', 'flat', 'theme', '主题方案', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('17', '国家', '1', 'sys_area_type', '区域类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('18', '省份、直辖市', '2', 'sys_area_type', '区域类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('19', '地市', '3', 'sys_area_type', '区域类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('20', '区县', '4', 'sys_area_type', '区域类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('21', '公司', '1', 'sys_office_type', '机构类型', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('22', '部门', '2', 'sys_office_type', '机构类型', '70', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('23', '小组', '3', 'sys_office_type', '机构类型', '80', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('24', '其它', '4', 'sys_office_type', '机构类型', '90', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('25', '综合部', '1', 'sys_office_common', '快捷通用部门', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('26', '开发部', '2', 'sys_office_common', '快捷通用部门', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('27', '人力部', '3', 'sys_office_common', '快捷通用部门', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('28', '一级', '1', 'sys_office_grade', '机构等级', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('29', '二级', '2', 'sys_office_grade', '机构等级', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('30', '三级', '3', 'sys_office_grade', '机构等级', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('31', '四级', '4', 'sys_office_grade', '机构等级', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('32', '所有数据', '1', 'sys_data_scope', '数据范围', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('33', '所在公司及以下数据', '2', 'sys_data_scope', '数据范围', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('34', '所在公司数据', '3', 'sys_data_scope', '数据范围', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('35', '所在部门及以下数据', '4', 'sys_data_scope', '数据范围', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('36', '所在部门数据', '5', 'sys_data_scope', '数据范围', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('37', '仅本人数据', '8', 'sys_data_scope', '数据范围', '90', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('38', '按明细设置', '9', 'sys_data_scope', '数据范围', '100', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('39', '系统管理', '1', 'sys_user_type', '用户类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('40', '部门经理', '2', 'sys_user_type', '用户类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('41', '普通用户', '3', 'sys_user_type', '用户类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('42', '基础主题', 'basic', 'cms_theme', '站点主题', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('43', '蓝色主题', 'blue', 'cms_theme', '站点主题', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('44', '红色主题', 'red', 'cms_theme', '站点主题', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('45', '文章模型', 'article', 'cms_module', '栏目模型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('46', '图片模型', 'picture', 'cms_module', '栏目模型', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('47', '下载模型', 'download', 'cms_module', '栏目模型', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('48', '链接模型', 'link', 'cms_module', '栏目模型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('49', '专题模型', 'special', 'cms_module', '栏目模型', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('50', '默认展现方式', '0', 'cms_show_modes', '展现方式', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('51', '首栏目内容列表', '1', 'cms_show_modes', '展现方式', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('52', '栏目第一条内容', '2', 'cms_show_modes', '展现方式', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('53', '发布', '0', 'cms_del_flag', '内容状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('54', '删除', '1', 'cms_del_flag', '内容状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('55', '审核', '2', 'cms_del_flag', '内容状态', '15', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('56', '首页焦点图', '1', 'cms_posid', '推荐位', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('57', '栏目页文章推荐', '2', 'cms_posid', '推荐位', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('58', '咨询', '1', 'cms_guestbook', '留言板分类', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('59', '建议', '2', 'cms_guestbook', '留言板分类', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('60', '投诉', '3', 'cms_guestbook', '留言板分类', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('61', '其它', '4', 'cms_guestbook', '留言板分类', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('62', '公休', '1', 'oa_leave_type', '请假类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('63', '病假', '2', 'oa_leave_type', '请假类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('64', '事假', '3', 'oa_leave_type', '请假类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('65', '调休', '4', 'oa_leave_type', '请假类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('66', '婚假', '5', 'oa_leave_type', '请假类型', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('67', '接入日志', '1', 'sys_log_type', '日志类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('68', '异常日志', '2', 'sys_log_type', '日志类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('69', '请假流程', 'leave', 'act_type', '流程类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('70', '审批测试流程', 'test_audit', 'act_type', '流程类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('71', '分类1', '1', 'act_category', '流程分类', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('72', '分类2', '2', 'act_category', '流程分类', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('73', '增删改查', 'crud', 'gen_category', '代码生成分类', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('74', '增删改查（包含从表）', 'crud_many', 'gen_category', '代码生成分类', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('75', '树结构', 'tree', 'gen_category', '代码生成分类', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('76', '=', '=', 'gen_query_type', '查询方式', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('77', '!=', '!=', 'gen_query_type', '查询方式', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('78', '&gt;', '&gt;', 'gen_query_type', '查询方式', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('79', '&lt;', '&lt;', 'gen_query_type', '查询方式', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('80', 'Between', 'between', 'gen_query_type', '查询方式', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('81', 'Like', 'like', 'gen_query_type', '查询方式', '60', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('82', 'Left Like', 'left_like', 'gen_query_type', '查询方式', '70', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('83', 'Right Like', 'right_like', 'gen_query_type', '查询方式', '80', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('84', '文本框', 'input', 'gen_show_type', '字段生成方案', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('85', '文本域', 'textarea', 'gen_show_type', '字段生成方案', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('86', '下拉框', 'select', 'gen_show_type', '字段生成方案', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('87', '复选框', 'checkbox', 'gen_show_type', '字段生成方案', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('88', '单选框', 'radiobox', 'gen_show_type', '字段生成方案', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('89', '日期选择', 'dateselect', 'gen_show_type', '字段生成方案', '60', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('90', '人员选择', 'userselect', 'gen_show_type', '字段生成方案', '70', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('91', '部门选择', 'officeselect', 'gen_show_type', '字段生成方案', '80', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('92', '区域选择', 'areaselect', 'gen_show_type', '字段生成方案', '90', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('93', 'String', 'String', 'gen_java_type', 'Java类型', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('94', 'Long', 'Long', 'gen_java_type', 'Java类型', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('95', '仅持久层', 'dao', 'gen_category', '代码生成分类', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('96', '男', '1', 'sex', '性别', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('97', '女', '2', 'sex', '性别', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('98', 'Integer', 'Integer', 'gen_java_type', 'Java类型', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('99', 'Double', 'Double', 'gen_java_type', 'Java类型', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('100', 'Date', 'java.util.Date', 'gen_java_type', 'Java类型', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('104', 'Custom', 'Custom', 'gen_java_type', 'Java类型', '90', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('105', '会议通告', '1', 'oa_notify_type', '通知通告类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('106', '奖惩通告', '2', 'oa_notify_type', '通知通告类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('107', '活动通告', '3', 'oa_notify_type', '通知通告类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('108', '草稿', '0', 'oa_notify_status', '通知通告状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('109', '发布', '1', 'oa_notify_status', '通知通告状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('110', '未读', '0', 'oa_notify_read', '通知通告状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('111', '已读', '1', 'oa_notify_read', '通知通告状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('112', '草稿', '0', 'oa_notify_status', '通知通告状态', '10', '0', '1', null, '1', null, '', '0');
INSERT INTO `sys_dict` VALUES ('113', '删除', '0', 'del_flag', '删除标记', null, null, null, null, null, null, '', '');
INSERT INTO `sys_dict` VALUES ('118', '关于', 'about', 'blog_type', '博客类型', null, null, null, null, null, null, '全url是:/blog/open/page/about', '');
INSERT INTO `sys_dict` VALUES ('119', '交流', 'communication', 'blog_type', '博客类型', null, null, null, null, null, null, '', '');
INSERT INTO `sys_dict` VALUES ('120', '文章', 'article', 'blog_type', '博客类型', null, null, null, null, null, null, '', '');

-- ----------------------------
-- Table structure for `sys_file`
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '文件类型',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030696876011257858 DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('140', '0', 'http://p6r7ke2jc.bkt.clouddn.com/ifast/20180406/cat003.jpeg', '2018-04-06 17:58:03');
INSERT INTO `sys_file` VALUES ('141', '0', 'http://p6r7ke2jc.bkt.clouddn.com/ifast/20180406/cat002-1523009188140.jpeg', '2018-04-06 18:06:28');
INSERT INTO `sys_file` VALUES ('148', '0', 'http://p6r7ke2jc.bkt.clouddn.com/ifast/20180406/cd09920f-7d51-4c60-a3a1-c36c83b3dfb4-1523028484072.png', '2018-04-06 23:28:05');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030986111205257219 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1030598360316366850', '1', 'admin', '登录', '49', 'com.ifast.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 07:32:51');
INSERT INTO `sys_log` VALUES ('1030598361448828930', '1', 'admin', '请求访问主页', '157', 'com.ifast.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 07:32:52');
INSERT INTO `sys_log` VALUES ('1030598362564513794', '1', 'admin', '主页', '0', 'com.ifast.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 07:32:52');
INSERT INTO `sys_log` VALUES ('1030598363994771457', '1', 'admin', '主页', '0', 'com.ifast.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 07:32:52');
INSERT INTO `sys_log` VALUES ('1030598407766528001', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 07:33:03');
INSERT INTO `sys_log` VALUES ('1030598410073395202', '1', 'admin', '查询数据字典key列表', '22', 'com.ifast.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 07:33:03');
INSERT INTO `sys_log` VALUES ('1030598411092611073', '1', 'admin', '查询数据字典列表', '175', 'com.ifast.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 07:33:03');
INSERT INTO `sys_log` VALUES ('1030598423692300290', '1', 'admin', '进入数据字典添加页面', '0', 'com.ifast.common.controller.DictController.add()', null, '127.0.0.1', '2018-08-18 07:33:06');
INSERT INTO `sys_log` VALUES ('1030598463169089538', '1', 'admin', '进入数据字典添加页面', '0', 'com.ifast.common.controller.DictController.add()', null, '127.0.0.1', '2018-08-18 07:33:16');
INSERT INTO `sys_log` VALUES ('1030598491312869377', '1', 'admin', '进入文件管理页面', '0', 'com.ifast.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 07:33:23');
INSERT INTO `sys_log` VALUES ('1030598492390805506', '1', 'admin', '查询文件列表', '42', 'com.ifast.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 07:33:23');
INSERT INTO `sys_log` VALUES ('1030598502633295873', '1', 'admin', '进入系统配置页面', '0', 'com.ifast.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 07:33:25');
INSERT INTO `sys_log` VALUES ('1030598504109690882', '1', 'admin', '查询系统配置列表', '39', 'com.ifast.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 07:33:26');
INSERT INTO `sys_log` VALUES ('1030598511965622274', '1', 'admin', '进入系统配置添加页面', '0', 'com.ifast.common.controller.ConfigController.add()', null, '127.0.0.1', '2018-08-18 07:33:28');
INSERT INTO `sys_log` VALUES ('1030598552465821697', '1', 'admin', '进入系统用户列表页面', '0', 'com.ifast.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 07:33:37');
INSERT INTO `sys_log` VALUES ('1030598553795416066', '1', 'admin', '查询部门树形数据', '19', 'com.ifast.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 07:33:37');
INSERT INTO `sys_log` VALUES ('1030598554030297090', '1', 'admin', '查询系统用户列表', '47', 'com.ifast.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 07:33:38');
INSERT INTO `sys_log` VALUES ('1030598560321753089', '1', 'admin', '添加用户', '27', 'com.ifast.sys.controller.UserController.add()', null, '127.0.0.1', '2018-08-18 07:33:39');
INSERT INTO `sys_log` VALUES ('1030598610057809922', '1', 'admin', '进入部门树形显示页面', '0', 'com.ifast.sys.controller.DeptController.treeView()', null, '127.0.0.1', '2018-08-18 07:33:51');
INSERT INTO `sys_log` VALUES ('1030598612268208130', '1', 'admin', '查询部门树形数据', '13', 'com.ifast.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 07:33:51');
INSERT INTO `sys_log` VALUES ('1030598704496758785', '1', 'admin', '添加用户', '13', 'com.ifast.sys.controller.UserController.add()', null, '127.0.0.1', '2018-08-18 07:34:13');
INSERT INTO `sys_log` VALUES ('1030598741087866881', '1', 'admin', '进入系统配置添加页面', '0', 'com.ifast.common.controller.ConfigController.add()', null, '127.0.0.1', '2018-08-18 07:34:22');
INSERT INTO `sys_log` VALUES ('1030598772406734850', '1', 'admin', '添加用户', '36', 'com.ifast.sys.controller.UserController.add()', null, '127.0.0.1', '2018-08-18 07:34:30');
INSERT INTO `sys_log` VALUES ('1030599168915263490', '1', 'admin', '进入代码生成页面', '0', 'com.ifast.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 07:36:04');
INSERT INTO `sys_log` VALUES ('1030599170379075585', '1', 'admin', '查询数据表列表', '24', 'com.ifast.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 07:36:04');
INSERT INTO `sys_log` VALUES ('1030599181774999553', '1', 'admin', '根据数据表生成代码', '466', 'com.ifast.generator.controller.GeneratorController.code()', null, '127.0.0.1', '2018-08-18 07:36:07');
INSERT INTO `sys_log` VALUES ('1030599231607525377', '1', 'admin', '进入代码生成配置编辑页面', '29', 'com.ifast.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 07:36:19');
INSERT INTO `sys_log` VALUES ('1030599772286865410', '1', 'admin', '进入系统日志列表页面', '0', 'com.ifast.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 07:38:28');
INSERT INTO `sys_log` VALUES ('1030599773725511681', '1', 'admin', '查询系统日志列表', '37', 'com.ifast.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 07:38:28');
INSERT INTO `sys_log` VALUES ('1030599848820330497', '1', 'admin', '删除系统日志', '78', 'com.ifast.common.controller.LogController.remove()', null, '127.0.0.1', '2018-08-18 07:38:46');
INSERT INTO `sys_log` VALUES ('1030599849055211521', '1', 'admin', '查询系统日志列表', '24', 'com.ifast.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 07:38:46');
INSERT INTO `sys_log` VALUES ('1030599898266980354', '1', 'admin', '查询系统日志列表', '301', 'com.ifast.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 07:38:58');
INSERT INTO `sys_log` VALUES ('1030606252541071362', '1', 'admin', '主页', '0', 'com.ifast.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:04:13');
INSERT INTO `sys_log` VALUES ('1030606253312823297', '1', 'admin', '主页', '0', 'com.ifast.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:04:13');
INSERT INTO `sys_log` VALUES ('1030606262208942082', '1', 'admin', '进入个人中心', '712', 'com.ifast.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 08:04:15');
INSERT INTO `sys_log` VALUES ('1030610863175766018', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 08:22:32');
INSERT INTO `sys_log` VALUES ('1030610902807744513', '1', 'admin', '登录', '38', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 08:22:42');
INSERT INTO `sys_log` VALUES ('1030610903701131266', '1', 'admin', '请求访问主页', '109', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 08:22:42');
INSERT INTO `sys_log` VALUES ('1030610904183476226', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:22:42');
INSERT INTO `sys_log` VALUES ('1030610905127194626', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:22:42');
INSERT INTO `sys_log` VALUES ('1030610919446548482', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 08:22:46');
INSERT INTO `sys_log` VALUES ('1030610920545456129', '1', 'admin', '查询数据字典key列表', '19', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 08:22:46');
INSERT INTO `sys_log` VALUES ('1030610920994246658', '1', 'admin', '查询数据字典列表', '87', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 08:22:46');
INSERT INTO `sys_log` VALUES ('1030610947133149185', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 08:22:52');
INSERT INTO `sys_log` VALUES ('1030610948097839105', '1', 'admin', '查询文件列表', '45', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:22:53');
INSERT INTO `sys_log` VALUES ('1030610953307164674', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 08:22:54');
INSERT INTO `sys_log` VALUES ('1030610954368323586', '1', 'admin', '查询系统配置列表', '31', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 08:22:54');
INSERT INTO `sys_log` VALUES ('1030610998131691522', '1', 'admin', '查询文件列表', '26', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:04');
INSERT INTO `sys_log` VALUES ('1030611002728648705', '1', 'admin', '查询文件列表', '348', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:06');
INSERT INTO `sys_log` VALUES ('1030611004620279809', '1', 'admin', '查询文件列表', '16', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:06');
INSERT INTO `sys_log` VALUES ('1030611009108185090', '1', 'admin', '查询文件列表', '28', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:07');
INSERT INTO `sys_log` VALUES ('1030611012023226370', '1', 'admin', '查询文件列表', '21', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:08');
INSERT INTO `sys_log` VALUES ('1030611015047319554', '1', 'admin', '查询文件列表', '22', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 08:23:08');
INSERT INTO `sys_log` VALUES ('1030611411803312130', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 08:24:43');
INSERT INTO `sys_log` VALUES ('1030611413095157762', '1', 'admin', '查询系统日志列表', '35', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 08:24:43');
INSERT INTO `sys_log` VALUES ('1030611517105508354', '1', 'admin', '请求访问主页', '91', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 08:25:08');
INSERT INTO `sys_log` VALUES ('1030611517315223553', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:25:08');
INSERT INTO `sys_log` VALUES ('1030611517784985601', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 08:25:08');
INSERT INTO `sys_log` VALUES ('1030611528996360194', '1', 'admin', '进入个人中心', '112', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 08:25:11');
INSERT INTO `sys_log` VALUES ('1030625048525840386', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 09:18:54');
INSERT INTO `sys_log` VALUES ('1030627929161220097', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 09:30:21');
INSERT INTO `sys_log` VALUES ('1030627962342359041', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 09:30:29');
INSERT INTO `sys_log` VALUES ('1030627963135082498', '1', 'admin', '请求访问主页', '104', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 09:30:29');
INSERT INTO `sys_log` VALUES ('1030627965450338305', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 09:30:30');
INSERT INTO `sys_log` VALUES ('1030627967409078274', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 09:30:30');
INSERT INTO `sys_log` VALUES ('1030639113075703810', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 10:14:48');
INSERT INTO `sys_log` VALUES ('1030639172613849090', '1', 'admin', '登录', '177', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:15:02');
INSERT INTO `sys_log` VALUES ('1030639329011056642', '1', 'admin', '请求访问主页', '36633', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:15:39');
INSERT INTO `sys_log` VALUES ('1030639329556316162', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:15:39');
INSERT INTO `sys_log` VALUES ('1030639330302902273', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:15:39');
INSERT INTO `sys_log` VALUES ('1030639952842481666', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 10:18:08');
INSERT INTO `sys_log` VALUES ('1030639974984212481', '1', 'admin', '登录', '41', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:18:13');
INSERT INTO `sys_log` VALUES ('1030639975802101761', '1', 'admin', '请求访问主页', '110', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:18:13');
INSERT INTO `sys_log` VALUES ('1030639976292835329', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:18:13');
INSERT INTO `sys_log` VALUES ('1030639976938758146', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:18:14');
INSERT INTO `sys_log` VALUES ('1030640404992647170', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 10:19:56');
INSERT INTO `sys_log` VALUES ('1030640997572304898', '1', 'admin', '登录', '26', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:22:17');
INSERT INTO `sys_log` VALUES ('1030640998125953025', '1', 'admin', '请求访问主页', '96', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:22:17');
INSERT INTO `sys_log` VALUES ('1030641001812746242', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:18');
INSERT INTO `sys_log` VALUES ('1030641002618052609', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:18');
INSERT INTO `sys_log` VALUES ('1030641021655998465', '1', 'admin', '请求访问主页', '103', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641021974765569', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641022587133953', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641022792654849', '1', 'admin', '请求访问主页', '133', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641023392440321', '1', 'admin', '请求访问主页', '147', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641023946088449', '1', 'admin', '请求访问主页', '149', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641024310992897', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641024709451778', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:22:23');
INSERT INTO `sys_log` VALUES ('1030641468395495425', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 10:24:09');
INSERT INTO `sys_log` VALUES ('1030641494999965697', '1', 'admin', '登录', '40', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:24:15');
INSERT INTO `sys_log` VALUES ('1030641495767523330', '1', 'admin', '请求访问主页', '102', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:24:16');
INSERT INTO `sys_log` VALUES ('1030641496291811330', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:16');
INSERT INTO `sys_log` VALUES ('1030641497113894913', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:16');
INSERT INTO `sys_log` VALUES ('1030641608837570561', '1', 'admin', '请求访问主页', '466', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641608875319297', '1', 'admin', '请求访问主页', '119', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641610448183297', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641610871808002', '1', 'admin', '请求访问主页', '75', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641611123466241', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641611312209922', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641611865858050', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:24:43');
INSERT INTO `sys_log` VALUES ('1030641674168049665', '1', 'admin', '进入个人中心', '123', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:24:58');
INSERT INTO `sys_log` VALUES ('1030641706510327810', '1', 'admin', '请求访问主页', '91', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:25:06');
INSERT INTO `sys_log` VALUES ('1030641706791346178', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:25:06');
INSERT INTO `sys_log` VALUES ('1030641707470823426', '1', 'admin', '主页', '1', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:25:06');
INSERT INTO `sys_log` VALUES ('1030641719919517697', '1', 'admin', '进入个人中心', '126', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:25:09');
INSERT INTO `sys_log` VALUES ('1030641826710691841', '1', 'admin', '请求访问主页', '106', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:25:35');
INSERT INTO `sys_log` VALUES ('1030641826966544385', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:25:35');
INSERT INTO `sys_log` VALUES ('1030641827666993153', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:25:35');
INSERT INTO `sys_log` VALUES ('1030641845262098434', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 10:25:39');
INSERT INTO `sys_log` VALUES ('1030642101714526210', '1', 'admin', '登录', '41', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:26:40');
INSERT INTO `sys_log` VALUES ('1030642102675021826', '1', 'admin', '请求访问主页', '110', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:26:40');
INSERT INTO `sys_log` VALUES ('1030642104138833922', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:26:41');
INSERT INTO `sys_log` VALUES ('1030642104948334594', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:26:41');
INSERT INTO `sys_log` VALUES ('1030643510623088641', '1', 'admin', '登录', '47', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:32:16');
INSERT INTO `sys_log` VALUES ('1030643511763939329', '1', 'admin', '请求访问主页', '162', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:32:16');
INSERT INTO `sys_log` VALUES ('1030643515824025602', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:32:17');
INSERT INTO `sys_log` VALUES ('1030643516755161090', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:32:17');
INSERT INTO `sys_log` VALUES ('1030643614348226561', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:32:41');
INSERT INTO `sys_log` VALUES ('1030644268932308993', '1', 'admin', '登录', '38', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:35:17');
INSERT INTO `sys_log` VALUES ('1030644269733421058', '1', 'admin', '请求访问主页', '101', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:35:17');
INSERT INTO `sys_log` VALUES ('1030644270224154626', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:17');
INSERT INTO `sys_log` VALUES ('1030644270928797697', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:17');
INSERT INTO `sys_log` VALUES ('1030644342559121410', '1', 'admin', '请求访问主页', '96', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:35:34');
INSERT INTO `sys_log` VALUES ('1030644342982746113', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:34');
INSERT INTO `sys_log` VALUES ('1030644343532199937', '1', 'admin', '请求访问主页', '104', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644343737720834', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644344102625281', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644344635301889', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644344694022146', '1', 'admin', '请求访问主页', '100', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644345147006978', '1', 'admin', '请求访问主页', '89', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644345482551297', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644346065559553', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:35:35');
INSERT INTO `sys_log` VALUES ('1030644705131560962', '1', 'admin', '登录', '35', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:37:01');
INSERT INTO `sys_log` VALUES ('1030644705970421761', '1', 'admin', '请求访问主页', '112', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:37:01');
INSERT INTO `sys_log` VALUES ('1030644706410823682', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:37:01');
INSERT INTO `sys_log` VALUES ('1030644707790749697', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:37:01');
INSERT INTO `sys_log` VALUES ('1030644946740281345', '1', 'admin', '登录', '38', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:37:58');
INSERT INTO `sys_log` VALUES ('1030644947566559233', '1', 'admin', '请求访问主页', '104', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:37:59');
INSERT INTO `sys_log` VALUES ('1030644948032126978', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:37:59');
INSERT INTO `sys_log` VALUES ('1030644948845821953', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:37:59');
INSERT INTO `sys_log` VALUES ('1030644987928346625', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:38:08');
INSERT INTO `sys_log` VALUES ('1030645519891869697', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:40:15');
INSERT INTO `sys_log` VALUES ('1030645520718147586', '1', 'admin', '请求访问主页', '108', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:40:15');
INSERT INTO `sys_log` VALUES ('1030645521150160898', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:40:15');
INSERT INTO `sys_log` VALUES ('1030645521968050178', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:40:16');
INSERT INTO `sys_log` VALUES ('1030645556600418306', '1', 'admin', '进入个人中心', '124', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:40:24');
INSERT INTO `sys_log` VALUES ('1030645867855597570', '1', 'admin', '登录', '42', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:41:38');
INSERT INTO `sys_log` VALUES ('1030645868795121665', '1', 'admin', '请求访问主页', '121', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:41:38');
INSERT INTO `sys_log` VALUES ('1030645869415878658', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:41:38');
INSERT INTO `sys_log` VALUES ('1030645870074384386', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:41:39');
INSERT INTO `sys_log` VALUES ('1030646030619758594', '1', 'admin', '进入个人中心', '462', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:42:17');
INSERT INTO `sys_log` VALUES ('1030646271301505025', '1', 'admin', '请求访问主页', '140', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:43:14');
INSERT INTO `sys_log` VALUES ('1030646271792238593', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:43:14');
INSERT INTO `sys_log` VALUES ('1030646272530436097', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:43:15');
INSERT INTO `sys_log` VALUES ('1030646284509368321', '1', 'admin', '进入个人中心', '111', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:43:17');
INSERT INTO `sys_log` VALUES ('1030646441300836354', '1', 'admin', '登录', '46', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:43:55');
INSERT INTO `sys_log` VALUES ('1030646443293130753', '1', 'admin', '请求访问主页', '369', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:43:55');
INSERT INTO `sys_log` VALUES ('1030646443943247874', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:43:55');
INSERT INTO `sys_log` VALUES ('1030646444740165633', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:43:56');
INSERT INTO `sys_log` VALUES ('1030646461949394945', '1', 'admin', '进入个人中心', '129', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:44:00');
INSERT INTO `sys_log` VALUES ('1030646519293919234', '1', 'admin', '进入个人中心', '97', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 10:44:13');
INSERT INTO `sys_log` VALUES ('1030647166407872514', '1', 'admin', '登录', '43', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:46:48');
INSERT INTO `sys_log` VALUES ('1030647167397728257', '1', 'admin', '请求访问主页', '110', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 10:46:48');
INSERT INTO `sys_log` VALUES ('1030647167863296002', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:46:48');
INSERT INTO `sys_log` VALUES ('1030647168735711233', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 10:46:48');
INSERT INTO `sys_log` VALUES ('1030647223202942978', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 10:47:01');
INSERT INTO `sys_log` VALUES ('1030647224511565825', '1', 'admin', '查询数据字典key列表', '18', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 10:47:01');
INSERT INTO `sys_log` VALUES ('1030647224956162049', '1', 'admin', '查询数据字典列表', '82', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 10:47:02');
INSERT INTO `sys_log` VALUES ('1030649334795890689', '-1', '获取用户信息为空', '登录', '58', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 10:55:25');
INSERT INTO `sys_log` VALUES ('1030651398506684417', '-1', '获取用户信息为空', '登录', '38', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:03:37');
INSERT INTO `sys_log` VALUES ('1030652959811182594', '-1', '获取用户信息为空', '登录', '117', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:09:49');
INSERT INTO `sys_log` VALUES ('1030653534917369858', '1', 'admin', '登录', '64', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:12:06');
INSERT INTO `sys_log` VALUES ('1030653538193121282', '1', 'admin', '请求访问主页', '567', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:12:07');
INSERT INTO `sys_log` VALUES ('1030653541695365122', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:12:08');
INSERT INTO `sys_log` VALUES ('1030653542454534145', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:12:08');
INSERT INTO `sys_log` VALUES ('1030653577506332673', '1', 'admin', '进入个人中心', '787', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:12:16');
INSERT INTO `sys_log` VALUES ('1030653890955071489', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:13:31');
INSERT INTO `sys_log` VALUES ('1030653891806515202', '1', 'admin', '请求访问主页', '111', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:13:31');
INSERT INTO `sys_log` VALUES ('1030653892251111425', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:13:31');
INSERT INTO `sys_log` VALUES ('1030653893001891842', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:13:31');
INSERT INTO `sys_log` VALUES ('1030653904062271490', '1', 'admin', '进入个人中心', '249', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:13:34');
INSERT INTO `sys_log` VALUES ('1030653948446396418', '1', 'admin', '进入个人中心', '95', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:13:45');
INSERT INTO `sys_log` VALUES ('1030654058853060609', '1', 'admin', '进入个人中心', '411', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:14:11');
INSERT INTO `sys_log` VALUES ('1030654124401643521', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:14:27');
INSERT INTO `sys_log` VALUES ('1030654165375799297', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 11:14:36');
INSERT INTO `sys_log` VALUES ('1030654166516649985', '1', 'admin', '查询菜单列表', '24', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 11:14:37');
INSERT INTO `sys_log` VALUES ('1030654315913564162', '1', 'admin', '添加菜单', '0', 'com.scj.sys.controller.MenuController.add()', null, '127.0.0.1', '2018-08-18 11:15:12');
INSERT INTO `sys_log` VALUES ('1030654980312293377', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:17:51');
INSERT INTO `sys_log` VALUES ('1030654981922906113', '1', 'admin', '请求访问主页', '258', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:17:51');
INSERT INTO `sys_log` VALUES ('1030654983961337857', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:17:51');
INSERT INTO `sys_log` VALUES ('1030654984561123329', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:17:52');
INSERT INTO `sys_log` VALUES ('1030655378561437698', '-1', '获取用户信息为空', '登录', '112', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:19:25');
INSERT INTO `sys_log` VALUES ('1030655410152935425', '1', 'admin', '登录', '23', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:19:33');
INSERT INTO `sys_log` VALUES ('1030655410794663937', '1', 'admin', '请求访问主页', '101', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:19:33');
INSERT INTO `sys_log` VALUES ('1030655411239260162', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:19:33');
INSERT INTO `sys_log` VALUES ('1030655412044566529', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:19:34');
INSERT INTO `sys_log` VALUES ('1030655450166595586', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 11:19:43');
INSERT INTO `sys_log` VALUES ('1030655451307446274', '1', 'admin', '查询数据字典key列表', '18', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 11:19:43');
INSERT INTO `sys_log` VALUES ('1030655451747848193', '1', 'admin', '查询数据字典列表', '84', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 11:19:43');
INSERT INTO `sys_log` VALUES ('1030655457196249089', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 11:19:44');
INSERT INTO `sys_log` VALUES ('1030655458148356097', '1', 'admin', '查询文件列表', '53', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 11:19:45');
INSERT INTO `sys_log` VALUES ('1030655463739363330', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 11:19:46');
INSERT INTO `sys_log` VALUES ('1030655464821493762', '1', 'admin', '查询系统配置列表', '29', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 11:19:46');
INSERT INTO `sys_log` VALUES ('1030655487227465730', '1', 'admin', '进入系统配置添加页面', '0', 'com.scj.common.controller.ConfigController.add()', null, '127.0.0.1', '2018-08-18 11:19:51');
INSERT INTO `sys_log` VALUES ('1030655506328326146', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 11:19:56');
INSERT INTO `sys_log` VALUES ('1030655508798771202', '1', 'admin', '查询部门树形数据', '20', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 11:19:57');
INSERT INTO `sys_log` VALUES ('1030655509071400962', '1', 'admin', '查询系统用户列表', '60', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 11:19:57');
INSERT INTO `sys_log` VALUES ('1030655517686501378', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 11:19:59');
INSERT INTO `sys_log` VALUES ('1030655518638608386', '1', 'admin', '查询系统角色菜单', '27', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 11:19:59');
INSERT INTO `sys_log` VALUES ('1030655523856322561', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 11:20:00');
INSERT INTO `sys_log` VALUES ('1030655524745515010', '1', 'admin', '查询菜单列表', '22', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 11:20:00');
INSERT INTO `sys_log` VALUES ('1030655531737419778', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 11:20:02');
INSERT INTO `sys_log` VALUES ('1030655532723081218', '1', 'admin', '获取部门列表', '19', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 11:20:02');
INSERT INTO `sys_log` VALUES ('1030655544836231169', '1', 'admin', '进入添加部门页面', '0', 'com.scj.sys.controller.DeptController.add()', null, '127.0.0.1', '2018-08-18 11:20:05');
INSERT INTO `sys_log` VALUES ('1030655577342087169', '1', 'admin', '进入添加部门页面', '12', 'com.scj.sys.controller.DeptController.add()', null, '127.0.0.1', '2018-08-18 11:20:13');
INSERT INTO `sys_log` VALUES ('1030655604030443521', '1', 'admin', '添加用户', '12', 'com.scj.sys.controller.UserController.add()', null, '127.0.0.1', '2018-08-18 11:20:19');
INSERT INTO `sys_log` VALUES ('1030655609151688706', '1', 'admin', '进入部门树形显示页面', '0', 'com.scj.sys.controller.DeptController.treeView()', null, '127.0.0.1', '2018-08-18 11:20:21');
INSERT INTO `sys_log` VALUES ('1030655610149933058', '1', 'admin', '查询部门树形数据', '12', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 11:20:21');
INSERT INTO `sys_log` VALUES ('1030655667267964930', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 11:20:34');
INSERT INTO `sys_log` VALUES ('1030655668282986497', '1', 'admin', '查询系统日志列表', '29', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 11:20:35');
INSERT INTO `sys_log` VALUES ('1030655678051520514', '1', 'admin', '进入在线用户列表页面', '4', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 11:20:37');
INSERT INTO `sys_log` VALUES ('1030655681004310530', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 11:20:38');
INSERT INTO `sys_log` VALUES ('1030655847115526146', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:21:17');
INSERT INTO `sys_log` VALUES ('1030656425698811905', '1', 'admin', '登录', '35', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:23:35');
INSERT INTO `sys_log` VALUES ('1030656426722222081', '1', 'admin', '请求访问主页', '124', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:23:35');
INSERT INTO `sys_log` VALUES ('1030656427196178433', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:23:36');
INSERT INTO `sys_log` VALUES ('1030656428051816450', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:23:36');
INSERT INTO `sys_log` VALUES ('1030656455641948161', '1', 'admin', '进入在线用户列表页面', '4', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 11:23:42');
INSERT INTO `sys_log` VALUES ('1030656456782798849', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 11:23:43');
INSERT INTO `sys_log` VALUES ('1030656466899460098', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 11:23:45');
INSERT INTO `sys_log` VALUES ('1030656467876732929', '1', 'admin', '查询数据表列表', '20', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 11:23:45');
INSERT INTO `sys_log` VALUES ('1030656473610346498', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 11:23:47');
INSERT INTO `sys_log` VALUES ('1030656475250319361', '1', 'admin', '查询定时任务列表', '84', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 11:23:47');
INSERT INTO `sys_log` VALUES ('1030656500361617410', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 11:23:53');
INSERT INTO `sys_log` VALUES ('1030656501405999105', '1', 'admin', '查询部门树形数据', '17', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 11:23:53');
INSERT INTO `sys_log` VALUES ('1030656501674434562', '1', 'admin', '查询系统用户列表', '51', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 11:23:53');
INSERT INTO `sys_log` VALUES ('1030656504052604930', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 11:23:54');
INSERT INTO `sys_log` VALUES ('1030656505021489154', '1', 'admin', '查询系统角色菜单', '22', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 11:23:54');
INSERT INTO `sys_log` VALUES ('1030656510323089410', '1', 'admin', '进入系统菜单页面', '1', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 11:23:55');
INSERT INTO `sys_log` VALUES ('1030656511396831234', '1', 'admin', '查询菜单列表', '27', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 11:23:56');
INSERT INTO `sys_log` VALUES ('1030656571312463874', '1', 'admin', '添加菜单', '13', 'com.scj.sys.controller.MenuController.add()', null, '127.0.0.1', '2018-08-18 11:24:10');
INSERT INTO `sys_log` VALUES ('1030656647648796673', '1', 'admin', '保存菜单', '99', 'com.scj.sys.controller.MenuController.save()', null, '127.0.0.1', '2018-08-18 11:24:28');
INSERT INTO `sys_log` VALUES ('1030656647967563777', '1', 'admin', '查询菜单列表', '19', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 11:24:28');
INSERT INTO `sys_log` VALUES ('1030656727885832193', '1', 'admin', '编辑角色', '32', 'com.scj.sys.controller.RoleController.edit()', null, '127.0.0.1', '2018-08-18 11:24:47');
INSERT INTO `sys_log` VALUES ('1030656729773268994', '1', 'admin', '根据角色ID查询菜单树形数据', '219', 'com.scj.sys.controller.MenuController.tree()', null, '127.0.0.1', '2018-08-18 11:24:48');
INSERT INTO `sys_log` VALUES ('1030656881145700353', '1', 'admin', '更新角色', '115', 'com.scj.sys.controller.RoleController.update()', null, '127.0.0.1', '2018-08-18 11:25:24');
INSERT INTO `sys_log` VALUES ('1030656881359609858', '1', 'admin', '查询系统角色菜单', '12', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 11:25:24');
INSERT INTO `sys_log` VALUES ('1030656896320692226', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 11:25:27');
INSERT INTO `sys_log` VALUES ('1030656922539286529', '1', 'admin', '登录', '16', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:25:34');
INSERT INTO `sys_log` VALUES ('1030656923046797314', '1', 'admin', '请求访问主页', '88', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:25:34');
INSERT INTO `sys_log` VALUES ('1030656923571085313', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:25:34');
INSERT INTO `sys_log` VALUES ('1030656924435111938', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:25:34');
INSERT INTO `sys_log` VALUES ('1030656943045238785', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 11:25:39');
INSERT INTO `sys_log` VALUES ('1030656944009928705', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 11:25:39');
INSERT INTO `sys_log` VALUES ('1030657225363841026', '1', 'admin', '进入在线用户列表页面', '0', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 11:26:46');
INSERT INTO `sys_log` VALUES ('1030657226458554369', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 11:26:46');
INSERT INTO `sys_log` VALUES ('1030660464792592386', '1', 'admin', '进入个人中心', '586', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:39:38');
INSERT INTO `sys_log` VALUES ('1030660507138285570', '1', 'admin', '进入个人中心', '230', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 11:39:48');
INSERT INTO `sys_log` VALUES ('1030661917141655553', '1', 'admin', '请求访问主页', '87', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:45:24');
INSERT INTO `sys_log` VALUES ('1030661917615611906', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:45:25');
INSERT INTO `sys_log` VALUES ('1030661918160871426', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:45:25');
INSERT INTO `sys_log` VALUES ('1030662088466366466', '1', 'admin', '登录', '59', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:46:05');
INSERT INTO `sys_log` VALUES ('1030662090408329218', '1', 'admin', '请求访问主页', '117', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:46:06');
INSERT INTO `sys_log` VALUES ('1030662090966171650', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:46:06');
INSERT INTO `sys_log` VALUES ('1030662091633065986', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:46:06');
INSERT INTO `sys_log` VALUES ('1030662449386258434', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 11:47:31');
INSERT INTO `sys_log` VALUES ('1030662450334171137', '1', 'admin', '请求访问主页', '111', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 11:47:32');
INSERT INTO `sys_log` VALUES ('1030662450908790785', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:47:32');
INSERT INTO `sys_log` VALUES ('1030662451743457281', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 11:47:32');
INSERT INTO `sys_log` VALUES ('1030664425733914626', '1', 'admin', '进入在线用户列表页面', '13', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 11:55:23');
INSERT INTO `sys_log` VALUES ('1030664427227086849', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 11:55:23');
INSERT INTO `sys_log` VALUES ('1030664431685632001', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 11:55:24');
INSERT INTO `sys_log` VALUES ('1030664433220747265', '1', 'admin', '查询系统日志列表', '72', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 11:55:24');
INSERT INTO `sys_log` VALUES ('1030664497032888321', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 11:55:40');
INSERT INTO `sys_log` VALUES ('1030664498203099138', '1', 'admin', '查询系统配置列表', '30', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 11:55:40');
INSERT INTO `sys_log` VALUES ('1030664548987731969', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 11:55:52');
INSERT INTO `sys_log` VALUES ('1030664550048890882', '1', 'admin', '查询部门树形数据', '22', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 11:55:52');
INSERT INTO `sys_log` VALUES ('1030664550296354817', '1', 'admin', '查询系统用户列表', '52', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 11:55:52');
INSERT INTO `sys_log` VALUES ('1030664564808646658', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 11:55:56');
INSERT INTO `sys_log` VALUES ('1030664565890777090', '1', 'admin', '查询数据字典key列表', '16', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 11:55:56');
INSERT INTO `sys_log` VALUES ('1030664566037577729', '1', 'admin', '查询数据字典列表', '25', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 11:55:56');
INSERT INTO `sys_log` VALUES ('1030664598094643202', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 11:56:04');
INSERT INTO `sys_log` VALUES ('1030664599151607809', '1', 'admin', '查询文件列表', '44', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 11:56:04');
INSERT INTO `sys_log` VALUES ('1030664638318018561', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 11:56:13');
INSERT INTO `sys_log` VALUES ('1030664639496617985', '1', 'admin', '获取部门列表', '17', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 11:56:14');
INSERT INTO `sys_log` VALUES ('1030664647881031681', '1', 'admin', '进入添加部门页面', '0', 'com.scj.sys.controller.DeptController.add()', null, '127.0.0.1', '2018-08-18 11:56:16');
INSERT INTO `sys_log` VALUES ('1030665377492852737', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 11:59:09');
INSERT INTO `sys_log` VALUES ('1030695746355159041', '1', 'admin', '登录', '209', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 13:59:50');
INSERT INTO `sys_log` VALUES ('1030695747345014786', '1', 'admin', '请求访问主页', '124', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 13:59:50');
INSERT INTO `sys_log` VALUES ('1030695750864035841', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 13:59:51');
INSERT INTO `sys_log` VALUES ('1030695752734695425', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 13:59:52');
INSERT INTO `sys_log` VALUES ('1030695780014448642', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 13:59:58');
INSERT INTO `sys_log` VALUES ('1030695781218213890', '1', 'admin', '查询菜单列表', '25', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 13:59:58');
INSERT INTO `sys_log` VALUES ('1030696782260174849', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:03:57');
INSERT INTO `sys_log` VALUES ('1030696783308750849', '1', 'admin', '请求访问主页', '136', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:03:57');
INSERT INTO `sys_log` VALUES ('1030696783925313537', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:03:57');
INSERT INTO `sys_log` VALUES ('1030696784596402178', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:03:58');
INSERT INTO `sys_log` VALUES ('1030696815672000513', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:04:05');
INSERT INTO `sys_log` VALUES ('1030696816980623361', '1', 'admin', '查询数据字典key列表', '18', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:04:05');
INSERT INTO `sys_log` VALUES ('1030696817530077185', '1', 'admin', '查询数据字典列表', '109', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:04:05');
INSERT INTO `sys_log` VALUES ('1030696845438976001', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 14:04:12');
INSERT INTO `sys_log` VALUES ('1030696846412054529', '1', 'admin', '查询文件列表', '41', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:04:12');
INSERT INTO `sys_log` VALUES ('1030696876141281282', '1', 'admin', '上传文件', '744', 'com.scj.oss.controller.FileController.upload()', '{\"bytes\":\"iVBORw0KGgoAAAANSUhEUgAAAv4AAAINCAIAAADJAexZAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzdeXhcxZUo8FNVd+tdu2xLlmXLNgbbGAxhMQybA8YmYRJCCHlJ4EGIyTpASCaQBIb3PrJNAhgekwRnZQiBsMMQDA4BwuIAgWAWL3i3vGhvqfe7VdX740rtVmuxbMmSuvv8PsNnt27fvn10u/v0qTp1iZQSEEIIIYRKA53oA0AIIYQQGj9K9m+EkOzfs6UgvBFvxBvxRrwRb8Qb8cYiuDF7O8ndAiGEEEKoKBFCvIQHB7wQQgghVEIw9UEIIYRQCcEBL4QQQgiVEIJ5D0IIIYRKBw54IYQQQqiEYOqDEEIIoeKXbXSneS3vCCGEEEJFDKs+CCGEECohh5b6JBKJGTNmRCKRPXv2DLqBbdtLly4lhPz6178GgDvvvJOMQHaHmzZtKisr82709jDMYRBCDMN48cUXR3hsA+8+0HXXXXdIARmJbEyGObZhDinvaR7e9gghhBDyTN6qz8MPPzzUj15//fXm5ubD3vMo736otm/f/vbbbwNAPB5/7rnnDmMPlmWdc8458+fPj8fjR2J7hBBCqOhlW9rpEW1uv+aaa2QO7yMZAH71q1/l3h6LxaZPn55339dff32oGskwWdFIbNy4EQDOO+88OcAdd9wxmj0Pau3atbFYbP78+QBwxx13DJOOhMPh5ubmgUe1du1a77D/4z/+YzTbI4QQQmgyVn3C4fD5558/VI1k06ZNDz300CmnnNLQ0HB4+9+wYQMAfPrTnx75XbwBpsMYSEokErfffruu6z/84Q8bGho2btz40EMPHdrhApx77rmrVq0CgLVr146kkHOo2yOEEEKlYzKmPtCXlwxaI/GKKIeUuOSybXv79u26rjc1NY32KEfAG1xramo666yzli1bBodbsjrvvPMikcjevXtjsdiR2B4hhBAqbpO9ud0r6mRnyWTZtv3UU095ZaHD27O3T13XHcfJTqkeflb1YbNt+8c//jEAnHfeeeFw+LrrrotEIn/7299wDjJCCCE0USZp1ScUCi1btsyyrKeeeir3di9xOeWUUwbODRohrxASj8eXLVuWWxH50pe+5FVlxpB3tOFw+Jvf/CYANDU1nXDCCQOf1Eh4ta76+vpIJHIktkcIIYRKxOGkPvF4vKGhYdC2al3XX3jhhTE5Mm9IK2+2yihHu6BvjjP0n2qdnRkzts3t3tFmEzVN0y688EIA+O1vf3vQDvxcd95557XXXgsA1113XTgcHvPtEUIIodIxSas+MNiYV3a0azTlmWzT2VVXXZV7o5f95CYlK1euzKZ0Xi+V16GWvXH47nFvgjP0n0995ZVXNjQ0DDWDe6ic0stjzjvvvNxjPoztEUIIoZI1qub2oXqqc9vXR2/gmNfoR7uGMXxSchi8Cc55iZr3pOBgXe55vICP/MAOdXuEEEKodEzeqg8MGPMa/WjXMHRdnz17du4tq1evzqZ0Xn3FG87L3rhhw4ahhpOyE5wHFmZ+9atfQc46h7mGyikHXffoMLZHCCGE0KROfXLHvMZktGvcDJrZ5LIsy8uNEEIIITQOJntzuycUCn3zm9/0xrzGZLQre+mrga3s2ab3MVnvxytQHXPMMbFYbGBVxptXNMxy1QghhBA6QiZ11Qf6luZbu3btI488YprmDTfcMJq9DTPV5o477ojFYl7/+aiOOGeC81ANVt6Tisfj3mYIIYQQGjeTPfXxcpGNGzfefPPNY5KXeFOFNm7cmDtnaOXKld4UnDHpBh90gnOuo48++pJLLgG80ARCCCE07iZ76pNdCwf61kQefvth1hzyEpHc61vlTT2+9tprh+oGD4VCu3fvNk3z7LPPPugxe5equPLKK4cZm/NWdj68S3ohhBBC6FCNqrl9nHnDQ7quZ3OgUbrmmmu8DCl7i9cqNSaXbfeurnrQo81WsEZ5FXqEEEIIHRIy+VMfhBBCCKGxMtkHvBBCCCGERq8wmtsRQgghhMYWVn0QQgghVEIw9UEIIYRQCSGQ0+6FEEIIIVTcsMMLIYQQQiUEB7wQQgghVEIw9UEIIYRQ8cPmdoQQQgiVIqz6IIQQQqiEYOqDEEIIoRKCze0IIYQQKiHY3I4QQgihEoIDXgghhBAqIZj6IIQQQqj4YXM7QgghhEoRVn0QQgghVEIw9UEIIYRQCcHmdoQQQgiVEGxuRwghhFAJwQEvhBBCCJUQTH0QQgghVPywuR0hhBBCpQirPgghhBAqIZj6IIQQQqiEYHM7QgghhEoINrcjhBBCqITggBdCCCGESgimPgghhBAqftjcjhBCCKFShFUfhBBCCJUQTH0QQgghVEKwuR0hhBBCJQSb2xFCCCFUQnDACyGEEEIlBFMfhBBCCBU/bG5HCCGEUCnCqg9CCCGESgimPgghhBAqIdjcjhBCCKESgs3tCCGEECohOOCFEEIIoRKCqQ9CCCGEih82tyOEEEKoFGHVByGEEEIlBFMfhBBCCJUQbG5HCCGEUAkZo+b2310OdmoM9oMQQgghlOfcb8GsU8ZqZ8rY7ObdJyETG5tdIYQQQgjl+silY7gznOuDEEIIoeKHze0IIYQQKkVY9UEIIYRQCcHUByGEEEIlBFMfhBBCCBW/bEs7xUV9EEIIIVQ6sOqDEEIIoRKCqQ9CCCGEih82tyOEEEKoFGHVByGEEEIlZIwuZDECV6+G1QDPrITlQ2xw55NwbRusXA73TIdtH8CcdSPabe8O43Dag+Ddw9vD8IcBAKsuhWvCIz22gXfPs2QJvLZgRAd8UEM9RNZIjhMhhBBCgyrCqs/qnUP/bM9BsoqDGOXdx8iK1XD1nok+CIQQQqigZFvalcnZ3D57Acj+RZTcmtBBbIY1ZwxeF1kzTFY0Atu8K7TOA3nGqPYzEkOVdrya0Oo18Ams/SCEEEKHrtiqPivnAQA8MWhRJA63bgaYBysPd+dbowAAK2cewl2uXg1kNdwZP9yHHOCelb3HP/hzRAghhNCwxm+uz/j4xExYvRlW/xO+PR1m9//RtmZY5yUumw9z51u6AQCOiYzyGEfrmFqAtkFuX/MyrMh5armTmXJ59bPRbrYHyBqAWtj6r/3i3DtJK1sY2wNkDSxZAq9FgKzp3Sa3dDeSgxnh80IIIYSGQQjxRrqKrrl9OqwEgDb484BCy5+3AwB84qDjZUOJw0NtAABzAU5bDaTvz/hPu9k4MO+Jw2mr++UHAHDtg0BeHmSza9vyNzvtg/6b7QEy2Gb5exu5KJy25sC/en8FQzxKv4MZ4fNCCCGERqzYqj4A8Il5sHozPNQM1+TOFvISl3mwHOCJw9tvrLeDbMWD/W5evQZWj8vsH4CcRrZa+HZODnd1340HajDelpvhtIoDrWdr1sM66DdXySvSrFsHaxYcmDl0tZem5Oytt5bTf28jt27zgPpQvC8ZGvAo69bBnQ29dZ0RPi+EEEJo5MY79Vlx5Fukls8E2AzrtsO2BQc+aw+Mdh2u3jnO/cdrRpkTDGP4QK06O+epfQCrYcDk6zC8dimc9mC/ODyxGQBg1XEHtpq9AFZth2vb4Ik9sHx6zt76ZyqzF8BWgDnr8qM6crkHDNkkbMCjPBOFFX1p68ifF0IIITRyRVj18ca8VrfBn+MHJoWMdrRrsKYzyM0J+goneRNTPNc+CNdm/zFgfsyhGVBh8p7aIFldGC6phXU5cfAmCV37IMzN6Q675l/hmoF7W5x/hLMXwMp1+VEdubn97+LNmhr4KMvPOPDsRv68EEIIoYOasOb2gy5pOCbyx7xyRrvGXG9OAAcKJ2OiX6D29M4RHnThRG/qz+o1Q645tDEGEAYAuGYxXLsGIKekNHCH3t4GncrtZU7ZvR2CWpgz2DEPP2F85M8LIYQQGrlirPoMGPMa/WjX8HJbrnLrFtC3DM9om5Kmg1wOZA2sWwenwShG1qaDvPTAstcAsG4dkHWjLkQhhBBChaM4U5+8Ma/Rj3ZNvOmwdUnvyNrVkUHWdRxpdhWG17x1gfYc6DaHNpjz8njN1D5E2MqOEEJoTBRvc3ufby8BAHioecxGu64eqpW9r+n9SK/3M3sBrKoFAFi9BnJaxeGYWgBv9OeQTAe5EuRK2LoEAAA29+5zmL2NZJRqhIZ8lD1AVvf2rh/m80IIIYSGVWyrOWfNboAlAOu2w5pmWNe/renwfGIeAMDqf8K2/rdnm5UuOPLFiWvOBi9RWZGzsM0FTYMfGOStJR3vXY5oTf9tvEAddG+9/VbZCcsRWAIAbbC1/2Y/HdlFZ+eWD/4od/4TAGBJxaE8L4QQQuhQFG3q4/UBQRusWDc2eclyb6pQG8zJSTuy/VwDm5WOiDB8fx4AAGw+8ME/e0HvKo5z+qc1vVeAn9c3YOQFBGDFk/2SiTtf7F3sZ3ne3nI26+3hB1iypK94FgZvxlFuEnbQa85nLT+uN3PKC6Y3z/37Cw7leSGEEEKHokjn+gAAwAVNvR+lS5pGlJcMuZSO10/eN9sGNgPp376+ZMmQF1W9ZyXccwiHfHDLz4CVm2E1wLUvwgV9c5PvuRQ+eBDWDfYUnsmZwXPN2fDQg7CuDeYMu9k9y2H1mt6co595/WZYf3sJrB4QjWeWw4q8stKgwvDaciBrBgnmyuUHhiZH+LwQQgihg8q2tNPJeeX2MZEdyrmkYYx2uADkyvyrnz6zcryXFb7HSw3a4KfZiUdheG0lPDOv32ZLloDMW0ogDK+t7J0wdMC8AZtNBzlgs1WX5s+Dnr0AZO7damHrIV1MfuCj1MLWlf2TyBE+L4QQQmjEyNikPteWQQbnoyKEEELoCLj6YVh88VjtrHjn+iCEEEII9cm2tBdtcztCCCGE0EBY9UEIIYRQCcHUByGEEEIlBFMfhBBCCBW/kmhuRwghhBDKg1UfhBBCCJUQTH0QQgghVPywuR0hhBBCpQirP', '127.0.0.1', '2018-08-18 14:04:19');
INSERT INTO `sys_log` VALUES ('1030696876699123713', '1', 'admin', '查询文件列表', '26', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:04:19');
INSERT INTO `sys_log` VALUES ('1030697373975805953', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 14:06:18');
INSERT INTO `sys_log` VALUES ('1030697375091490817', '1', 'admin', '查询菜单列表', '24', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:06:18');
INSERT INTO `sys_log` VALUES ('1030697432456986625', '1', 'admin', '删除菜单', '55', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:06:32');
INSERT INTO `sys_log` VALUES ('1030697432754782209', '1', 'admin', '查询菜单列表', '21', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:06:32');
INSERT INTO `sys_log` VALUES ('1030697486760640513', '1', 'admin', '删除菜单', '61', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:06:45');
INSERT INTO `sys_log` VALUES ('1030697487066824705', '1', 'admin', '查询菜单列表', '25', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:06:45');
INSERT INTO `sys_log` VALUES ('1030697515881693186', '1', 'admin', '删除菜单', '57', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:06:52');
INSERT INTO `sys_log` VALUES ('1030697516112379906', '1', 'admin', '查询菜单列表', '17', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:06:52');
INSERT INTO `sys_log` VALUES ('1030697712246423553', '1', 'admin', '删除菜单', '356', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:07:39');
INSERT INTO `sys_log` VALUES ('1030697712615522305', '1', 'admin', '查询菜单列表', '19', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:07:39');
INSERT INTO `sys_log` VALUES ('1030697753442877441', '1', 'admin', '删除菜单', '57', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:07:49');
INSERT INTO `sys_log` VALUES ('1030697753681952770', '1', 'admin', '查询菜单列表', '18', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:07:49');
INSERT INTO `sys_log` VALUES ('1030697883248197634', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 14:08:19');
INSERT INTO `sys_log` VALUES ('1030697909726838785', '1', 'admin', '登录', '14', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:08:26');
INSERT INTO `sys_log` VALUES ('1030697910242738177', '1', 'admin', '请求访问主页', '80', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:08:26');
INSERT INTO `sys_log` VALUES ('1030697910490202114', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:08:26');
INSERT INTO `sys_log` VALUES ('1030697911136124929', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:08:26');
INSERT INTO `sys_log` VALUES ('1030697922829844481', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:08:29');
INSERT INTO `sys_log` VALUES ('1030697923760979970', '1', 'admin', '查询数据字典key列表', '11', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:08:29');
INSERT INTO `sys_log` VALUES ('1030697923932946434', '1', 'admin', '查询数据字典列表', '34', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:08:29');
INSERT INTO `sys_log` VALUES ('1030697928626372609', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 14:08:30');
INSERT INTO `sys_log` VALUES ('1030697929251323906', '1', 'admin', '查询文件列表', '23', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:08:30');
INSERT INTO `sys_log` VALUES ('1030697934703919105', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 14:08:32');
INSERT INTO `sys_log` VALUES ('1030697935832186882', '1', 'admin', '查询系统配置列表', '27', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 14:08:32');
INSERT INTO `sys_log` VALUES ('1030697950034100226', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 14:08:35');
INSERT INTO `sys_log` VALUES ('1030697950977818626', '1', 'admin', '查询部门树形数据', '19', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:08:36');
INSERT INTO `sys_log` VALUES ('1030697951124619266', '1', 'admin', '查询系统用户列表', '27', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:08:36');
INSERT INTO `sys_log` VALUES ('1030697993159933953', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:08:46');
INSERT INTO `sys_log` VALUES ('1030697994103652354', '1', 'admin', '查询系统角色菜单', '21', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:08:46');
INSERT INTO `sys_log` VALUES ('1030698024487190529', '1', 'admin', '编辑角色', '12', 'com.scj.sys.controller.RoleController.edit()', null, '127.0.0.1', '2018-08-18 14:08:53');
INSERT INTO `sys_log` VALUES ('1030698025774841858', '1', 'admin', '根据角色ID查询菜单树形数据', '86', 'com.scj.sys.controller.MenuController.tree()', null, '127.0.0.1', '2018-08-18 14:08:53');
INSERT INTO `sys_log` VALUES ('1030698047765577729', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 14:08:59');
INSERT INTO `sys_log` VALUES ('1030698048575078401', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:08:59');
INSERT INTO `sys_log` VALUES ('1030698069332688898', '1', 'admin', '删除菜单', '364', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:04');
INSERT INTO `sys_log` VALUES ('1030698069580152833', '1', 'admin', '查询菜单列表', '15', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:04');
INSERT INTO `sys_log` VALUES ('1030698094238466049', '1', 'admin', '删除菜单', '364', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:10');
INSERT INTO `sys_log` VALUES ('1030698094716616705', '1', 'admin', '查询菜单列表', '41', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:10');
INSERT INTO `sys_log` VALUES ('1030698113200914434', '1', 'admin', '删除菜单', '88', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:14');
INSERT INTO `sys_log` VALUES ('1030698113473544194', '1', 'admin', '查询菜单列表', '27', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:14');
INSERT INTO `sys_log` VALUES ('1030698128967303169', '1', 'admin', '删除菜单', '63', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:18');
INSERT INTO `sys_log` VALUES ('1030698129185406978', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:18');
INSERT INTO `sys_log` VALUES ('1030698139889270785', '1', 'admin', '删除菜单', '56', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:21');
INSERT INTO `sys_log` VALUES ('1030698140103180290', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:21');
INSERT INTO `sys_log` VALUES ('1030698153982132226', '1', 'admin', '删除菜单', '59', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:24');
INSERT INTO `sys_log` VALUES ('1030698154225401858', '1', 'admin', '查询菜单列表', '22', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:24');
INSERT INTO `sys_log` VALUES ('1030698165248032770', '1', 'admin', '删除菜单', '62', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:27');
INSERT INTO `sys_log` VALUES ('1030698165461942274', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:27');
INSERT INTO `sys_log` VALUES ('1030698178321678337', '1', 'admin', '删除菜单', '53', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:30');
INSERT INTO `sys_log` VALUES ('1030698178606891010', '1', 'admin', '查询菜单列表', '15', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:30');
INSERT INTO `sys_log` VALUES ('1030698190820704257', '1', 'admin', '删除菜单', '49', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:33');
INSERT INTO `sys_log` VALUES ('1030698191030419457', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:33');
INSERT INTO `sys_log` VALUES ('1030698201746866178', '1', 'admin', '删除菜单', '72', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:35');
INSERT INTO `sys_log` VALUES ('1030698201973358594', '1', 'admin', '查询菜单列表', '14', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:35');
INSERT INTO `sys_log` VALUES ('1030698215705509890', '1', 'admin', '删除菜单', '55', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:39');
INSERT INTO `sys_log` VALUES ('1030698215944585217', '1', 'admin', '查询菜单列表', '19', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:39');
INSERT INTO `sys_log` VALUES ('1030698227256623106', '1', 'admin', '删除菜单', '56', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:41');
INSERT INTO `sys_log` VALUES ('1030698227449561090', '1', 'admin', '查询菜单列表', '14', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:42');
INSERT INTO `sys_log` VALUES ('1030698237864017921', '1', 'admin', '删除菜单', '58', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:44');
INSERT INTO `sys_log` VALUES ('1030698238073733122', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:44');
INSERT INTO `sys_log` VALUES ('1030698260278378497', '1', 'admin', '删除菜单', '57', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:49');
INSERT INTO `sys_log` VALUES ('1030698260609728513', '1', 'admin', '查询菜单列表', '18', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:49');
INSERT INTO `sys_log` VALUES ('1030698273477853186', '1', 'admin', '删除菜单', '57', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:53');
INSERT INTO `sys_log` VALUES ('1030698273666596866', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:53');
INSERT INTO `sys_log` VALUES ('1030698284370460674', '1', 'admin', '删除菜单', '66', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:55');
INSERT INTO `sys_log` VALUES ('1030698284563398657', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:55');
INSERT INTO `sys_log` VALUES ('1030698294659088386', '1', 'admin', '删除菜单', '63', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:09:58');
INSERT INTO `sys_log` VALUES ('1030698294847832066', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:09:58');
INSERT INTO `sys_log` VALUES ('1030698308898750465', '1', 'admin', '删除菜单', '112', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:01');
INSERT INTO `sys_log` VALUES ('1030698309150408706', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:01');
INSERT INTO `sys_log` VALUES ('1030698321204838402', '1', 'admin', '删除菜单', '54', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:04');
INSERT INTO `sys_log` VALUES ('1030698321414553602', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:04');
INSERT INTO `sys_log` VALUES ('1030698332869197826', '1', 'admin', '删除菜单', '60', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:07');
INSERT INTO `sys_log` VALUES ('1030698333083107329', '1', 'admin', '查询菜单列表', '14', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:07');
INSERT INTO `sys_log` VALUES ('1030698343606616066', '1', 'admin', '删除菜单', '62', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:09');
INSERT INTO `sys_log` VALUES ('1030698343820525570', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:09');
INSERT INTO `sys_log` VALUES ('1030698357963718657', '1', 'admin', '删除菜单', '51', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:13');
INSERT INTO `sys_log` VALUES ('1030698358156656642', '1', 'admin', '查询菜单列表', '11', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:13');
INSERT INTO `sys_log` VALUES ('1030698371045752834', '1', 'admin', '删除菜单', '68', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:16');
INSERT INTO `sys_log` VALUES ('1030698371251273729', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:16');
INSERT INTO `sys_log` VALUES ('1030698382613643265', '1', 'admin', '删除菜单', '56', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:19');
INSERT INTO `sys_log` VALUES ('1030698382810775554', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:19');
INSERT INTO `sys_log` VALUES ('1030698393724354562', '1', 'admin', '删除菜单', '90', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:21');
INSERT INTO `sys_log` VALUES ('1030698394051510273', '1', 'admin', '查询菜单列表', '38', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:21');
INSERT INTO `sys_log` VALUES ('1030698410459627522', '1', 'admin', '删除菜单', '79', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:25');
INSERT INTO `sys_log` VALUES ('1030698410652565505', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:25');
INSERT INTO `sys_log` VALUES ('1030698422484697089', '1', 'admin', '删除菜单', '52', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:28');
INSERT INTO `sys_log` VALUES ('1030698422681829377', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:28');
INSERT INTO `sys_log` VALUES ('1030698436992794626', '1', 'admin', '删除菜单', '53', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:31');
INSERT INTO `sys_log` VALUES ('1030698437240258561', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:32');
INSERT INTO `sys_log` VALUES ('1030698448250306562', '1', 'admin', '删除菜单', '59', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:34');
INSERT INTO `sys_log` VALUES ('1030698448451633154', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:34');
INSERT INTO `sys_log` VALUES ('1030698467745431553', '1', 'admin', '删除菜单', '58', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:39');
INSERT INTO `sys_log` VALUES ('1030698467787374594', '1', 'admin', '删除菜单', '68', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:39');
INSERT INTO `sys_log` VALUES ('1030698468009672705', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:39');
INSERT INTO `sys_log` VALUES ('1030698468185833474', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:39');
INSERT INTO `sys_log` VALUES ('1030698500649746434', '1', 'admin', '删除菜单', '55', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:47');
INSERT INTO `sys_log` VALUES ('1030698500859461633', '1', 'admin', '查询菜单列表', '11', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:47');
INSERT INTO `sys_log` VALUES ('1030698512616095745', '1', 'admin', '删除菜单', '52', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:50');
INSERT INTO `sys_log` VALUES ('1030698512825810945', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:50');
INSERT INTO `sys_log` VALUES ('1030698522862780418', '1', 'admin', '删除菜单', '55', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:10:52');
INSERT INTO `sys_log` VALUES ('1030698523072495617', '1', 'admin', '查询菜单列表', '14', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:10:52');
INSERT INTO `sys_log` VALUES ('1030698630689947650', '1', 'admin', '删除菜单', '66', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:11:18');
INSERT INTO `sys_log` VALUES ('1030698630891274242', '1', 'admin', '查询菜单列表', '12', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:11:18');
INSERT INTO `sys_log` VALUES ('1030698641704189953', '1', 'admin', '删除菜单', '53', 'com.scj.sys.controller.MenuController.remove()', null, '127.0.0.1', '2018-08-18 14:11:20');
INSERT INTO `sys_log` VALUES ('1030698641926488066', '1', 'admin', '查询菜单列表', '13', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:11:20');
INSERT INTO `sys_log` VALUES ('1030698660997988353', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 14:11:25');
INSERT INTO `sys_log` VALUES ('1030698684859383809', '1', 'admin', '登录', '13', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:11:31');
INSERT INTO `sys_log` VALUES ('1030698685324951553', '1', 'admin', '请求访问主页', '79', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:11:31');
INSERT INTO `sys_log` VALUES ('1030698685564026882', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:11:31');
INSERT INTO `sys_log` VALUES ('1030698686268669953', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:11:31');
INSERT INTO `sys_log` VALUES ('1030698707013697538', '1', 'admin', '进入数据字典列表页面', '1', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:11:36');
INSERT INTO `sys_log` VALUES ('1030698708066467842', '1', 'admin', '查询数据字典key列表', '13', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:11:36');
INSERT INTO `sys_log` VALUES ('1030698708234240001', '1', 'admin', '查询数据字典列表', '29', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:11:36');
INSERT INTO `sys_log` VALUES ('1030698716933226498', '1', 'admin', '进入在线用户列表页面', '4', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 14:11:38');
INSERT INTO `sys_log` VALUES ('1030698717688201217', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 14:11:38');
INSERT INTO `sys_log` VALUES ('1030698723765747713', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 14:11:40');
INSERT INTO `sys_log` VALUES ('1030698724751409154', '1', 'admin', '查询系统日志列表', '28', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 14:11:40');
INSERT INTO `sys_log` VALUES ('1030698739427278849', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 14:11:44');
INSERT INTO `sys_log` VALUES ('1030698740278722561', '1', 'admin', '查询数据表列表', '22', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 14:11:44');
INSERT INTO `sys_log` VALUES ('1030698867991085058', '1', 'admin', '进入代码生成配置编辑页面', '39', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 14:12:14');
INSERT INTO `sys_log` VALUES ('1030700023349825538', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 14:16:50');
INSERT INTO `sys_log` VALUES ('1030700050839293953', '1', 'admin', '登录', '38', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:16:56');
INSERT INTO `sys_log` VALUES ('1030700051661377537', '1', 'admin', '请求访问主页', '110', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:16:56');
INSERT INTO `sys_log` VALUES ('1030700052118556673', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:16:57');
INSERT INTO `sys_log` VALUES ('1030700053032914945', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:16:57');
INSERT INTO `sys_log` VALUES ('1030700065624215554', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:17:00');
INSERT INTO `sys_log` VALUES ('1030700066882506753', '1', 'admin', '查询数据字典key列表', '18', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:17:00');
INSERT INTO `sys_log` VALUES ('1030700067398406146', '1', 'admin', '查询数据字典列表', '94', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:17:00');
INSERT INTO `sys_log` VALUES ('1030700146746249218', '1', 'admin', '进入个人中心', '121', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 14:17:19');
INSERT INTO `sys_log` VALUES ('1030700208582873090', '1', 'admin', '查询数据字典列表', '36', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:17:34');
INSERT INTO `sys_log` VALUES ('1030700252111360001', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 14:17:44');
INSERT INTO `sys_log` VALUES ('1030700253084438530', '1', 'admin', '查询文件列表', '42', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:17:44');
INSERT INTO `sys_log` VALUES ('1030700258960658433', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 14:17:46');
INSERT INTO `sys_log` VALUES ('1030700260328001537', '1', 'admin', '查询系统配置列表', '29', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 14:17:46');
INSERT INTO `sys_log` VALUES ('1030700280716513281', '1', 'admin', '删除文件', '63', 'com.scj.oss.controller.FileController.remove()', null, '127.0.0.1', '2018-08-18 14:17:51');
INSERT INTO `sys_log` VALUES ('1030700280963977217', '1', 'admin', '查询文件列表', '24', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:17:51');
INSERT INTO `sys_log` VALUES ('1030700306687643650', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 14:17:57');
INSERT INTO `sys_log` VALUES ('1030700307652333570', '1', 'admin', '查询部门树形数据', '16', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:17:57');
INSERT INTO `sys_log` VALUES ('1030700307845271553', '1', 'admin', '查询系统用户列表', '40', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:17:58');
INSERT INTO `sys_log` VALUES ('1030700347800211458', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:18:07');
INSERT INTO `sys_log` VALUES ('1030700349012365313', '1', 'admin', '查询系统角色菜单', '22', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:18:07');
INSERT INTO `sys_log` VALUES ('1030702647620960257', '1', 'admin', '登录', '33', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:27:15');
INSERT INTO `sys_log` VALUES ('1030702648484986881', '1', 'admin', '请求访问主页', '110', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:27:16');
INSERT INTO `sys_log` VALUES ('1030702648958943234', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:27:16');
INSERT INTO `sys_log` VALUES ('1030702649869107201', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:27:16');
INSERT INTO `sys_log` VALUES ('1030702661776736258', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:27:19');
INSERT INTO `sys_log` VALUES ('1030702662959529985', '1', 'admin', '查询数据字典key列表', '22', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:27:19');
INSERT INTO `sys_log` VALUES ('1030702664779857922', '1', 'admin', '查询数据字典列表', '417', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:27:19');
INSERT INTO `sys_log` VALUES ('1030702687026446337', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 14:27:25');
INSERT INTO `sys_log` VALUES ('1030702687961776130', '1', 'admin', '查询文件列表', '46', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:27:25');
INSERT INTO `sys_log` VALUES ('1030702701349994497', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 14:27:28');
INSERT INTO `sys_log` VALUES ('1030702702457290754', '1', 'admin', '查询系统配置列表', '27', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 14:27:28');
INSERT INTO `sys_log` VALUES ('1030702737190322178', '1', 'admin', '查询系统配置列表', '25', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 14:27:37');
INSERT INTO `sys_log` VALUES ('1030702751673253890', '1', 'admin', '查询系统配置列表', '24', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 14:27:40');
INSERT INTO `sys_log` VALUES ('1030702762788159490', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 14:27:43');
INSERT INTO `sys_log` VALUES ('1030702763647991809', '1', 'admin', '查询部门树形数据', '15', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:27:43');
INSERT INTO `sys_log` VALUES ('1030702763840929793', '1', 'admin', '查询系统用户列表', '32', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:27:43');
INSERT INTO `sys_log` VALUES ('1030702773378777089', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 14:27:45');
INSERT INTO `sys_log` VALUES ('1030702774385410049', '1', 'admin', '查询数据表列表', '19', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 14:27:46');
INSERT INTO `sys_log` VALUES ('1030702781503143937', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 14:27:47');
INSERT INTO `sys_log` VALUES ('1030702782610440194', '1', 'admin', '查询定时任务列表', '38', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:27:48');
INSERT INTO `sys_log` VALUES ('1030702797080788993', '1', 'admin', '进入在线用户列表页面', '3', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 14:27:51');
INSERT INTO `sys_log` VALUES ('1030702797999341570', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 14:27:51');
INSERT INTO `sys_log` VALUES ('1030702803414188034', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 14:27:53');
INSERT INTO `sys_log` VALUES ('1030702804513095681', '1', 'admin', '查询系统日志列表', '31', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 14:27:53');
INSERT INTO `sys_log` VALUES ('1030702921299296257', '1', 'admin', '批量删除用户', '401', 'com.scj.sys.controller.UserController.batchRemove()', null, '127.0.0.1', '2018-08-18 14:28:21');
INSERT INTO `sys_log` VALUES ('1030702921660006401', '1', 'admin', '查询系统用户列表', '33', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:28:21');
INSERT INTO `sys_log` VALUES ('1030702940999942145', '1', 'admin', '批量删除用户', '75', 'com.scj.sys.controller.UserController.batchRemove()', null, '127.0.0.1', '2018-08-18 14:28:25');
INSERT INTO `sys_log` VALUES ('1030702941243211777', '1', 'admin', '查询系统用户列表', '23', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:28:25');
INSERT INTO `sys_log` VALUES ('1030702962013405186', '1', 'admin', '编辑用户', '104', 'com.scj.sys.controller.UserController.edit()', null, '127.0.0.1', '2018-08-18 14:28:30');
INSERT INTO `sys_log` VALUES ('1030703038559453186', '1', 'admin', '更新用户', '110', 'com.scj.sys.controller.UserController.update()', null, '127.0.0.1', '2018-08-18 14:28:49');
INSERT INTO `sys_log` VALUES ('1030703038806917122', '1', 'admin', '查询系统用户列表', '29', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:28:49');
INSERT INTO `sys_log` VALUES ('1030703063926603777', '1', 'admin', '进入个人中心', '104', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 14:28:55');
INSERT INTO `sys_log` VALUES ('1030703224446812162', '1', 'admin', '更新用户', '59', 'com.scj.sys.controller.UserController.updatePeronal()', null, '127.0.0.1', '2018-08-18 14:29:33');
INSERT INTO `sys_log` VALUES ('1030703364335239169', '1', 'admin', '进入个人中心', '407', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 14:30:06');
INSERT INTO `sys_log` VALUES ('1030703399026327554', '1', 'admin', '添加用户', '12', 'com.scj.sys.controller.UserController.add()', null, '127.0.0.1', '2018-08-18 14:30:15');
INSERT INTO `sys_log` VALUES ('1030703442575785985', '1', 'admin', '退出', '351', 'com.scj.sys.controller.UserController.exit()', null, '127.0.0.1', '2018-08-18 14:30:25');
INSERT INTO `sys_log` VALUES ('1030703457457176577', '1', 'admin', '进入部门树形显示页面', '0', 'com.scj.sys.controller.DeptController.treeView()', null, '127.0.0.1', '2018-08-18 14:30:28');
INSERT INTO `sys_log` VALUES ('1030703458367340546', '1', 'admin', '查询部门树形数据', '12', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:30:29');
INSERT INTO `sys_log` VALUES ('1030703516005466114', '1', 'admin', '保存用户', '102', 'com.scj.sys.controller.UserController.save()', null, '127.0.0.1', '2018-08-18 14:30:42');
INSERT INTO `sys_log` VALUES ('1030703516202598402', '1', 'admin', '查询系统用户列表', '26', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:30:42');
INSERT INTO `sys_log` VALUES ('1030703568774004737', '1', 'admin', '请求更改用户密码', '0', 'com.scj.sys.controller.UserController.resetPwd()', null, '127.0.0.1', '2018-08-18 14:30:55');
INSERT INTO `sys_log` VALUES ('1030703644003041281', '1', 'admin', '请求更改用户密码', '0', 'com.scj.sys.controller.UserController.resetPwd()', null, '127.0.0.1', '2018-08-18 14:31:13');
INSERT INTO `sys_log` VALUES ('1030703663879847938', '1', 'admin', 'admin提交更改用户密码', '92', 'com.scj.sys.controller.UserController.adminResetPwd()', null, '127.0.0.1', '2018-08-18 14:31:18');
INSERT INTO `sys_log` VALUES ('1030703664198615042', '1', 'admin', '查询系统用户列表', '30', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:31:18');
INSERT INTO `sys_log` VALUES ('1030703684276748289', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 14:31:23');
INSERT INTO `sys_log` VALUES ('1030703711921405954', '1030703515640561666', 'gacl', '登录', '13', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:31:29');
INSERT INTO `sys_log` VALUES ('1030703712378585090', '1030703515640561666', 'gacl', '请求访问主页', '79', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:31:29');
INSERT INTO `sys_log` VALUES ('1030703712575717378', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:31:29');
INSERT INTO `sys_log` VALUES ('1030703713322303490', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:31:29');
INSERT INTO `sys_log` VALUES ('1030703724936331266', '1030703515640561666', 'gacl', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:31:32');
INSERT INTO `sys_log` VALUES ('1030703725833912322', '1030703515640561666', 'gacl', '查询数据字典key列表', '15', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:31:32');
INSERT INTO `sys_log` VALUES ('1030703726005878785', '1030703515640561666', 'gacl', '查询数据字典列表', '35', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:31:32');
INSERT INTO `sys_log` VALUES ('1030703731613663234', '1030703515640561666', 'gacl', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 14:31:34');
INSERT INTO `sys_log` VALUES ('1030703732234420226', '1030703515640561666', 'gacl', '查询文件列表', '25', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 14:31:34');
INSERT INTO `sys_log` VALUES ('1030703740769828866', '1030703515640561666', 'gacl', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 14:31:36');
INSERT INTO `sys_log` VALUES ('1030703741482860546', '1030703515640561666', 'gacl', '查询部门树形数据', '13', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:31:36');
INSERT INTO `sys_log` VALUES ('1030703741562552321', '1030703515640561666', 'gacl', '查询系统用户列表', '24', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:31:36');
INSERT INTO `sys_log` VALUES ('1030703788714917890', '1030703515640561666', 'gacl', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:31:47');
INSERT INTO `sys_log` VALUES ('1030703789834797058', '1030703515640561666', 'gacl', '查询系统角色菜单', '20', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:31:48');
INSERT INTO `sys_log` VALUES ('1030703803768274946', '1030703515640561666', 'gacl', '删除角色', '81', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:31:51');
INSERT INTO `sys_log` VALUES ('1030703828971847681', '1030703515640561666', 'gacl', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:31:57');
INSERT INTO `sys_log` VALUES ('1030703829739405313', '1030703515640561666', 'gacl', '查询系统角色菜单', '11', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:31:57');
INSERT INTO `sys_log` VALUES ('1030704436441280514', '1', 'admin', '登录', '33', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:34:22');
INSERT INTO `sys_log` VALUES ('1030704437267558402', '1', 'admin', '请求访问主页', '113', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:34:22');
INSERT INTO `sys_log` VALUES ('1030704437871538177', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:34:22');
INSERT INTO `sys_log` VALUES ('1030704438634901506', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:34:22');
INSERT INTO `sys_log` VALUES ('1030704454170603522', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:34:26');
INSERT INTO `sys_log` VALUES ('1030704455458254849', '1', 'admin', '查询系统角色菜单', '22', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:34:26');
INSERT INTO `sys_log` VALUES ('1030704506259664898', '1', 'admin', '添加角色', '0', 'com.scj.sys.controller.RoleController.add()', null, '127.0.0.1', '2018-08-18 14:34:39');
INSERT INTO `sys_log` VALUES ('1030704507652173826', '1', 'admin', '查询菜单树形数据', '68', 'com.scj.sys.controller.MenuController.tree()', null, '127.0.0.1', '2018-08-18 14:34:39');
INSERT INTO `sys_log` VALUES ('1030704687860445185', '1', 'admin', '保存角色', '118', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:35:22');
INSERT INTO `sys_log` VALUES ('1030704688099520513', '1', 'admin', '查询系统角色菜单', '16', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:35:22');
INSERT INTO `sys_log` VALUES ('1030705065872093185', '1', 'admin', '批量删除角色', '371', 'com.scj.sys.controller.RoleController.batchRemove()', null, '127.0.0.1', '2018-08-18 14:36:52');
INSERT INTO `sys_log` VALUES ('1030705066115362818', '1', 'admin', '查询系统角色菜单', '2', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:36:52');
INSERT INTO `sys_log` VALUES ('1030705145287045121', '1', 'admin', '进入系统角色页面', '1', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:37:11');
INSERT INTO `sys_log` VALUES ('1030705146138488834', '1', 'admin', '查询系统角色菜单', '0', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:37:11');
INSERT INTO `sys_log` VALUES ('1030705171451113473', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:37:17');
INSERT INTO `sys_log` VALUES ('1030705172210282497', '1', 'admin', '查询系统角色菜单', '0', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:37:17');
INSERT INTO `sys_log` VALUES ('1030705291181715458', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:37:46');
INSERT INTO `sys_log` VALUES ('1030705291995410433', '1', 'admin', '查询系统角色菜单', '1', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:37:46');
INSERT INTO `sys_log` VALUES ('1030705309548572673', '1', 'admin', '批量删除角色', '62', 'com.scj.sys.controller.RoleController.batchRemove()', null, '127.0.0.1', '2018-08-18 14:37:50');
INSERT INTO `sys_log` VALUES ('1030705309686984706', '1', 'admin', '查询系统角色菜单', '0', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:37:50');
INSERT INTO `sys_log` VALUES ('1030705566994952193', '1', 'admin', '删除角色', '3632', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:38:51');
INSERT INTO `sys_log` VALUES ('1030705601962864642', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:39:00');
INSERT INTO `sys_log` VALUES ('1030705602734616578', '1', 'admin', '查询系统角色菜单', '13', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:39:00');
INSERT INTO `sys_log` VALUES ('1030705765624606721', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 14:39:39');
INSERT INTO `sys_log` VALUES ('1030705766505410561', '1', 'admin', '查询部门树形数据', '14', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 14:39:39');
INSERT INTO `sys_log` VALUES ('1030705766962589697', '1', 'admin', '查询系统用户列表', '95', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 14:39:39');
INSERT INTO `sys_log` VALUES ('1030705779818131458', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 14:39:42');
INSERT INTO `sys_log` VALUES ('1030705780732489729', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 14:39:42');
INSERT INTO `sys_log` VALUES ('1030705787074277378', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 14:39:44');
INSERT INTO `sys_log` VALUES ('1030705787925721089', '1', 'admin', '获取部门列表', '15', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 14:39:44');
INSERT INTO `sys_log` VALUES ('1030705814026874881', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:39:50');
INSERT INTO `sys_log` VALUES ('1030705814802821121', '1', 'admin', '查询系统角色菜单', '0', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:39:50');
INSERT INTO `sys_log` VALUES ('1030705838458695682', '1', 'admin', '进入在线用户列表页面', '4', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 14:39:56');
INSERT INTO `sys_log` VALUES ('1030705839284973569', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 14:39:56');
INSERT INTO `sys_log` VALUES ('1030705882117206018', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 14:40:07');
INSERT INTO `sys_log` VALUES ('1030705902832873474', '-1', '获取用户信息为空', '登录', '13', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:40:11');
INSERT INTO `sys_log` VALUES ('1030705921786933250', '1030703515640561666', 'gacl', '登录', '13', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:40:16');
INSERT INTO `sys_log` VALUES ('1030705922281861121', '1030703515640561666', 'gacl', '请求访问主页', '78', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:40:16');
INSERT INTO `sys_log` VALUES ('1030705922495770625', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:40:16');
INSERT INTO `sys_log` VALUES ('1030705923108139009', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:40:16');
INSERT INTO `sys_log` VALUES ('1030705937930809345', '1030703515640561666', 'gacl', '进入在线用户列表页面', '0', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 14:40:20');
INSERT INTO `sys_log` VALUES ('1030705938778058754', '1030703515640561666', 'gacl', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 14:40:20');
INSERT INTO `sys_log` VALUES ('1030706144265400322', '1030703515640561666', 'gacl', '登录', '14', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:41:09');
INSERT INTO `sys_log` VALUES ('1030706144730968065', '1030703515640561666', 'gacl', '请求访问主页', '81', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:41:09');
INSERT INTO `sys_log` VALUES ('1030706144974237697', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:41:09');
INSERT INTO `sys_log` VALUES ('1030706145653714946', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:41:09');
INSERT INTO `sys_log` VALUES ('1030706158014328834', '1030703515640561666', 'gacl', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 14:41:12');
INSERT INTO `sys_log` VALUES ('1030706159054516226', '1030703515640561666', 'gacl', '查询数据字典key列表', '18', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 14:41:13');
INSERT INTO `sys_log` VALUES ('1030706159251648513', '1030703515640561666', 'gacl', '查询数据字典列表', '34', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 14:41:13');
INSERT INTO `sys_log` VALUES ('1030706171612262402', '1030703515640561666', 'gacl', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:41:16');
INSERT INTO `sys_log` VALUES ('1030706172677615617', '1030703515640561666', 'gacl', '查询系统角色菜单', '1', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:41:16');
INSERT INTO `sys_log` VALUES ('1030706177979215874', '1030703515640561666', 'gacl', '添加角色', '0', 'com.scj.sys.controller.RoleController.add()', null, '127.0.0.1', '2018-08-18 14:41:17');
INSERT INTO `sys_log` VALUES ('1030706178969071617', '1030703515640561666', 'gacl', '查询菜单树形数据', '71', 'com.scj.sys.controller.MenuController.tree()', null, '127.0.0.1', '2018-08-18 14:41:17');
INSERT INTO `sys_log` VALUES ('1030706203249897474', '1030703515640561666', 'gacl', '保存角色', '99', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:41:23');
INSERT INTO `sys_log` VALUES ('1030706203535110146', '1030703515640561666', 'gacl', '查询系统角色菜单', '14', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:41:23');
INSERT INTO `sys_log` VALUES ('1030706419202027521', '1030703515640561666', 'gacl', '删除角色', '79', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:42:15');
INSERT INTO `sys_log` VALUES ('1030706486591909889', '1030703515640561666', 'gacl', '删除角色', '75', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:42:31');
INSERT INTO `sys_log` VALUES ('1030706930718453762', '1030703515640561666', 'gacl', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:44:16');
INSERT INTO `sys_log` VALUES ('1030706931502788609', '1030703515640561666', 'gacl', '请求访问主页', '104', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:44:17');
INSERT INTO `sys_log` VALUES ('1030706931972550657', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:44:17');
INSERT INTO `sys_log` VALUES ('1030706932756885506', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:44:17');
INSERT INTO `sys_log` VALUES ('1030706948472942593', '1030703515640561666', 'gacl', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 14:44:21');
INSERT INTO `sys_log` VALUES ('1030706949999669249', '1030703515640561666', 'gacl', '查询定时任务列表', '83', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:44:21');
INSERT INTO `sys_log` VALUES ('1030706976490893314', '1030703515640561666', 'gacl', '根据id和cmd执行/停止定时任务', '51', 'com.scj.job.controller.JobController.changeJobStatus()', null, '127.0.0.1', '2018-08-18 14:44:27');
INSERT INTO `sys_log` VALUES ('1030706976755134465', '1030703515640561666', 'gacl', '查询定时任务列表', '28', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:44:28');
INSERT INTO `sys_log` VALUES ('1030707094770266114', '1030703515640561666', 'gacl', '根据id和cmd执行/停止定时任务', '38', 'com.scj.job.controller.JobController.changeJobStatus()', null, '127.0.0.1', '2018-08-18 14:44:56');
INSERT INTO `sys_log` VALUES ('1030707095021924354', '1030703515640561666', 'gacl', '查询定时任务列表', '26', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:44:56');
INSERT INTO `sys_log` VALUES ('1030707135903805442', '1030703515640561666', 'gacl', '删除定时任务', '36', 'com.scj.job.controller.JobController.remove()', null, '127.0.0.1', '2018-08-18 14:45:05');
INSERT INTO `sys_log` VALUES ('1030707136126103554', '1030703515640561666', 'gacl', '查询定时任务列表', '24', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:45:06');
INSERT INTO `sys_log` VALUES ('1030707144678289409', '1030703515640561666', 'gacl', '删除定时任务', '35', 'com.scj.job.controller.JobController.remove()', null, '127.0.0.1', '2018-08-18 14:45:08');
INSERT INTO `sys_log` VALUES ('1030707144900587521', '1030703515640561666', 'gacl', '查询定时任务列表', '23', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 14:45:08');
INSERT INTO `sys_log` VALUES ('1030707164404101121', '1030703515640561666', 'gacl', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 14:45:12');
INSERT INTO `sys_log` VALUES ('1030707165452677122', '1030703515640561666', 'gacl', '查询系统角色菜单', '23', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:45:13');
INSERT INTO `sys_log` VALUES ('1030707172239060994', '1030703515640561666', 'gacl', '添加角色', '0', 'com.scj.sys.controller.RoleController.add()', null, '127.0.0.1', '2018-08-18 14:45:14');
INSERT INTO `sys_log` VALUES ('1030707173388300289', '1030703515640561666', 'gacl', '查询菜单树形数据', '82', 'com.scj.sys.controller.MenuController.tree()', null, '127.0.0.1', '2018-08-18 14:45:14');
INSERT INTO `sys_log` VALUES ('1030707195001548802', '1030703515640561666', 'gacl', '保存角色', '118', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:45:20');
INSERT INTO `sys_log` VALUES ('1030707195219652610', '1030703515640561666', 'gacl', '查询系统角色菜单', '12', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:45:20');
INSERT INTO `sys_log` VALUES ('1030707216388304898', '1030703515640561666', 'gacl', '删除角色', '80', 'com.scj.sys.controller.RoleController.save()', null, '127.0.0.1', '2018-08-18 14:45:25');
INSERT INTO `sys_log` VALUES ('1030707216577048578', '1030703515640561666', 'gacl', '查询系统角色菜单', '13', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 14:45:25');
INSERT INTO `sys_log` VALUES ('1030707258033549314', '1030703515640561666', 'gacl', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 14:45:35');
INSERT INTO `sys_log` VALUES ('1030707258947907585', '1030703515640561666', 'gacl', '查询数据表列表', '18', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 14:45:35');
INSERT INTO `sys_log` VALUES ('1030710088010133506', '1', 'admin', '登录', '34', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 14:56:49');
INSERT INTO `sys_log` VALUES ('1030710088819634177', '1', 'admin', '请求访问主页', '105', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 14:56:49');
INSERT INTO `sys_log` VALUES ('1030710089318756354', '1', 'admin', '主页', '1', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:56:50');
INSERT INTO `sys_log` VALUES ('1030710090128257026', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 14:56:50');
INSERT INTO `sys_log` VALUES ('1030710101708730370', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 14:56:53');
INSERT INTO `sys_log` VALUES ('1030710102992187393', '1', 'admin', '查询数据表列表', '20', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 14:56:53');
INSERT INTO `sys_log` VALUES ('1030710361432616961', '1', 'admin', '进入在线用户列表页面', '5', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 14:57:54');
INSERT INTO `sys_log` VALUES ('1030710362527330305', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 14:57:55');
INSERT INTO `sys_log` VALUES ('1030712056417972226', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 15:04:39');
INSERT INTO `sys_log` VALUES ('1030712057508491265', '1', 'admin', '查询部门树形数据', '17', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 15:04:39');
INSERT INTO `sys_log` VALUES ('1030712057927921665', '1', 'admin', '查询系统用户列表', '85', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 15:04:39');
INSERT INTO `sys_log` VALUES ('1030712070481473537', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 15:04:42');
INSERT INTO `sys_log` VALUES ('1030712071530049537', '1', 'admin', '查询菜单列表', '31', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 15:04:42');
INSERT INTO `sys_log` VALUES ('1030712082695286785', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 15:04:45');
INSERT INTO `sys_log` VALUES ('1030712083857108993', '1', 'admin', '查询定时任务列表', '50', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 15:04:45');
INSERT INTO `sys_log` VALUES ('1030712103490646017', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 15:04:50');
INSERT INTO `sys_log` VALUES ('1030712104811851778', '1', 'admin', '查询系统日志列表', '34', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 15:04:50');
INSERT INTO `sys_log` VALUES ('1030719012494958594', '1', 'admin', '登录', '277', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 15:32:17');
INSERT INTO `sys_log` VALUES ('1030719017834307585', '1', 'admin', '请求访问主页', '738', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 15:32:18');
INSERT INTO `sys_log` VALUES ('1030719018648002562', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:32:19');
INSERT INTO `sys_log` VALUES ('1030719019293925378', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:32:19');
INSERT INTO `sys_log` VALUES ('1030719042941411329', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 15:32:24');
INSERT INTO `sys_log` VALUES ('1030719047190241281', '1', 'admin', '查询数据字典key列表', '244', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 15:32:25');
INSERT INTO `sys_log` VALUES ('1030719049509691393', '1', 'admin', '查询数据字典列表', '758', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 15:32:26');
INSERT INTO `sys_log` VALUES ('1030719073027153921', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 15:32:32');
INSERT INTO `sys_log` VALUES ('1030719077347287041', '1', 'admin', '查询文件列表', '354', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 15:32:33');
INSERT INTO `sys_log` VALUES ('1030719088986480641', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 15:32:35');
INSERT INTO `sys_log` VALUES ('1030719091419176961', '1', 'admin', '查询部门树形数据', '61', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 15:32:36');
INSERT INTO `sys_log` VALUES ('1030719092379672578', '1', 'admin', '查询系统用户列表', '250', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 15:32:36');
INSERT INTO `sys_log` VALUES ('1030719099581292546', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 15:32:38');
INSERT INTO `sys_log` VALUES ('1030719102370504705', '1', 'admin', '查询菜单列表', '55', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 15:32:38');
INSERT INTO `sys_log` VALUES ('1030719112919179265', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 15:32:41');
INSERT INTO `sys_log` VALUES ('1030719116387868674', '1', 'admin', '获取部门列表', '106', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 15:32:42');
INSERT INTO `sys_log` VALUES ('1030719143051059202', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 15:32:48');
INSERT INTO `sys_log` VALUES ('1030719146951761922', '1', 'admin', '查询定时任务列表', '321', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 15:32:49');
INSERT INTO `sys_log` VALUES ('1030719156430888962', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 15:32:51');
INSERT INTO `sys_log` VALUES ('1030719158536429569', '1', 'admin', '查询数据表列表', '88', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 15:32:52');
INSERT INTO `sys_log` VALUES ('1030719438380392449', '1', 'admin', '进入代码生成配置编辑页面', '660', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 15:33:59');
INSERT INTO `sys_log` VALUES ('1030720101038481410', '1', 'admin', '根据数据表生成代码', '1329', 'com.scj.generator.controller.GeneratorController.code()', null, '127.0.0.1', '2018-08-18 15:36:37');
INSERT INTO `sys_log` VALUES ('1030720244177494018', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 15:37:11');
INSERT INTO `sys_log` VALUES ('1030720247553908738', '1', 'admin', '查询系统配置列表', '276', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 15:37:12');
INSERT INTO `sys_log` VALUES ('1030720273600536578', '1', 'admin', '进入配置编辑页面', '233', 'com.scj.common.controller.ConfigController.edit()', null, '127.0.0.1', '2018-08-18 15:37:18');
INSERT INTO `sys_log` VALUES ('1030720331423211522', '1', 'admin', '更新系统配置', '953', 'com.scj.common.controller.ConfigController.update()', null, '127.0.0.1', '2018-08-18 15:37:32');
INSERT INTO `sys_log` VALUES ('1030720335214862338', '1', 'admin', '查询系统配置列表', '432', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 15:37:32');
INSERT INTO `sys_log` VALUES ('1030720369532657665', '1', 'admin', '进入配置编辑页面', '101', 'com.scj.common.controller.ConfigController.edit()', null, '127.0.0.1', '2018-08-18 15:37:41');
INSERT INTO `sys_log` VALUES ('1030720448029057026', '1', 'admin', '进入配置编辑页面', '40', 'com.scj.common.controller.ConfigController.edit()', null, '127.0.0.1', '2018-08-18 15:37:59');
INSERT INTO `sys_log` VALUES ('1030720475984093185', '1', 'admin', '更新系统配置', '1241', 'com.scj.common.controller.ConfigController.update()', null, '127.0.0.1', '2018-08-18 15:38:06');
INSERT INTO `sys_log` VALUES ('1030720478374846466', '1', 'admin', '查询系统配置列表', '79', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 15:38:07');
INSERT INTO `sys_log` VALUES ('1030720495370166274', '1', 'admin', '进入配置编辑页面', '29', 'com.scj.common.controller.ConfigController.edit()', null, '127.0.0.1', '2018-08-18 15:38:11');
INSERT INTO `sys_log` VALUES ('1030720557886267394', '1', 'admin', '更新系统配置', '1303', 'com.scj.common.controller.ConfigController.update()', null, '127.0.0.1', '2018-08-18 15:38:26');
INSERT INTO `sys_log` VALUES ('1030720562361589762', '1', 'admin', '查询系统配置列表', '531', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 15:38:27');
INSERT INTO `sys_log` VALUES ('1030720616220647425', '1', 'admin', '进入代码生成配置编辑页面', '335', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 15:38:39');
INSERT INTO `sys_log` VALUES ('1030720765588201473', '1', 'admin', '进入在线用户列表页面', '4', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 15:39:15');
INSERT INTO `sys_log` VALUES ('1030720768138338305', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 15:39:16');
INSERT INTO `sys_log` VALUES ('1030720778145947649', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 15:39:18');
INSERT INTO `sys_log` VALUES ('1030720783648874498', '1', 'admin', '查询系统日志列表', '463', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 15:39:19');
INSERT INTO `sys_log` VALUES ('1030721047680311298', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 15:40:22');
INSERT INTO `sys_log` VALUES ('1030721052319211521', '1', 'admin', '查询部门树形数据', '116', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 15:40:23');
INSERT INTO `sys_log` VALUES ('1030721054554775554', '1', 'admin', '查询系统用户列表', '634', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 15:40:24');
INSERT INTO `sys_log` VALUES ('1030721089287806977', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 15:40:32');
INSERT INTO `sys_log` VALUES ('1030721091972161538', '1', 'admin', '查询菜单列表', '57', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 15:40:33');
INSERT INTO `sys_log` VALUES ('1030721112918515714', '1', 'admin', '进入个人中心', '2020', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 15:40:38');
INSERT INTO `sys_log` VALUES ('1030723031078592513', '1', 'admin', '请求访问主页', '2763', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 15:48:15');
INSERT INTO `sys_log` VALUES ('1030723034438230017', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:48:16');
INSERT INTO `sys_log` VALUES ('1030723035100930050', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:48:16');
INSERT INTO `sys_log` VALUES ('1030723064112930818', '1', 'admin', '进入个人中心', '2950', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 15:48:23');
INSERT INTO `sys_log` VALUES ('1030723288084598785', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 15:49:16');
INSERT INTO `sys_log` VALUES ('1030723343214530562', '1', 'admin', '登录', '375', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 15:49:30');
INSERT INTO `sys_log` VALUES ('1030723356703412226', '1', 'admin', '请求访问主页', '2484', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 15:49:33');
INSERT INTO `sys_log` VALUES ('1030723360406982657', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:49:34');
INSERT INTO `sys_log` VALUES ('1030723361300369409', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 15:49:34');
INSERT INTO `sys_log` VALUES ('1030723385967071234', '1', 'admin', '进入个人中心', '2578', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 15:49:40');
INSERT INTO `sys_log` VALUES ('1030724648108601345', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '58.248.235.233', '2018-08-18 15:54:41');
INSERT INTO `sys_log` VALUES ('1030724684175421442', '1', 'admin', '登录', '28', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '58.248.235.233', '2018-08-18 15:54:49');
INSERT INTO `sys_log` VALUES ('1030724684917813249', '1', 'admin', '请求访问主页', '54', 'com.scj.sys.controller.LoginController.index()', null, '58.248.235.233', '2018-08-18 15:54:49');
INSERT INTO `sys_log` VALUES ('1030724685756674050', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '58.248.235.233', '2018-08-18 15:54:50');
INSERT INTO `sys_log` VALUES ('1030724708498190338', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '58.248.235.233', '2018-08-18 15:54:55');
INSERT INTO `sys_log` VALUES ('1030724733823397889', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '58.248.235.233', '2018-08-18 15:55:01');
INSERT INTO `sys_log` VALUES ('1030724765041602562', '1', 'admin', '查询数据字典key列表', '43', 'com.scj.common.controller.DictController.listType()', null, '58.248.235.233', '2018-08-18 15:55:09');
INSERT INTO `sys_log` VALUES ('1030724765331009538', '1', 'admin', '查询数据字典列表', '89', 'com.scj.common.controller.DictController.list()', null, '58.248.235.233', '2018-08-18 15:55:09');
INSERT INTO `sys_log` VALUES ('1030724795278340098', '1', 'admin', '进入数据字典添加页面', '0', 'com.scj.common.controller.DictController.add()', null, '58.248.235.233', '2018-08-18 15:55:16');
INSERT INTO `sys_log` VALUES ('1030724816707039233', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '58.248.235.233', '2018-08-18 15:55:21');
INSERT INTO `sys_log` VALUES ('1030724818833551361', '1', 'admin', '查询文件列表', '31', 'com.scj.oss.controller.FileController.list()', null, '58.248.235.233', '2018-08-18 15:55:21');
INSERT INTO `sys_log` VALUES ('1030724826630762498', '1', 'admin', '进入系统用户列表页面', '1', 'com.scj.sys.controller.UserController.user()', null, '58.248.235.233', '2018-08-18 15:55:23');
INSERT INTO `sys_log` VALUES ('1030724827394125826', '1', 'admin', '查询部门树形数据', '18', 'com.scj.sys.controller.DeptController.tree()', null, '58.248.235.233', '2018-08-18 15:55:23');
INSERT INTO `sys_log` VALUES ('1030724827683532801', '1', 'admin', '查询系统用户列表', '42', 'com.scj.sys.controller.UserController.list()', null, '58.248.235.233', '2018-08-18 15:55:24');
INSERT INTO `sys_log` VALUES ('1030724833853353986', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '58.248.235.233', '2018-08-18 15:55:25');
INSERT INTO `sys_log` VALUES ('1030724834776100865', '1', 'admin', '查询系统角色菜单', '16', 'com.scj.sys.controller.RoleController.list()', null, '58.248.235.233', '2018-08-18 15:55:25');
INSERT INTO `sys_log` VALUES ('1030724839385640961', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '58.248.235.233', '2018-08-18 15:55:26');
INSERT INTO `sys_log` VALUES ('1030724839826042881', '1', 'admin', '查询菜单列表', '14', 'com.scj.sys.controller.MenuController.list()', null, '58.248.235.233', '2018-08-18 15:55:26');
INSERT INTO `sys_log` VALUES ('1030724845257666561', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '58.248.235.233', '2018-08-18 15:55:28');
INSERT INTO `sys_log` VALUES ('1030724846134276098', '1', 'admin', '获取部门列表', '12', 'com.scj.sys.controller.DeptController.list()', null, '58.248.235.233', '2018-08-18 15:55:28');
INSERT INTO `sys_log` VALUES ('1030724872231235585', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '58.248.235.233', '2018-08-18 15:55:34');
INSERT INTO `sys_log` VALUES ('1030725052489838593', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '61.129.8.203', '2018-08-18 15:56:17');
INSERT INTO `sys_log` VALUES ('1030725082391031810', '1030703515640561666', 'gacl', '登录', '7', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '58.248.235.233', '2018-08-18 15:56:24');
INSERT INTO `sys_log` VALUES ('1030725082718187522', '1030703515640561666', 'gacl', '请求访问主页', '39', 'com.scj.sys.controller.LoginController.index()', null, '58.248.235.233', '2018-08-18 15:56:24');
INSERT INTO `sys_log` VALUES ('1030725083011788802', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '58.248.235.233', '2018-08-18 15:56:24');
INSERT INTO `sys_log` VALUES ('1030725083561242625', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '58.248.235.233', '2018-08-18 15:56:25');
INSERT INTO `sys_log` VALUES ('1030725097641521153', '1030703515640561666', 'gacl', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '58.248.235.233', '2018-08-18 15:56:28');
INSERT INTO `sys_log` VALUES ('1030725098190974978', '1030703515640561666', 'gacl', '查询数据字典key列表', '8', 'com.scj.common.controller.DictController.listType()', null, '58.248.235.233', '2018-08-18 15:56:28');
INSERT INTO `sys_log` VALUES ('1030725098375524353', '1030703515640561666', 'gacl', '查询数据字典列表', '15', 'com.scj.common.controller.DictController.list()', null, '58.248.235.233', '2018-08-18 15:56:28');
INSERT INTO `sys_log` VALUES ('1030725106160152577', '1030703515640561666', 'gacl', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '58.248.235.233', '2018-08-18 15:56:30');
INSERT INTO `sys_log` VALUES ('1030725106663469057', '1030703515640561666', 'gacl', '查询文件列表', '12', 'com.scj.oss.controller.FileController.list()', null, '58.248.235.233', '2018-08-18 15:56:30');
INSERT INTO `sys_log` VALUES ('1030725115127574529', '1030703515640561666', 'gacl', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '58.248.235.233', '2018-08-18 15:56:32');
INSERT INTO `sys_log` VALUES ('1030725115534422017', '1030703515640561666', 'gacl', '查询部门树形数据', '6', 'com.scj.sys.controller.DeptController.tree()', null, '58.248.235.233', '2018-08-18 15:56:32');
INSERT INTO `sys_log` VALUES ('1030725115622502401', '1030703515640561666', 'gacl', '查询系统用户列表', '15', 'com.scj.sys.controller.UserController.list()', null, '58.248.235.233', '2018-08-18 15:56:32');
INSERT INTO `sys_log` VALUES ('1030725120676638722', '1030703515640561666', 'gacl', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '58.248.235.233', '2018-08-18 15:56:33');
INSERT INTO `sys_log` VALUES ('1030725121435807746', '1030703515640561666', 'gacl', '查询系统角色菜单', '1', 'com.scj.sys.controller.RoleController.list()', null, '58.248.235.233', '2018-08-18 15:56:34');
INSERT INTO `sys_log` VALUES ('1030725127953756161', '1030703515640561666', 'gacl', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '58.248.235.233', '2018-08-18 15:56:35');
INSERT INTO `sys_log` VALUES ('1030725128599678978', '1030703515640561666', 'gacl', '查询菜单列表', '9', 'com.scj.sys.controller.MenuController.list()', null, '58.248.235.233', '2018-08-18 15:56:35');
INSERT INTO `sys_log` VALUES ('1030725292043317250', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '140.207.118.16', '2018-08-18 15:57:14');
INSERT INTO `sys_log` VALUES ('1030725300721332225', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '61.151.226.62', '2018-08-18 15:57:16');
INSERT INTO `sys_log` VALUES ('1030725320027713538', '1030703515640561666', 'gacl', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '58.248.235.233', '2018-08-18 15:57:21');
INSERT INTO `sys_log` VALUES ('1030725320497475585', '1030703515640561666', 'gacl', '获取部门列表', '5', 'com.scj.sys.controller.DeptController.list()', null, '58.248.235.233', '2018-08-18 15:57:21');
INSERT INTO `sys_log` VALUES ('1030725376399159298', '1030703515640561666', 'gacl', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '58.248.235.233', '2018-08-18 15:57:34');
INSERT INTO `sys_log` VALUES ('1030725376910864385', '1030703515640561666', 'gacl', '查询数据表列表', '11', 'com.scj.generator.controller.GeneratorController.list()', null, '58.248.235.233', '2018-08-18 15:57:34');
INSERT INTO `sys_log` VALUES ('1030725403771187202', '1030703515640561666', 'gacl', '进入在线用户列表页面', '5', 'com.scj.sys.controller.SessionController.online()', null, '58.248.235.233', '2018-08-18 15:57:41');
INSERT INTO `sys_log` VALUES ('1030725404257726465', '1030703515640561666', 'gacl', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '58.248.235.233', '2018-08-18 15:57:41');
INSERT INTO `sys_log` VALUES ('1030725407856439298', '1030703515640561666', 'gacl', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '58.248.235.233', '2018-08-18 15:57:42');
INSERT INTO `sys_log` VALUES ('1030725408569470977', '1030703515640561666', 'gacl', '查询系统日志列表', '20', 'com.scj.common.controller.LogController.list()', null, '58.248.235.233', '2018-08-18 15:57:42');
INSERT INTO `sys_log` VALUES ('1030725810140524546', '1030703515640561666', 'gacl', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '58.248.235.233', '2018-08-18 15:59:18');
INSERT INTO `sys_log` VALUES ('1030725812581609473', '1030703515640561666', 'gacl', '查询定时任务列表', '19', 'com.scj.job.controller.JobController.list()', null, '58.248.235.233', '2018-08-18 15:59:18');
INSERT INTO `sys_log` VALUES ('1030725819686760449', '1030703515640561666', 'gacl', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '58.248.235.233', '2018-08-18 15:59:20');
INSERT INTO `sys_log` VALUES ('1030725820395597826', '1030703515640561666', 'gacl', '查询系统配置列表', '25', 'com.scj.common.controller.ConfigController.list()', null, '58.248.235.233', '2018-08-18 15:59:20');
INSERT INTO `sys_log` VALUES ('1030728680327917570', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '67.198.131.14', '2018-08-18 16:10:42');
INSERT INTO `sys_log` VALUES ('1030729209871380482', '1', 'admin', '登录', '10', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '67.198.131.14', '2018-08-18 16:12:48');
INSERT INTO `sys_log` VALUES ('1030729213080023042', '1', 'admin', '请求访问主页', '29', 'com.scj.sys.controller.LoginController.index()', null, '67.198.131.14', '2018-08-18 16:12:49');
INSERT INTO `sys_log` VALUES ('1030729220340363265', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '67.198.131.14', '2018-08-18 16:12:51');
INSERT INTO `sys_log` VALUES ('1030729240775012354', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '67.198.131.14', '2018-08-18 16:12:56');
INSERT INTO `sys_log` VALUES ('1030729517737488385', '1030703515640561666', 'gacl', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '58.248.235.233', '2018-08-18 16:14:02');
INSERT INTO `sys_log` VALUES ('1030729519054499841', '1030703515640561666', 'gacl', '查询部门树形数据', '5', 'com.scj.sys.controller.DeptController.tree()', null, '58.248.235.233', '2018-08-18 16:14:02');
INSERT INTO `sys_log` VALUES ('1030729519155163137', '1030703515640561666', 'gacl', '查询系统用户列表', '11', 'com.scj.sys.controller.UserController.list()', null, '58.248.235.233', '2018-08-18 16:14:02');
INSERT INTO `sys_log` VALUES ('1030730537137909762', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '67.198.131.14', '2018-08-18 16:18:05');
INSERT INTO `sys_log` VALUES ('1030730539126009857', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '67.198.131.14', '2018-08-18 16:18:05');
INSERT INTO `sys_log` VALUES ('1030730560550514689', '1', 'admin', '查询文件列表', '8', 'com.scj.oss.controller.FileController.list()', null, '67.198.131.14', '2018-08-18 16:18:10');
INSERT INTO `sys_log` VALUES ('1030730573808709634', '1', 'admin', '查询系统配置列表', '11', 'com.scj.common.controller.ConfigController.list()', null, '67.198.131.14', '2018-08-18 16:18:14');
INSERT INTO `sys_log` VALUES ('1030730578141425665', '1', 'admin', '进入个人中心', '41', 'com.scj.sys.controller.UserController.personal()', null, '67.198.131.14', '2018-08-18 16:18:15');
INSERT INTO `sys_log` VALUES ('1030733689077751810', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 16:30:36');
INSERT INTO `sys_log` VALUES ('1030733713782202369', '1', 'admin', '登录', '45', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 16:30:42');
INSERT INTO `sys_log` VALUES ('1030733714583314434', '1', 'admin', '请求访问主页', '104', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 16:30:42');
INSERT INTO `sys_log` VALUES ('1030733715090825217', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:30:42');
INSERT INTO `sys_log` VALUES ('1030733716080680962', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:30:43');
INSERT INTO `sys_log` VALUES ('1030733728835559425', '1', 'admin', '请求访问主页', '295', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 16:30:46');
INSERT INTO `sys_log` VALUES ('1030733729452122113', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:30:46');
INSERT INTO `sys_log` VALUES ('1030733729955438593', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:30:46');
INSERT INTO `sys_log` VALUES ('1030734192058576898', '1', 'admin', '登录', '37', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 16:32:36');
INSERT INTO `sys_log` VALUES ('1030734194080231426', '1', 'admin', '请求访问主页', '108', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 16:32:37');
INSERT INTO `sys_log` VALUES ('1030734194600325122', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:32:37');
INSERT INTO `sys_log` VALUES ('1030734195426603010', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:32:37');
INSERT INTO `sys_log` VALUES ('1030734384942034946', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 16:33:22');
INSERT INTO `sys_log` VALUES ('1030734386191937537', '1', 'admin', '查询部门树形数据', '17', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 16:33:22');
INSERT INTO `sys_log` VALUES ('1030734386695254018', '1', 'admin', '查询系统用户列表', '92', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 16:33:23');
INSERT INTO `sys_log` VALUES ('1030734390700814337', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 16:33:24');
INSERT INTO `sys_log` VALUES ('1030734391766167554', '1', 'admin', '查询系统角色菜单', '22', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 16:33:24');
INSERT INTO `sys_log` VALUES ('1030734395746562049', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 16:33:25');
INSERT INTO `sys_log` VALUES ('1030734396711251969', '1', 'admin', '查询菜单列表', '22', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 16:33:25');
INSERT INTO `sys_log` VALUES ('1030734404080644097', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 16:33:27');
INSERT INTO `sys_log` VALUES ('1030734405259243522', '1', 'admin', '获取部门列表', '60', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 16:33:27');
INSERT INTO `sys_log` VALUES ('1030734551917277185', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 16:34:02');
INSERT INTO `sys_log` VALUES ('1030734552978436097', '1', 'admin', '查询数据表列表', '19', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 16:34:02');
INSERT INTO `sys_log` VALUES ('1030734557625724929', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 16:34:03');
INSERT INTO `sys_log` VALUES ('1030734558842073090', '1', 'admin', '查询定时任务列表', '56', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 16:34:04');
INSERT INTO `sys_log` VALUES ('1030734569445273601', '1', 'admin', '进入在线用户列表页面', '5', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 16:34:06');
INSERT INTO `sys_log` VALUES ('1030734570254774273', '1', 'admin', '查询在线用户列表数据', '1', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 16:34:06');
INSERT INTO `sys_log` VALUES ('1030734574319054849', '1', 'admin', '进入系统日志列表页面', '1', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 16:34:07');
INSERT INTO `sys_log` VALUES ('1030734575346659329', '1', 'admin', '查询系统日志列表', '32', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 16:34:08');
INSERT INTO `sys_log` VALUES ('1030735224356483073', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 16:36:42');
INSERT INTO `sys_log` VALUES ('1030735225652523009', '1', 'admin', '查询数据字典key列表', '44', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 16:36:43');
INSERT INTO `sys_log` VALUES ('1030735225782546434', '1', 'admin', '查询数据字典列表', '42', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 16:36:43');
INSERT INTO `sys_log` VALUES ('1030735232837365761', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 16:36:44');
INSERT INTO `sys_log` VALUES ('1030735234020159489', '1', 'admin', '查询系统配置列表', '32', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 16:36:45');
INSERT INTO `sys_log` VALUES ('1030735239757967362', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 16:36:46');
INSERT INTO `sys_log` VALUES ('1030735240668131330', '1', 'admin', '查询文件列表', '30', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 16:36:46');
INSERT INTO `sys_log` VALUES ('1030735953058082817', '1', 'admin', '进入文件管理页面', '1', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-18 16:39:36');
INSERT INTO `sys_log` VALUES ('1030735953762725889', '1', 'admin', '查询文件列表', '24', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-18 16:39:36');
INSERT INTO `sys_log` VALUES ('1030735958099636226', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-18 16:39:37');
INSERT INTO `sys_log` VALUES ('1030735959047548930', '1', 'admin', '查询系统配置列表', '28', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-18 16:39:37');
INSERT INTO `sys_log` VALUES ('1030735963820666882', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-18 16:39:39');
INSERT INTO `sys_log` VALUES ('1030735964781162497', '1', 'admin', '查询数据字典key列表', '13', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-18 16:39:39');
INSERT INTO `sys_log` VALUES ('1030735964944740354', '1', 'admin', '查询数据字典列表', '36', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-18 16:39:39');
INSERT INTO `sys_log` VALUES ('1030735984343396354', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 16:39:43');
INSERT INTO `sys_log` VALUES ('1030735986302136321', '1', 'admin', '查询菜单列表', '16', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 16:39:44');
INSERT INTO `sys_log` VALUES ('1030735987724005377', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 16:39:44');
INSERT INTO `sys_log` VALUES ('1030735988424454145', '1', 'admin', '获取部门列表', '10', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 16:39:44');
INSERT INTO `sys_log` VALUES ('1030735998406897665', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 16:39:47');
INSERT INTO `sys_log` VALUES ('1030735999216398338', '1', 'admin', '查询数据表列表', '22', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 16:39:47');
INSERT INTO `sys_log` VALUES ('1030736003268096002', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 16:39:48');
INSERT INTO `sys_log` VALUES ('1030736004056625153', '1', 'admin', '查询定时任务列表', '24', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 16:39:48');
INSERT INTO `sys_log` VALUES ('1030736011761561601', '1', 'admin', '进入在线用户列表页面', '0', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 16:39:50');
INSERT INTO `sys_log` VALUES ('1030736012407484417', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 16:39:50');
INSERT INTO `sys_log` VALUES ('1030736016257855490', '1', 'admin', '进入系统日志列表页面', '0', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 16:39:51');
INSERT INTO `sys_log` VALUES ('1030736017021218818', '1', 'admin', '查询系统日志列表', '30', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 16:39:51');
INSERT INTO `sys_log` VALUES ('1030736387332124674', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 16:41:20');
INSERT INTO `sys_log` VALUES ('1030736421662502914', '1', 'admin', '登录', '14', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 16:41:28');
INSERT INTO `sys_log` VALUES ('1030736422211956737', '1', 'admin', '请求访问主页', '90', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 16:41:28');
INSERT INTO `sys_log` VALUES ('1030736422509752321', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:41:28');
INSERT INTO `sys_log` VALUES ('1030736423071789057', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 16:41:28');
INSERT INTO `sys_log` VALUES ('1030737691320266753', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 16:46:30');
INSERT INTO `sys_log` VALUES ('1030737692154933250', '1', 'admin', '查询部门树形数据', '20', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 16:46:31');
INSERT INTO `sys_log` VALUES ('1030737692289150978', '1', 'admin', '查询系统用户列表', '32', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 16:46:31');
INSERT INTO `sys_log` VALUES ('1030737696974188545', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 16:46:32');
INSERT INTO `sys_log` VALUES ('1030737697930489858', '1', 'admin', '查询系统角色菜单', '3', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 16:46:32');
INSERT INTO `sys_log` VALUES ('1030737701948633090', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 16:46:33');
INSERT INTO `sys_log` VALUES ('1030737702695219202', '1', 'admin', '查询菜单列表', '17', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 16:46:33');
INSERT INTO `sys_log` VALUES ('1030737707543834626', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-18 16:46:34');
INSERT INTO `sys_log` VALUES ('1030737708181368834', '1', 'admin', '获取部门列表', '10', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-18 16:46:34');
INSERT INTO `sys_log` VALUES ('1030740346960621570', '1', 'admin', '进入在线用户列表页面', '0', 'com.scj.sys.controller.SessionController.online()', null, '127.0.0.1', '2018-08-18 16:57:04');
INSERT INTO `sys_log` VALUES ('1030740352153169921', '1', 'admin', '查询在线用户列表数据', '0', 'com.scj.sys.controller.SessionController.list()', null, '127.0.0.1', '2018-08-18 16:57:05');
INSERT INTO `sys_log` VALUES ('1030740358574649346', '1', 'admin', '进入系统日志列表页面', '1', 'com.scj.common.controller.LogController.log()', null, '127.0.0.1', '2018-08-18 16:57:06');
INSERT INTO `sys_log` VALUES ('1030740359426093058', '1', 'admin', '查询系统日志列表', '23', 'com.scj.common.controller.LogController.list()', null, '127.0.0.1', '2018-08-18 16:57:07');
INSERT INTO `sys_log` VALUES ('1030743672997670913', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 17:10:16');
INSERT INTO `sys_log` VALUES ('1030743722112970754', '1030703515640561666', 'gacl', '登录', '100', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 17:10:28');
INSERT INTO `sys_log` VALUES ('1030743723488702465', '1030703515640561666', 'gacl', '请求访问主页', '206', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 17:10:29');
INSERT INTO `sys_log` VALUES ('1030743723987824641', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 17:10:29');
INSERT INTO `sys_log` VALUES ('1030743725170618369', '1030703515640561666', 'gacl', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 17:10:29');
INSERT INTO `sys_log` VALUES ('1030744525796143106', '1030703515640561666', 'gacl', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 17:13:40');
INSERT INTO `sys_log` VALUES ('1030744528279171073', '1030703515640561666', 'gacl', '查询数据表列表', '34', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 17:13:41');
INSERT INTO `sys_log` VALUES ('1030744633564590081', '1030703515640561666', 'gacl', '进入代码生成配置编辑页面', '76', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 17:14:06');
INSERT INTO `sys_log` VALUES ('1030744800640495617', '1030703515640561666', 'gacl', '更新代码生成配置', '916', 'com.scj.generator.controller.GeneratorController.update()', null, '127.0.0.1', '2018-08-18 17:14:45');
INSERT INTO `sys_log` VALUES ('1030744831544127490', '1030703515640561666', 'gacl', '根据数据表生成代码', '425', 'com.scj.generator.controller.GeneratorController.code()', null, '127.0.0.1', '2018-08-18 17:14:53');
INSERT INTO `sys_log` VALUES ('1030753926049296386', '1', 'admin', '登录', '1326', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 17:51:01');
INSERT INTO `sys_log` VALUES ('1030753928180002817', '1', 'admin', '请求访问主页', '444', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 17:51:02');
INSERT INTO `sys_log` VALUES ('1030753928603627522', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 17:51:02');
INSERT INTO `sys_log` VALUES ('1030753929291493377', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 17:51:02');
INSERT INTO `sys_log` VALUES ('1030753959607922690', '1', 'admin', '进入个人中心', '390', 'com.scj.sys.controller.UserController.personal()', null, '127.0.0.1', '2018-08-18 17:51:09');
INSERT INTO `sys_log` VALUES ('1030754012632313858', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-18 17:51:22');
INSERT INTO `sys_log` VALUES ('1030754013840273409', '1', 'admin', '查询部门树形数据', '20', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-18 17:51:22');
INSERT INTO `sys_log` VALUES ('1030754014444253185', '1', 'admin', '查询系统用户列表', '134', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-18 17:51:22');
INSERT INTO `sys_log` VALUES ('1030754020244975617', '1', 'admin', '进入系统角色页面', '1', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-18 17:51:24');
INSERT INTO `sys_log` VALUES ('1030754021188694018', '1', 'admin', '查询系统角色菜单', '20', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-18 17:51:24');
INSERT INTO `sys_log` VALUES ('1030754026393825282', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-18 17:51:25');
INSERT INTO `sys_log` VALUES ('1030754027392069633', '1', 'admin', '查询菜单列表', '41', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-18 17:51:25');
INSERT INTO `sys_log` VALUES ('1030754063978983426', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 17:51:34');
INSERT INTO `sys_log` VALUES ('1030754065300189185', '1', 'admin', '查询数据表列表', '47', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 17:51:34');
INSERT INTO `sys_log` VALUES ('1030754105498398722', '1', 'admin', '进入代码生成配置编辑页面', '32', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 17:51:44');
INSERT INTO `sys_log` VALUES ('1030754189296398338', '1', 'admin', '更新代码生成配置', '3098', 'com.scj.generator.controller.GeneratorController.update()', null, '127.0.0.1', '2018-08-18 17:52:04');
INSERT INTO `sys_log` VALUES ('1030754249363025922', '1', 'admin', '更新代码生成配置', '5728', 'com.scj.generator.controller.GeneratorController.update()', null, '127.0.0.1', '2018-08-18 17:52:18');
INSERT INTO `sys_log` VALUES ('1030754321253396481', '1', 'admin', '进入定时任务管理页面', '1', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 17:52:35');
INSERT INTO `sys_log` VALUES ('1030754328698286081', '1', 'admin', '查询定时任务列表', '742', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 17:52:37');
INSERT INTO `sys_log` VALUES ('1030754539579502593', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-18 17:53:27');
INSERT INTO `sys_log` VALUES ('1030754540586135554', '1', 'admin', '查询定时任务列表', '61', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-18 17:53:28');
INSERT INTO `sys_log` VALUES ('1030754545719963649', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 17:53:29');
INSERT INTO `sys_log` VALUES ('1030754546487521282', '1', 'admin', '查询数据表列表', '13', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 17:53:29');
INSERT INTO `sys_log` VALUES ('1030754584903151618', '1', 'admin', '进入代码生成配置编辑页面', '68', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 17:53:38');
INSERT INTO `sys_log` VALUES ('1030754628888817665', '1', 'admin', '更新代码生成配置', '2725', 'com.scj.generator.controller.GeneratorController.update()', null, '127.0.0.1', '2018-08-18 17:53:49');
INSERT INTO `sys_log` VALUES ('1030754646290984962', '1', 'admin', '根据数据表生成代码', '210', 'com.scj.generator.controller.GeneratorController.code()', null, '127.0.0.1', '2018-08-18 17:53:53');
INSERT INTO `sys_log` VALUES ('1030794851878219777', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '61.92.27.239', '2018-08-18 20:33:39');
INSERT INTO `sys_log` VALUES ('1030845211816800257', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-18 23:53:45');
INSERT INTO `sys_log` VALUES ('1030845248391131138', '1', 'admin', '登录', '357', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-18 23:53:54');
INSERT INTO `sys_log` VALUES ('1030845260693024769', '1', 'admin', '请求访问主页', '2239', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-18 23:53:57');
INSERT INTO `sys_log` VALUES ('1030845264614699010', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 23:53:58');
INSERT INTO `sys_log` VALUES ('1030845265885573121', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-18 23:53:58');
INSERT INTO `sys_log` VALUES ('1030845302111776769', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-18 23:54:07');
INSERT INTO `sys_log` VALUES ('1030845309577637890', '1', 'admin', '查询数据表列表', '426', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-18 23:54:09');
INSERT INTO `sys_log` VALUES ('1030845349591298050', '1', 'admin', '进入代码生成配置编辑页面', '386', 'com.scj.generator.controller.GeneratorController.edit()', null, '127.0.0.1', '2018-08-18 23:54:18');
INSERT INTO `sys_log` VALUES ('1030845403127394305', '1', 'admin', '根据数据表生成代码', '1584', 'com.scj.generator.controller.GeneratorController.code()', null, '127.0.0.1', '2018-08-18 23:54:31');
INSERT INTO `sys_log` VALUES ('1030928881802063873', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '196.52.43.92', '2018-08-19 05:26:14');
INSERT INTO `sys_log` VALUES ('1030936504437878785', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '58.182.122.144', '2018-08-19 05:56:31');
INSERT INTO `sys_log` VALUES ('1030984916449374210', '-1', '获取用户信息为空', '重定向到登录', '0', 'com.scj.sys.controller.LoginController.welcome()', null, '127.0.0.1', '2018-08-19 09:08:53');
INSERT INTO `sys_log` VALUES ('1030984955838083074', '1', 'admin', '登录', '180', 'com.scj.sys.controller.LoginController.ajaxLogin()', null, '127.0.0.1', '2018-08-19 09:09:03');
INSERT INTO `sys_log` VALUES ('1030984957553553409', '1', 'admin', '请求访问主页', '290', 'com.scj.sys.controller.LoginController.index()', null, '127.0.0.1', '2018-08-19 09:09:03');
INSERT INTO `sys_log` VALUES ('1030984958337888258', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-19 09:09:04');
INSERT INTO `sys_log` VALUES ('1030984959566819329', '1', 'admin', '主页', '0', 'com.scj.sys.controller.LoginController.main()', null, '127.0.0.1', '2018-08-19 09:09:04');
INSERT INTO `sys_log` VALUES ('1030984990755663873', '1', 'admin', '进入数据字典列表页面', '0', 'com.scj.common.controller.DictController.sysDict()', null, '127.0.0.1', '2018-08-19 09:09:11');
INSERT INTO `sys_log` VALUES ('1030984992789901314', '1', 'admin', '查询数据字典key列表', '149', 'com.scj.common.controller.DictController.listType()', null, '127.0.0.1', '2018-08-19 09:09:12');
INSERT INTO `sys_log` VALUES ('1030984993943334914', '1', 'admin', '查询数据字典列表', '404', 'com.scj.common.controller.DictController.list()', null, '127.0.0.1', '2018-08-19 09:09:12');
INSERT INTO `sys_log` VALUES ('1030985012209528834', '1', 'admin', '进入文件管理页面', '0', 'com.scj.oss.controller.FileController.sysFile()', null, '127.0.0.1', '2018-08-19 09:09:16');
INSERT INTO `sys_log` VALUES ('1030985014315069441', '1', 'admin', '查询文件列表', '69', 'com.scj.oss.controller.FileController.list()', null, '127.0.0.1', '2018-08-19 09:09:17');
INSERT INTO `sys_log` VALUES ('1030985020510056449', '1', 'admin', '进入系统配置页面', '0', 'com.scj.common.controller.ConfigController.Config()', null, '127.0.0.1', '2018-08-19 09:09:18');
INSERT INTO `sys_log` VALUES ('1030985022313607169', '1', 'admin', '查询系统配置列表', '67', 'com.scj.common.controller.ConfigController.list()', null, '127.0.0.1', '2018-08-19 09:09:19');
INSERT INTO `sys_log` VALUES ('1030985030773518338', '1', 'admin', '进入系统用户列表页面', '0', 'com.scj.sys.controller.UserController.user()', null, '127.0.0.1', '2018-08-19 09:09:21');
INSERT INTO `sys_log` VALUES ('1030985031796928513', '1', 'admin', '查询部门树形数据', '17', 'com.scj.sys.controller.DeptController.tree()', null, '127.0.0.1', '2018-08-19 09:09:21');
INSERT INTO `sys_log` VALUES ('1030985032132472833', '1', 'admin', '查询系统用户列表', '64', 'com.scj.sys.controller.UserController.list()', null, '127.0.0.1', '2018-08-19 09:09:21');
INSERT INTO `sys_log` VALUES ('1030985217025781761', '1', 'admin', '进入系统角色页面', '0', 'com.scj.sys.controller.RoleController.role()', null, '127.0.0.1', '2018-08-19 09:10:05');
INSERT INTO `sys_log` VALUES ('1030985219047436290', '1', 'admin', '查询系统角色菜单', '44', 'com.scj.sys.controller.RoleController.list()', null, '127.0.0.1', '2018-08-19 09:10:06');
INSERT INTO `sys_log` VALUES ('1030985224273539074', '1', 'admin', '进入系统菜单页面', '0', 'com.scj.sys.controller.MenuController.menu()', null, '127.0.0.1', '2018-08-19 09:10:07');
INSERT INTO `sys_log` VALUES ('1030985225770905602', '1', 'admin', '查询菜单列表', '126', 'com.scj.sys.controller.MenuController.list()', null, '127.0.0.1', '2018-08-19 09:10:07');
INSERT INTO `sys_log` VALUES ('1030985232058167298', '1', 'admin', '进入部分页面', '0', 'com.scj.sys.controller.DeptController.dept()', null, '127.0.0.1', '2018-08-19 09:10:09');
INSERT INTO `sys_log` VALUES ('1030985233224183810', '1', 'admin', '获取部门列表', '60', 'com.scj.sys.controller.DeptController.list()', null, '127.0.0.1', '2018-08-19 09:10:09');
INSERT INTO `sys_log` VALUES ('1030985266241744898', '1', 'admin', '进入代码生成页面', '0', 'com.scj.generator.controller.GeneratorController.generator()', null, '127.0.0.1', '2018-08-19 09:10:17');
INSERT INTO `sys_log` VALUES ('1030985269026762754', '1', 'admin', '查询数据表列表', '114', 'com.scj.generator.controller.GeneratorController.list()', null, '127.0.0.1', '2018-08-19 09:10:18');
INSERT INTO `sys_log` VALUES ('1030985988781912065', '1', 'admin', '进入定时任务管理页面', '0', 'com.scj.job.controller.JobController.taskScheduleJob()', null, '127.0.0.1', '2018-08-19 09:13:09');
INSERT INTO `sys_log` VALUES ('1030985991092973569', '1', 'admin', '查询定时任务列表', '62', 'com.scj.job.controller.JobController.list()', null, '127.0.0.1', '2018-08-19 09:13:10');
INSERT INTO `sys_log` VALUES ('1030986111205257218', '1', 'admin', '进入配置编辑页面', '21', 'com.scj.common.controller.ConfigController.edit()', null, '127.0.0.1', '2018-08-19 09:13:38');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentId` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030656647233560579 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '基础管理', '', '', '0', 'fa fa-bars', '0', '2017-08-09 22:49:47', null);
INSERT INTO `sys_menu` VALUES ('2', '3', '系统菜单', 'sys/menu/', 'sys:menu:menu', '1', 'fa fa-th-list', '2', '2017-08-09 22:55:15', null);
INSERT INTO `sys_menu` VALUES ('3', '0', '系统管理', null, null, '0', 'fa fa-desktop', '1', '2017-08-09 23:06:55', '2017-08-14 14:13:43');
INSERT INTO `sys_menu` VALUES ('6', '3', '用户管理', 'sys/user/', 'sys:user:user', '1', 'fa fa-user', '0', '2017-08-10 14:12:11', null);
INSERT INTO `sys_menu` VALUES ('7', '3', '角色管理', 'sys/role', 'sys:role:role', '1', 'fa fa-paw', '1', '2017-08-10 14:13:19', null);
INSERT INTO `sys_menu` VALUES ('12', '6', '新增', '', 'sys:user:add', '2', '', '0', '2017-08-14 10:51:35', null);
INSERT INTO `sys_menu` VALUES ('13', '6', '编辑', '', 'sys:user:edit', '2', '', '0', '2017-08-14 10:52:06', null);
INSERT INTO `sys_menu` VALUES ('14', '6', '删除', null, 'sys:user:remove', '2', null, '0', '2017-08-14 10:52:24', null);
INSERT INTO `sys_menu` VALUES ('15', '7', '新增', '', 'sys:role:add', '2', '', '0', '2017-08-14 10:56:37', null);
INSERT INTO `sys_menu` VALUES ('20', '2', '新增', '', 'sys:menu:add', '2', '', '0', '2017-08-14 10:59:32', null);
INSERT INTO `sys_menu` VALUES ('21', '2', '编辑', '', 'sys:menu:edit', '2', '', '0', '2017-08-14 10:59:56', null);
INSERT INTO `sys_menu` VALUES ('22', '2', '删除', '', 'sys:menu:remove', '2', '', '0', '2017-08-14 11:00:26', null);
INSERT INTO `sys_menu` VALUES ('24', '6', '批量删除', '', 'sys:user:batchRemove', '2', '', '0', '2017-08-14 17:27:18', null);
INSERT INTO `sys_menu` VALUES ('25', '6', '停用', null, 'sys:user:disable', '2', null, '0', '2017-08-14 17:27:43', null);
INSERT INTO `sys_menu` VALUES ('26', '6', '重置密码', '', 'sys:user:resetPwd', '2', '', '0', '2017-08-14 17:28:34', null);
INSERT INTO `sys_menu` VALUES ('27', '91', '系统日志', 'common/log', 'common:log', '1', 'fa fa-warning', '0', '2017-08-14 22:11:53', null);
INSERT INTO `sys_menu` VALUES ('28', '27', '刷新', null, 'sys:log:list', '2', null, '0', '2017-08-14 22:30:22', null);
INSERT INTO `sys_menu` VALUES ('29', '27', '删除', null, 'sys:log:remove', '2', null, '0', '2017-08-14 22:30:43', null);
INSERT INTO `sys_menu` VALUES ('30', '27', '清空', null, 'sys:log:clear', '2', null, '0', '2017-08-14 22:31:02', null);
INSERT INTO `sys_menu` VALUES ('48', '77', '代码生成', 'common/generator', 'common:generator', '1', 'fa fa-code', '3', null, null);
INSERT INTO `sys_menu` VALUES ('55', '7', '编辑', '', 'sys:role:edit', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('56', '7', '删除', '', 'sys:role:remove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('57', '91', '运行监控', '/druid/index.html', '', '1', 'fa fa-caret-square-o-right', '1', null, null);
INSERT INTO `sys_menu` VALUES ('61', '2', '批量删除', '', 'sys:menu:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('62', '7', '批量删除', '', 'sys:role:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('71', '1', '文件管理', '/common/sysFile', 'oss:file:file', '1', 'fa fa-folder-open', '2', null, null);
INSERT INTO `sys_menu` VALUES ('72', '77', '计划任务', 'common/job', 'common:taskScheduleJob', '1', 'fa fa-hourglass-1', '4', null, null);
INSERT INTO `sys_menu` VALUES ('73', '3', '部门管理', '/sys/dept', 'system:sysDept:sysDept', '1', 'fa fa-users', '3', null, null);
INSERT INTO `sys_menu` VALUES ('74', '73', '增加', '/sys/dept/add', 'system:sysDept:add', '2', null, '1', null, null);
INSERT INTO `sys_menu` VALUES ('75', '73', '刪除', 'sys/dept/remove', 'system:sysDept:remove', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('76', '73', '编辑', '/sys/dept/edit', 'system:sysDept:edit', '2', null, '3', null, null);
INSERT INTO `sys_menu` VALUES ('77', '0', '系统工具', '', '', '0', 'fa fa-gear', '4', null, null);
INSERT INTO `sys_menu` VALUES ('78', '1', '数据字典', '/common/sysDict', 'common:sysDict:sysDict', '1', 'fa fa-book', '1', null, null);
INSERT INTO `sys_menu` VALUES ('79', '78', '增加', '/common/sysDict/add', 'common:sysDict:add', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('80', '78', '编辑', '/common/sysDict/edit', 'common:sysDict:edit', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('81', '78', '删除', '/common/sysDict/remove', 'common:sysDict:remove', '2', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('83', '78', '批量删除', '/common/sysDict/batchRemove', 'common:sysDict:batchRemove', '2', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('91', '0', '系统监控', '', '', '0', 'fa fa-video-camera', '5', null, null);
INSERT INTO `sys_menu` VALUES ('92', '91', '在线用户', 'sys/online', '', '1', 'fa fa-user', null, null, null);
INSERT INTO `sys_menu` VALUES ('175', '1', '系统配置', '/common/config', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('176', '175', '查看', null, 'common:config:config', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('177', '175', '新增', null, 'common:config:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('178', '175', '修改', null, 'common:config:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('179', '175', '删除', null, 'common:config:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('180', '175', '批量删除', null, 'common:config:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('193', '71', '增加', '/common/sysFile/add', 'oss:file:add', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('194', '71', '列表', '/common/sysFile/list', 'oss:file:list', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('195', '71', '编辑', '/common/sysFile/edit', 'oss:file:update', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('196', '71', '查询', '/common/sysFile/info', 'oss:file:info', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('197', '71', '删除', '/common/sysFile/remove', 'oss:file:remove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('1030656647233560578', '2', '三级菜单', 'http://www.mycodes.net/183/9655.htm', 'sys:resourceReuestMapping:view', '1', 'fa fa-battery-empty', '12', null, null);

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `roleSign` varchar(100) DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `userIdCreate` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030707194535981059 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级用户角色', 'admin', '超级管理员', '2', '2017-08-12 00:43:52', '2017-08-12 19:14:59');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menuId` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4508 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('367', '44', '1');
INSERT INTO `sys_role_menu` VALUES ('368', '44', '32');
INSERT INTO `sys_role_menu` VALUES ('369', '44', '33');
INSERT INTO `sys_role_menu` VALUES ('370', '44', '34');
INSERT INTO `sys_role_menu` VALUES ('371', '44', '35');
INSERT INTO `sys_role_menu` VALUES ('372', '44', '28');
INSERT INTO `sys_role_menu` VALUES ('373', '44', '29');
INSERT INTO `sys_role_menu` VALUES ('374', '44', '30');
INSERT INTO `sys_role_menu` VALUES ('375', '44', '38');
INSERT INTO `sys_role_menu` VALUES ('376', '44', '4');
INSERT INTO `sys_role_menu` VALUES ('377', '44', '27');
INSERT INTO `sys_role_menu` VALUES ('378', '45', '38');
INSERT INTO `sys_role_menu` VALUES ('379', '46', '3');
INSERT INTO `sys_role_menu` VALUES ('380', '46', '20');
INSERT INTO `sys_role_menu` VALUES ('381', '46', '21');
INSERT INTO `sys_role_menu` VALUES ('382', '46', '22');
INSERT INTO `sys_role_menu` VALUES ('383', '46', '23');
INSERT INTO `sys_role_menu` VALUES ('384', '46', '11');
INSERT INTO `sys_role_menu` VALUES ('385', '46', '12');
INSERT INTO `sys_role_menu` VALUES ('386', '46', '13');
INSERT INTO `sys_role_menu` VALUES ('387', '46', '14');
INSERT INTO `sys_role_menu` VALUES ('388', '46', '24');
INSERT INTO `sys_role_menu` VALUES ('389', '46', '25');
INSERT INTO `sys_role_menu` VALUES ('390', '46', '26');
INSERT INTO `sys_role_menu` VALUES ('391', '46', '15');
INSERT INTO `sys_role_menu` VALUES ('392', '46', '2');
INSERT INTO `sys_role_menu` VALUES ('393', '46', '6');
INSERT INTO `sys_role_menu` VALUES ('394', '46', '7');
INSERT INTO `sys_role_menu` VALUES ('598', '50', '38');
INSERT INTO `sys_role_menu` VALUES ('632', '38', '42');
INSERT INTO `sys_role_menu` VALUES ('737', '51', '38');
INSERT INTO `sys_role_menu` VALUES ('738', '51', '39');
INSERT INTO `sys_role_menu` VALUES ('739', '51', '40');
INSERT INTO `sys_role_menu` VALUES ('740', '51', '41');
INSERT INTO `sys_role_menu` VALUES ('741', '51', '4');
INSERT INTO `sys_role_menu` VALUES ('742', '51', '32');
INSERT INTO `sys_role_menu` VALUES ('743', '51', '33');
INSERT INTO `sys_role_menu` VALUES ('744', '51', '34');
INSERT INTO `sys_role_menu` VALUES ('745', '51', '35');
INSERT INTO `sys_role_menu` VALUES ('746', '51', '27');
INSERT INTO `sys_role_menu` VALUES ('747', '51', '28');
INSERT INTO `sys_role_menu` VALUES ('748', '51', '29');
INSERT INTO `sys_role_menu` VALUES ('749', '51', '30');
INSERT INTO `sys_role_menu` VALUES ('750', '51', '1');
INSERT INTO `sys_role_menu` VALUES ('1064', '54', '53');
INSERT INTO `sys_role_menu` VALUES ('1095', '55', '2');
INSERT INTO `sys_role_menu` VALUES ('1096', '55', '6');
INSERT INTO `sys_role_menu` VALUES ('1097', '55', '7');
INSERT INTO `sys_role_menu` VALUES ('1098', '55', '3');
INSERT INTO `sys_role_menu` VALUES ('1099', '55', '50');
INSERT INTO `sys_role_menu` VALUES ('1100', '55', '49');
INSERT INTO `sys_role_menu` VALUES ('1101', '55', '1');
INSERT INTO `sys_role_menu` VALUES ('1856', '53', '28');
INSERT INTO `sys_role_menu` VALUES ('1857', '53', '29');
INSERT INTO `sys_role_menu` VALUES ('1858', '53', '30');
INSERT INTO `sys_role_menu` VALUES ('1859', '53', '27');
INSERT INTO `sys_role_menu` VALUES ('1860', '53', '57');
INSERT INTO `sys_role_menu` VALUES ('1861', '53', '71');
INSERT INTO `sys_role_menu` VALUES ('1862', '53', '48');
INSERT INTO `sys_role_menu` VALUES ('1863', '53', '72');
INSERT INTO `sys_role_menu` VALUES ('1864', '53', '1');
INSERT INTO `sys_role_menu` VALUES ('1865', '53', '7');
INSERT INTO `sys_role_menu` VALUES ('1866', '53', '55');
INSERT INTO `sys_role_menu` VALUES ('1867', '53', '56');
INSERT INTO `sys_role_menu` VALUES ('1868', '53', '62');
INSERT INTO `sys_role_menu` VALUES ('1869', '53', '15');
INSERT INTO `sys_role_menu` VALUES ('1870', '53', '2');
INSERT INTO `sys_role_menu` VALUES ('1871', '53', '61');
INSERT INTO `sys_role_menu` VALUES ('1872', '53', '20');
INSERT INTO `sys_role_menu` VALUES ('1873', '53', '21');
INSERT INTO `sys_role_menu` VALUES ('1874', '53', '22');
INSERT INTO `sys_role_menu` VALUES ('2247', '63', '-1');
INSERT INTO `sys_role_menu` VALUES ('2248', '63', '84');
INSERT INTO `sys_role_menu` VALUES ('2249', '63', '85');
INSERT INTO `sys_role_menu` VALUES ('2250', '63', '88');
INSERT INTO `sys_role_menu` VALUES ('2251', '63', '87');
INSERT INTO `sys_role_menu` VALUES ('2252', '64', '84');
INSERT INTO `sys_role_menu` VALUES ('2253', '64', '89');
INSERT INTO `sys_role_menu` VALUES ('2254', '64', '88');
INSERT INTO `sys_role_menu` VALUES ('2255', '64', '87');
INSERT INTO `sys_role_menu` VALUES ('2256', '64', '86');
INSERT INTO `sys_role_menu` VALUES ('2257', '64', '85');
INSERT INTO `sys_role_menu` VALUES ('2258', '65', '89');
INSERT INTO `sys_role_menu` VALUES ('2259', '65', '88');
INSERT INTO `sys_role_menu` VALUES ('2260', '65', '86');
INSERT INTO `sys_role_menu` VALUES ('2262', '67', '48');
INSERT INTO `sys_role_menu` VALUES ('2263', '68', '88');
INSERT INTO `sys_role_menu` VALUES ('2264', '68', '87');
INSERT INTO `sys_role_menu` VALUES ('2265', '69', '89');
INSERT INTO `sys_role_menu` VALUES ('2266', '69', '88');
INSERT INTO `sys_role_menu` VALUES ('2267', '69', '86');
INSERT INTO `sys_role_menu` VALUES ('2268', '69', '87');
INSERT INTO `sys_role_menu` VALUES ('2269', '69', '85');
INSERT INTO `sys_role_menu` VALUES ('2270', '69', '84');
INSERT INTO `sys_role_menu` VALUES ('2271', '70', '85');
INSERT INTO `sys_role_menu` VALUES ('2272', '70', '89');
INSERT INTO `sys_role_menu` VALUES ('2273', '70', '88');
INSERT INTO `sys_role_menu` VALUES ('2274', '70', '87');
INSERT INTO `sys_role_menu` VALUES ('2275', '70', '86');
INSERT INTO `sys_role_menu` VALUES ('2276', '70', '84');
INSERT INTO `sys_role_menu` VALUES ('2277', '71', '87');
INSERT INTO `sys_role_menu` VALUES ('2278', '72', '59');
INSERT INTO `sys_role_menu` VALUES ('2279', '73', '48');
INSERT INTO `sys_role_menu` VALUES ('2280', '74', '88');
INSERT INTO `sys_role_menu` VALUES ('2281', '74', '87');
INSERT INTO `sys_role_menu` VALUES ('2282', '75', '88');
INSERT INTO `sys_role_menu` VALUES ('2283', '75', '87');
INSERT INTO `sys_role_menu` VALUES ('2284', '76', '85');
INSERT INTO `sys_role_menu` VALUES ('2285', '76', '89');
INSERT INTO `sys_role_menu` VALUES ('2286', '76', '88');
INSERT INTO `sys_role_menu` VALUES ('2287', '76', '87');
INSERT INTO `sys_role_menu` VALUES ('2288', '76', '86');
INSERT INTO `sys_role_menu` VALUES ('2289', '76', '84');
INSERT INTO `sys_role_menu` VALUES ('2292', '78', '88');
INSERT INTO `sys_role_menu` VALUES ('2293', '78', '87');
INSERT INTO `sys_role_menu` VALUES ('2294', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2295', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2296', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2308', '80', '87');
INSERT INTO `sys_role_menu` VALUES ('2309', '80', '86');
INSERT INTO `sys_role_menu` VALUES ('2310', '80', '-1');
INSERT INTO `sys_role_menu` VALUES ('2311', '80', '84');
INSERT INTO `sys_role_menu` VALUES ('2312', '80', '85');
INSERT INTO `sys_role_menu` VALUES ('2328', '79', '72');
INSERT INTO `sys_role_menu` VALUES ('2329', '79', '48');
INSERT INTO `sys_role_menu` VALUES ('2330', '79', '77');
INSERT INTO `sys_role_menu` VALUES ('2331', '79', '84');
INSERT INTO `sys_role_menu` VALUES ('2332', '79', '89');
INSERT INTO `sys_role_menu` VALUES ('2333', '79', '88');
INSERT INTO `sys_role_menu` VALUES ('2334', '79', '87');
INSERT INTO `sys_role_menu` VALUES ('2335', '79', '86');
INSERT INTO `sys_role_menu` VALUES ('2336', '79', '85');
INSERT INTO `sys_role_menu` VALUES ('2337', '79', '-1');
INSERT INTO `sys_role_menu` VALUES ('2338', '77', '89');
INSERT INTO `sys_role_menu` VALUES ('2339', '77', '88');
INSERT INTO `sys_role_menu` VALUES ('2340', '77', '87');
INSERT INTO `sys_role_menu` VALUES ('2341', '77', '86');
INSERT INTO `sys_role_menu` VALUES ('2342', '77', '85');
INSERT INTO `sys_role_menu` VALUES ('2343', '77', '84');
INSERT INTO `sys_role_menu` VALUES ('2344', '77', '72');
INSERT INTO `sys_role_menu` VALUES ('2345', '77', '-1');
INSERT INTO `sys_role_menu` VALUES ('2346', '77', '77');
INSERT INTO `sys_role_menu` VALUES ('4359', '1', '-1');
INSERT INTO `sys_role_menu` VALUES ('4360', '1', '193');
INSERT INTO `sys_role_menu` VALUES ('4361', '1', '194');
INSERT INTO `sys_role_menu` VALUES ('4362', '1', '195');
INSERT INTO `sys_role_menu` VALUES ('4363', '1', '196');
INSERT INTO `sys_role_menu` VALUES ('4364', '1', '197');
INSERT INTO `sys_role_menu` VALUES ('4365', '1', '79');
INSERT INTO `sys_role_menu` VALUES ('4366', '1', '80');
INSERT INTO `sys_role_menu` VALUES ('4367', '1', '81');
INSERT INTO `sys_role_menu` VALUES ('4368', '1', '83');
INSERT INTO `sys_role_menu` VALUES ('4369', '1', '152');
INSERT INTO `sys_role_menu` VALUES ('4370', '1', '153');
INSERT INTO `sys_role_menu` VALUES ('4371', '1', '154');
INSERT INTO `sys_role_menu` VALUES ('4372', '1', '155');
INSERT INTO `sys_role_menu` VALUES ('4373', '1', '156');
INSERT INTO `sys_role_menu` VALUES ('4374', '1', '158');
INSERT INTO `sys_role_menu` VALUES ('4375', '1', '159');
INSERT INTO `sys_role_menu` VALUES ('4376', '1', '160');
INSERT INTO `sys_role_menu` VALUES ('4377', '1', '161');
INSERT INTO `sys_role_menu` VALUES ('4378', '1', '162');
INSERT INTO `sys_role_menu` VALUES ('4379', '1', '164');
INSERT INTO `sys_role_menu` VALUES ('4380', '1', '165');
INSERT INTO `sys_role_menu` VALUES ('4381', '1', '166');
INSERT INTO `sys_role_menu` VALUES ('4382', '1', '167');
INSERT INTO `sys_role_menu` VALUES ('4383', '1', '168');
INSERT INTO `sys_role_menu` VALUES ('4384', '1', '170');
INSERT INTO `sys_role_menu` VALUES ('4385', '1', '171');
INSERT INTO `sys_role_menu` VALUES ('4386', '1', '172');
INSERT INTO `sys_role_menu` VALUES ('4387', '1', '173');
INSERT INTO `sys_role_menu` VALUES ('4388', '1', '174');
INSERT INTO `sys_role_menu` VALUES ('4389', '1', '176');
INSERT INTO `sys_role_menu` VALUES ('4390', '1', '177');
INSERT INTO `sys_role_menu` VALUES ('4391', '1', '178');
INSERT INTO `sys_role_menu` VALUES ('4392', '1', '179');
INSERT INTO `sys_role_menu` VALUES ('4393', '1', '180');
INSERT INTO `sys_role_menu` VALUES ('4394', '1', '182');
INSERT INTO `sys_role_menu` VALUES ('4395', '1', '183');
INSERT INTO `sys_role_menu` VALUES ('4396', '1', '184');
INSERT INTO `sys_role_menu` VALUES ('4397', '1', '185');
INSERT INTO `sys_role_menu` VALUES ('4398', '1', '186');
INSERT INTO `sys_role_menu` VALUES ('4399', '1', '188');
INSERT INTO `sys_role_menu` VALUES ('4400', '1', '189');
INSERT INTO `sys_role_menu` VALUES ('4401', '1', '190');
INSERT INTO `sys_role_menu` VALUES ('4402', '1', '191');
INSERT INTO `sys_role_menu` VALUES ('4403', '1', '192');
INSERT INTO `sys_role_menu` VALUES ('4404', '1', '205');
INSERT INTO `sys_role_menu` VALUES ('4405', '1', '181');
INSERT INTO `sys_role_menu` VALUES ('4406', '1', '187');
INSERT INTO `sys_role_menu` VALUES ('4407', '1', '71');
INSERT INTO `sys_role_menu` VALUES ('4408', '1', '78');
INSERT INTO `sys_role_menu` VALUES ('4409', '1', '151');
INSERT INTO `sys_role_menu` VALUES ('4410', '1', '157');
INSERT INTO `sys_role_menu` VALUES ('4411', '1', '163');
INSERT INTO `sys_role_menu` VALUES ('4412', '1', '169');
INSERT INTO `sys_role_menu` VALUES ('4413', '1', '175');
INSERT INTO `sys_role_menu` VALUES ('4414', '1', '199');
INSERT INTO `sys_role_menu` VALUES ('4415', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('4416', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('4417', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('4418', '1', '61');
INSERT INTO `sys_role_menu` VALUES ('4419', '1', '1030656647233560578');
INSERT INTO `sys_role_menu` VALUES ('4420', '1', '12');
INSERT INTO `sys_role_menu` VALUES ('4421', '1', '13');
INSERT INTO `sys_role_menu` VALUES ('4422', '1', '14');
INSERT INTO `sys_role_menu` VALUES ('4423', '1', '24');
INSERT INTO `sys_role_menu` VALUES ('4424', '1', '25');
INSERT INTO `sys_role_menu` VALUES ('4425', '1', '26');
INSERT INTO `sys_role_menu` VALUES ('4426', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('4427', '1', '55');
INSERT INTO `sys_role_menu` VALUES ('4428', '1', '56');
INSERT INTO `sys_role_menu` VALUES ('4429', '1', '62');
INSERT INTO `sys_role_menu` VALUES ('4430', '1', '74');
INSERT INTO `sys_role_menu` VALUES ('4431', '1', '75');
INSERT INTO `sys_role_menu` VALUES ('4432', '1', '76');
INSERT INTO `sys_role_menu` VALUES ('4433', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('4434', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('4435', '1', '7');
INSERT INTO `sys_role_menu` VALUES ('4436', '1', '73');
INSERT INTO `sys_role_menu` VALUES ('4437', '1', '48');
INSERT INTO `sys_role_menu` VALUES ('4438', '1', '72');
INSERT INTO `sys_role_menu` VALUES ('4439', '1', '28');
INSERT INTO `sys_role_menu` VALUES ('4440', '1', '29');
INSERT INTO `sys_role_menu` VALUES ('4441', '1', '30');
INSERT INTO `sys_role_menu` VALUES ('4442', '1', '27');
INSERT INTO `sys_role_menu` VALUES ('4443', '1', '57');
INSERT INTO `sys_role_menu` VALUES ('4444', '1', '92');
INSERT INTO `sys_role_menu` VALUES ('4445', '1', '98');
INSERT INTO `sys_role_menu` VALUES ('4446', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('4447', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('4448', '1', '77');
INSERT INTO `sys_role_menu` VALUES ('4449', '1', '91');
INSERT INTO `sys_role_menu` VALUES ('4450', '1', '97');

-- ----------------------------
-- Table structure for `sys_task`
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cronExpression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `methodName` varchar(255) DEFAULT NULL COMMENT '任务调用的方法名',
  `isConcurrent` varchar(255) DEFAULT NULL COMMENT '任务是否有状态',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `updateBy` varchar(64) DEFAULT NULL COMMENT '更新者',
  `beanClass` varchar(255) DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  `jobStatus` varchar(255) DEFAULT NULL COMMENT '任务状态',
  `jobGroup` varchar(255) DEFAULT NULL COMMENT '任务分组',
  `updateDate` datetime DEFAULT NULL COMMENT '更新时间',
  `createBy` varchar(64) DEFAULT NULL COMMENT '创建者',
  `springBean` varchar(255) DEFAULT NULL COMMENT 'Spring bean',
  `jobName` varchar(255) DEFAULT NULL COMMENT '任务名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1020572889410367491 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_task
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `deptId` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 0:禁用，1:正常',
  `userIdCreate` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(32) DEFAULT NULL COMMENT '性别',
  `birth` datetime DEFAULT NULL COMMENT '出身日期',
  `picId` bigint(32) DEFAULT NULL,
  `liveAddress` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '爱好',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030703515640561667 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '超级管理员', '33808479d49ca8a3cdc93d4f976d1e3d', '6', '290603672@qq.com', '15277778888', '1', '1', '2017-08-15 21:40:39', '2017-08-15 21:41:00', '96', '2018-04-02 00:00:00', '151', '广东省广州市天河区', '', '广东省', '广州市', '天河区');
INSERT INTO `sys_user` VALUES ('1030703515640561666', 'gacl', '孤傲苍狼', '1d928ef01658c8c7d257af477df52e47', '6', '290603672@qq.com', null, '1', null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `roleId` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('73', '30', '48');
INSERT INTO `sys_user_role` VALUES ('74', '30', '49');
INSERT INTO `sys_user_role` VALUES ('75', '30', '50');
INSERT INTO `sys_user_role` VALUES ('76', '31', '48');
INSERT INTO `sys_user_role` VALUES ('77', '31', '49');
INSERT INTO `sys_user_role` VALUES ('78', '31', '52');
INSERT INTO `sys_user_role` VALUES ('79', '32', '48');
INSERT INTO `sys_user_role` VALUES ('80', '32', '49');
INSERT INTO `sys_user_role` VALUES ('81', '32', '50');
INSERT INTO `sys_user_role` VALUES ('82', '32', '51');
INSERT INTO `sys_user_role` VALUES ('83', '32', '52');
INSERT INTO `sys_user_role` VALUES ('84', '33', '38');
INSERT INTO `sys_user_role` VALUES ('85', '33', '49');
INSERT INTO `sys_user_role` VALUES ('86', '33', '52');
INSERT INTO `sys_user_role` VALUES ('87', '34', '50');
INSERT INTO `sys_user_role` VALUES ('88', '34', '51');
INSERT INTO `sys_user_role` VALUES ('89', '34', '52');
INSERT INTO `sys_user_role` VALUES ('106', '124', '1');
INSERT INTO `sys_user_role` VALUES ('111', '2', '1');
INSERT INTO `sys_user_role` VALUES ('117', '135', '1');
INSERT INTO `sys_user_role` VALUES ('120', '134', '1');
INSERT INTO `sys_user_role` VALUES ('121', '134', '48');
INSERT INTO `sys_user_role` VALUES ('125', '132', '52');
INSERT INTO `sys_user_role` VALUES ('126', '132', '49');
INSERT INTO `sys_user_role` VALUES ('127', null, '1');
INSERT INTO `sys_user_role` VALUES ('128', null, '1');
INSERT INTO `sys_user_role` VALUES ('129', null, '1');
INSERT INTO `sys_user_role` VALUES ('132', '1', '1');
INSERT INTO `sys_user_role` VALUES ('133', '1030703515640561666', '1');

-- ----------------------------
-- Table structure for `wx_mp_user`
-- ----------------------------
DROP TABLE IF EXISTS `wx_mp_user`;
CREATE TABLE `wx_mp_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `open_id` varchar(255) DEFAULT NULL COMMENT '微信用户的openid',
  `nickname` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '昵称',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别',
  `language` varchar(255) DEFAULT NULL COMMENT '母语',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `province` varchar(255) DEFAULT NULL COMMENT '所在省份',
  `country` varchar(255) DEFAULT NULL COMMENT '所在国家',
  `headImgUrl` varchar(255) DEFAULT NULL COMMENT '微信头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `union_id` varchar(255) DEFAULT NULL COMMENT '如果开发者拥有多个移动应用、网站应用和公众帐号，可通过获取用户基本信息中的unionid来区分用户的唯一性，因为只要是同一个微信开放平台帐号下的移动应用、网站应用和公众帐号，用户的unionid是唯一的。换句话说，同一用户，对同一个微信开放平台下的不同应用，unionid是相同的。 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信小程序用户信息表';

-- ----------------------------
-- Records of wx_mp_user
-- ----------------------------
