 
#主要技术架构
SpringBoot+mybatisPlus+MySQL5.7+JDK1.8
# 技术选型
1.后端
 * 核心框架：Spring Boot
 * 安全框架：Apache Shiro
 * 视图框架：Spring MVC
 * 服务端验证：Hibernate Validator
 * 任务调度：Quartz
 * 持久层框架：Mybatis、Mybatis plus
 * 数据库连接池：Alibaba Druid
 * 缓存框架：Ehcache
 * 日志管理：SLF4J、Log4j
 * 工具类：Apache Commons、Jackson、Xstream、

2.前端

 * JS框架：jQuery。
 * CSS框架：Twitter Bootstrap
 * 数据表格：bootstrap table
 * 对话框：layer
 * 树结构控件：jQuery zTree
 * 日期控件： datepicker

# 目录结构说明
```
├─ main
|   │  
|   ├─ java
|   │   │
|   │   └─ com.scj ----------------主代码
|   │             │    
|   │             ├─ api ----------------基于jwt实现的api模块
|   │             │
|   │             ├─ common ----------------核心依赖模块
|   │             │    
|   │             ├─ generator ----------------代码生成器模块
|   │             │    
|   │             ├─ job ----------------定时任务模块
|   │             │    
|   │             ├─ shiro ----------------权限模块
|   │             │    
|   │             ├─ oss ----------------对象存储模块
|   │             │    
|   │             ├─ sys ----------------系统基础功能模块
|   │             │    
|   │             ├─ Application.Java ---------------- 启动入口类
|   │
|   ├─resources----------------资源文件
|         │
|         ├─ config ----------------缓存配置（ehcache.xml缓存配置文件,启动banner等）
|         │ 
|         ├─ mapper ------------------- mybatis 的mapper文件
|         │ 
|         ├─ public ------------- 静态资源文件，css，js
|         │ 
|         ├─ static ----- 静态资源文件，css，js
|         │ 
|         ├─ templates ---- 页面模板
|         │ 
|         ├─ application*.yml------ 项目配置文件
|
├─────────common---------------公共模块
|
├─────────wechat-api --------------- 微信服务模块
│ 
├─generator  代码生成器
│        └─resources 
│           ├─mapper   MyBatis文件
│           ├─template 代码生成器模板（可增加或修改相应模板）
│           ├─application.yml    全局配置文件
│           └─generator.properties   代码生成器，配置文件
│
```
#启动部署  