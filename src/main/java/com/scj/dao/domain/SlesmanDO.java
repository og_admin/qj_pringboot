package com.scj.dao.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;



/**
 * 
 * <pre>
 * 业务员表

 * </pre>
 * <small> 2018-08-26 14:30:56 | MiceLife</small>
 */
 @TableName("scj_slesman")
public class SlesmanDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /**  */
    @TableId
    private Integer id;
    /** 姓名 */
    private String name;
    /** 性别 */
    private String sex;
    /** 电话 */
    private String phone;
    /** 是否为区域管理员 */
    private String isAreaAdmin;
    /** 加入时间 */
    private Date timeJoin;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：姓名
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 获取：姓名
     */
    public String getName() {
        return name;
    }
    /**
     * 设置：性别
     */
    public void setSex(String sex) {
        this.sex = sex;
    }
    /**
     * 获取：性别
     */
    public String getSex() {
        return sex;
    }
    /**
     * 设置：电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * 获取：电话
     */
    public String getPhone() {
        return phone;
    }
    /**
     * 设置：是否为区域管理员
     */
    public void setIsAreaAdmin(String isAreaAdmin) {
        this.isAreaAdmin = isAreaAdmin;
    }
    /**
     * 获取：是否为区域管理员
     */
    public String getIsAreaAdmin() {
        return isAreaAdmin;
    }
    /**
     * 设置：加入时间
     */
    public void setTimeJoin(Date timeJoin) {
        this.timeJoin = timeJoin;
    }
    /**
     * 获取：加入时间
     */
    public Date getTimeJoin() {
        return timeJoin;
    }
}
