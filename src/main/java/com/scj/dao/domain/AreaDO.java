package com.scj.dao.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;



/**
 * 
 * <pre>
 * 省市区表
 * </pre>
 * <small> 2018-08-26 02:28:19 | MiceLife</small>
 */
 @TableName("area")
public class AreaDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /** ID */
    @TableId
    private Integer id;
    /** 栏目名 */
    private String areaname;
    /** 父栏目 */
    private Integer parentid;
    /**  */
    private String shortname;
    /**  */
    private String lng;
    /**  */
    private String lat;
    /** 1.省 2.市 3.区 4.镇 */
    private Integer level;
    /**  */
    private String position;
    /** 排序 */
    private Integer sort;

    /**
     * 设置：ID
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：ID
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：栏目名
     */
    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }
    /**
     * 获取：栏目名
     */
    public String getAreaname() {
        return areaname;
    }
    /**
     * 设置：父栏目
     */
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }
    /**
     * 获取：父栏目
     */
    public Integer getParentid() {
        return parentid;
    }
    /**
     * 设置：
     */
    public void setShortname(String shortname) {
        this.shortname = shortname;
    }
    /**
     * 获取：
     */
    public String getShortname() {
        return shortname;
    }
    /**
     * 设置：
     */
    public void setLng(String lng) {
        this.lng = lng;
    }
    /**
     * 获取：
     */
    public String getLng() {
        return lng;
    }
    /**
     * 设置：
     */
    public void setLat(String lat) {
        this.lat = lat;
    }
    /**
     * 获取：
     */
    public String getLat() {
        return lat;
    }
    /**
     * 设置：1.省 2.市 3.区 4.镇
     */
    public void setLevel(Integer level) {
        this.level = level;
    }
    /**
     * 获取：1.省 2.市 3.区 4.镇
     */
    public Integer getLevel() {
        return level;
    }
    /**
     * 设置：
     */
    public void setPosition(String position) {
        this.position = position;
    }
    /**
     * 获取：
     */
    public String getPosition() {
        return position;
    }
    /**
     * 设置：排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }
    /**
     * 获取：排序
     */
    public Integer getSort() {
        return sort;
    }
}
