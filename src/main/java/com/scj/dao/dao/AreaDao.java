package com.scj.dao.dao;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.AreaDO;

/**
 * 
 * <pre>
 * 省市区表
 * </pre>
 * <small> 2018-08-26 02:28:19 | MiceLife</small>
 */
public interface AreaDao extends BaseDao<AreaDO> {

}
