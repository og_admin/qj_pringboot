package com.scj.dao.dao;

import com.scj.dao.domain.SlesmanDO;
import com.scj.common.base.BaseDao;

/**
 * 
 * <pre>
 * 业务员表

 * </pre>
 * <small> 2018-08-26 14:30:56 | MiceLife</small>
 */
public interface SlesmanDao extends BaseDao<SlesmanDO> {

}
