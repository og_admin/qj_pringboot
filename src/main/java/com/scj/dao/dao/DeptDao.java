package com.scj.dao.dao;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.DeptDO;

/**
 * 部门管理
 */
public interface DeptDao extends BaseDao<DeptDO> {
	
	Long[] listParentDept();
	
	int getDeptUserNumber(Long deptId);
}
