package com.scj.dao.dao;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.UserDO;

/**
 *
 */
public interface UserDao extends BaseDao<UserDO> {
	
	Long[] listAllDept();

}
