package com.scj.dao.dao;

import java.util.List;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.MenuDO;

/**
 * 菜单管理
 */
public interface MenuDao extends BaseDao<MenuDO> {
	
	List<MenuDO> listMenuByUserId(Long id);
	
	List<String> listUserPerms(Long id);
}
