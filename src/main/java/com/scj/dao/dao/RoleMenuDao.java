package com.scj.dao.dao;

import java.io.Serializable;
import java.util.List;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.RoleMenuDO;

/**
 * 角色与菜单对应关系
 */
public interface RoleMenuDao extends BaseDao<RoleMenuDO> {
	
	List<Long> listMenuIdByRoleId(Serializable roleId);
	
	int removeByRoleId(Serializable roleId);
	
	int batchSave(List<RoleMenuDO> list);
}
