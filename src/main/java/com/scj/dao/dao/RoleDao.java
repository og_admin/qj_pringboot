package com.scj.dao.dao;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.RoleDO;

/**
 * 角色
 */
public interface RoleDao extends BaseDao<RoleDO> {

}
