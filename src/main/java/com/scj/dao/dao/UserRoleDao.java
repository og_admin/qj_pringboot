package com.scj.dao.dao;

import java.io.Serializable;
import java.util.List;

import com.scj.common.base.BaseDao;
import com.scj.dao.domain.UserRoleDO;

/**
 * 用户与角色对应关系
 */
public interface UserRoleDao extends BaseDao<UserRoleDO> {

	List<Long> listRoleId(Serializable userId);

	int removeByUserId(Serializable userId);

	int batchSave(List<UserRoleDO> list);

	int batchRemoveByUserId(Long[] ids);
}
