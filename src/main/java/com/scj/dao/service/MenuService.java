package com.scj.dao.service;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.scj.common.base.CoreService;
import com.scj.common.domain.Tree;
import com.scj.dao.domain.MenuDO;

/**
 *
 */
@Service
public interface MenuService extends CoreService<MenuDO> {
    Tree<MenuDO> getSysMenuTree(Long id);

    List<Tree<MenuDO>> listMenuTree(Long id);

    Tree<MenuDO> getTree();

    Tree<MenuDO> getTree(Long id);

    Set<String> listPerms(Long userId);
}
