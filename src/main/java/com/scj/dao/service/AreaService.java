package com.scj.dao.service;

import com.scj.common.base.CoreService;
import com.scj.dao.domain.AreaDO;

/**
 * 
 * <pre>
 * 省市区表
 * </pre>
 * <small> 2018-08-26 02:28:19 | MiceLife</small>
 */
public interface AreaService extends CoreService<AreaDO> {
    
}
