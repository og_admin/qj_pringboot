package com.scj.dao.service;

import com.baomidou.mybatisplus.service.IService;
import com.scj.dao.domain.UserRoleDO;

import java.io.Serializable;
import java.util.List;

/**
 * @author runnable@sina.cn
 */
public interface UserRoleService extends IService<UserRoleDO> {
    List<Long> listRoleId(Serializable id);

    int removeByUserId(Long userId);

    int batchSave(List<UserRoleDO> list);

    int removeByUserId(Serializable userId);

}
