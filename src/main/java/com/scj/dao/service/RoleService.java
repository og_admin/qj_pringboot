package com.scj.dao.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import com.scj.common.base.CoreService;
import com.scj.dao.domain.RoleDO;

/**
 *
 */
@Service
public interface RoleService extends CoreService<RoleDO> {
    List<RoleDO> findAll();
    List<RoleDO> findListByUserId(Serializable id);
}
