package com.scj.dao.service;

import com.scj.common.base.CoreService;
import com.scj.common.domain.Tree;
import com.scj.dao.domain.DeptDO;

/**
 * 部门管理
 */
public interface DeptService extends CoreService<DeptDO> {
    
	Tree<DeptDO> getTree();
	
	boolean checkDeptHasUser(Long deptId);

    Long[] listParentDept();
}
