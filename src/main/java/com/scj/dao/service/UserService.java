package com.scj.dao.service;

import java.util.Map;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.scj.common.base.CoreService;
import com.scj.common.domain.Tree;
import com.scj.dao.domain.DeptDO;
import com.scj.dao.domain.UserDO;
import com.scj.dao.vo.UserVO;

/**
 *
 */
public interface UserService extends CoreService<UserDO> {

    boolean exit(Map<String, Object> params);

    Set<String> listRoles(Long userId);

    int resetPwd(UserVO userVO, UserDO userDO);

    int adminResetPwd(UserVO userVO);

    Tree<DeptDO> getTree();

    /**
     * 更新个人信息
     * 
     * @param userDO
     * @return
     */
    int updatePersonal(UserDO userDO);

    /**
     * 更新个人图片
     * 
     * @param file
     *            图片
     * @param avatar_data
     *            裁剪信息
     * @param userId
     *            用户ID
     * @throws Exception
     */
    Map<String, Object> updatePersonalImg(MultipartFile file, String avatar_data, Long userId) throws Exception;
}
