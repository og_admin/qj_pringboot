package com.scj.dao.service.impl;

import org.springframework.stereotype.Service;

import com.scj.common.base.CoreServiceImpl;
import com.scj.dao.dao.AreaDao;
import com.scj.dao.domain.AreaDO;
import com.scj.dao.service.AreaService;

/**
 * 
 * <pre>
 * 省市区表
 * </pre>
 * <small> 2018-08-26 02:28:19 | MiceLife</small>
 */
@Service
public class AreaServiceImpl extends CoreServiceImpl<AreaDao, AreaDO> implements AreaService {

}
