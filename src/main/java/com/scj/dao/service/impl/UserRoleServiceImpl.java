package com.scj.dao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.scj.dao.dao.UserRoleDao;
import com.scj.dao.domain.UserRoleDO;
import com.scj.dao.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author runnable@sina.cn
 */
@Service("userRoleService")
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRoleDO> implements UserRoleService {
    @Override
    public List<Long> listRoleId(Serializable id) {
        return baseMapper.listRoleId(id);
    }

    @Override
    public int removeByUserId(Long userId) {
        return baseMapper.removeByUserId(userId);
    }

    @Override
    public int batchSave(List<UserRoleDO> list) {
        return baseMapper.batchSave(list);
    }

    @Override
    public int removeByUserId(Serializable userId) {
        return baseMapper.removeByUserId(userId);
    }
}
