package com.scj.dao.controller;


import java.awt.geom.Area;
import java.util.Arrays;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.HttpContextUtils;
import com.scj.common.utils.Result;
import com.scj.dao.domain.AreaDO;
import com.scj.dao.service.AreaService;

/**
 * 
 * <pre>
 * 省市区表
 * </pre>
 * <small> 2018-08-26 02:28:19 | MiceLife</small>
 */
@Controller
@RequestMapping("/scj/area")
public class AreaController extends AbstractController {
	@Autowired
	private AreaService areaService;
	
	@GetMapping()
	@RequiresPermissions("scj:area:area")
	String Area(){
	    return "sys/area/area";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("scj:area:area")
	public Result<Page<AreaDO>> list(AreaDO areaDTO){
        Wrapper<AreaDO> wrapper = new EntityWrapper<AreaDO>(areaDTO);
        Page<AreaDO> page = areaService.selectPage(getPage(AreaDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/getByParentId")
	@ResponseBody
	@RequiresPermissions("scj:area:query")
	List<AreaDO> getByParentId(AreaDO areaDTO){
		String parentId = HttpContextUtils.getHttpServletRequest().getParameter("parentId");
		if(parentId==null || Integer.parseInt(parentId)<0){
			parentId="1";
//			设置中国
			areaDTO.setParentid(1);
		}
		Wrapper<AreaDO> wrapper = new EntityWrapper<AreaDO>(areaDTO);
		wrapper.eq("parentid", Integer.parseInt(parentId));
		List<AreaDO> areaDOS = areaService.selectList(wrapper);
		return  areaDOS;
	}

}
