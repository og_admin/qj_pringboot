package com.scj.company.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.CompanyDO;
import com.scj.api.service.CompanyService;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.Result;
import com.scj.common.service.SlesmanService;
import com.scj.dao.domain.SlesmanDO;
import com.scj.oss.service.FileService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * <pre>
 * 公司信息
 * </pre>
 * <small> 2018-08-25 22:46:14 | MiceLife</small>
 */
@Controller("com.scj.company.controller.CompanyController")
@RequestMapping("/scj/company")
public class CompanyController extends AbstractController {
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SlesmanService slesmanService;

	
	@GetMapping()
	@RequiresPermissions("scj:company:company")
	String Company(){
	    return "scj/company/company";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("scj:company:company")
	public Result<Page<CompanyDO>> list(CompanyDO companyDTO){
        Wrapper<CompanyDO> wrapper = new EntityWrapper<CompanyDO>(companyDTO);
		if(companyDTO.getName()!=null){

			wrapper.like("name",companyDTO.getName()).or().like("contacts",companyDTO.getName());
			if(isNumeric(companyDTO.getName())){
				wrapper.or().like("phone",companyDTO.getName());
			}
			companyDTO.setName(null);
		}
        Page<CompanyDO> page = companyService.selectPage(getPage(CompanyDO.class), wrapper);
		List<CompanyDO> list=new ArrayList<>();
		for (CompanyDO companyDO : page.getRecords()) {
			SlesmanDO slesmanDO = slesmanService.selectById(companyDO.getSalesmanId());
			companyDO.setSalesmanDetails(slesmanDO.getName()+"("+slesmanDO.getPhone()+")");
			list.add(companyDO);
		}
		page.setRecords(list);
        return Result.ok(page);
	}

	public boolean isNumeric(String str){
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if( !isNum.matches() ){
			return false;
		}
		return true;
	}

	
	@GetMapping("/add")
	@RequiresPermissions("scj:company:add")
	String add(){
	    return "scj/company/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("scj:company:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		CompanyDO company = companyService.selectById(id);
		model.addAttribute("company", company);
	    return "scj/company/edit";
	}

	@GetMapping("/detail/{id}")
	String detail(@PathVariable("id") Integer id,Model model){
		CompanyDO company = companyService.selectById(id);
		model.addAttribute("company", company);
		return "scj/company/detail";
	}

	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("scj:company:add")
	public Result<String> save( CompanyDO company){
		companyService.insert(company);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("scj:company:edit")
	public Result<String>  update( CompanyDO company){
		boolean b = companyService.updateById(company);
		return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("scj:company:remove")
	public Result<String>  remove( Integer id){
		companyService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("scj:company:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		companyService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}

}
