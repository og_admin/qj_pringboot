package com.scj.dept;

import com.scj.common.annotation.Log;
import com.scj.common.base.AbstractController;
import com.scj.common.domain.Tree;
import com.scj.dept.domain.DepartmentDO;
import com.scj.dept.service.BackendDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *    @author 侠梦  823547749@qq.com
 *  * @date 2018/8/26 20:38
 *  * @description 后台部门管理相关接口
 */
@Controller
@RequestMapping("backendDept")
public class BackendDeptController extends AbstractController {

    @Autowired
    private BackendDeptService backendDeptService;

    @GetMapping("/tree")
    @ResponseBody
    @Log("查询后台部门树形数据")
    public Tree<DepartmentDO> tree() {
        Tree<DepartmentDO> tree = backendDeptService.getTree();
        return tree;
    }
}
