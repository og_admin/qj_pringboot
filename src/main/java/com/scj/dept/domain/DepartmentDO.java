package com.scj.dept.domain;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 *    @author 侠梦  823547749@qq.com
 *  * @date 2018/8/26 22:38
 *  * @description 部门实体
 */
@TableName("scj_department")
public class DepartmentDO {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    // 上级部门ID，一级部门为0
    private Long parentId;
    // 部门名称
    private String name;

    private String departmentHeads;

    private String telephone;

    private String companyId;


    public String getDepartmentHeads() {
        return departmentHeads;
    }

    public void setDepartmentHeads(String departmentHeads) {
        this.departmentHeads = departmentHeads;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * 设置：上级部门ID，一级部门为0
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取：上级部门ID，一级部门为0
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 设置：部门名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取：部门名称
     */
    public String getName() {
        return name;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
