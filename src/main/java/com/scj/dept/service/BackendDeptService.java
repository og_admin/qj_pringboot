package com.scj.dept.service;

import com.scj.common.base.CoreService;
import com.scj.common.domain.Tree;
import com.scj.dept.domain.DepartmentDO;

public interface BackendDeptService extends CoreService<DepartmentDO> {
    Tree<DepartmentDO> getTree();
}
