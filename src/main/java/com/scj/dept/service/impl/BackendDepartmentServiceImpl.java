package com.scj.dept.service.impl;

import com.scj.common.base.CoreServiceImpl;
import com.scj.common.domain.Tree;
import com.scj.common.utils.BuildTree;
import com.scj.dept.dao.BackendDeptmentDao;
import com.scj.dept.domain.DepartmentDO;
import com.scj.dept.service.BackendDeptService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BackendDepartmentServiceImpl  extends CoreServiceImpl<BackendDeptmentDao, DepartmentDO> implements BackendDeptService {
    @Override
    public Tree<DepartmentDO> getTree() {
        List<Tree<DepartmentDO>> trees = new ArrayList<>();
        List<DepartmentDO> sysDepts = baseMapper.selectList(null);
        for (DepartmentDO sysDept : sysDepts) {
            Tree<DepartmentDO> tree = new Tree<>();
            tree.setId(sysDept.getId().toString());
            tree.setParentId(sysDept.getParentId().toString());
            tree.setText(sysDept.getName());
            Map<String, Object> state = new HashMap<>(16);
            state.put("opened", true);
            tree.setState(state);
            trees.add(tree);
        }
        Tree<DepartmentDO> t = BuildTree.build(trees);
        return t;
    }
}
