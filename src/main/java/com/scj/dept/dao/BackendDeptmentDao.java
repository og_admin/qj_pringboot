package com.scj.dept.dao;

import com.scj.common.base.BaseDao;
import com.scj.dept.domain.DepartmentDO;

public interface BackendDeptmentDao extends BaseDao<DepartmentDO> {
}
