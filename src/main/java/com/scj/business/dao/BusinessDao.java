package com.scj.business.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import com.scj.business.domain.BusinessDO;
import com.scj.business.vo.BusinessQueryVO;
import com.scj.common.base.BaseDao;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-25 21:32:04 | Burning</small>
 */
@Repository
public interface BusinessDao extends BaseDao<BusinessDO> {

    /**
     * 自定义列表分页查询
     * @return
     */
    List<BusinessDO> selectByPageSelf(BusinessQueryVO businessQueryVO);

    /**
     * 自定义列表查询总数
     * @return
     */
    Integer countByPageSelf(BusinessQueryVO businessQueryVO);

    /**
     * 查询详细信息
     */
    BusinessDO selectBusinessDetails(int id);

}
