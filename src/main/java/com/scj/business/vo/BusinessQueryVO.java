package com.scj.business.vo;

import java.io.Serializable;

import com.scj.business.domain.BusinessDO;

/**
 * 商家查询VO
 * @author : MiceLife
 * @date : 2018/8/26 0026
 */
public class BusinessQueryVO implements Serializable {

    private String name;
    private Integer provinceId;
    private Integer cityId;
    private Integer areaId;

    private Integer offset;
    private Integer limit;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
