package com.scj.business.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;



/**
 *
 * <pre>
 *
 * </pre>
 * <small> 2018-08-25 21:32:04 | Burning</small>
 */
@TableName("scj_business")
public class BusinessDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;
    /** 门店地理位置 */
    private String address;
    /** 省 */
    private Integer provinceId;
    /** 市 */
    private Integer cityId;
    /** 区 */
    private Integer areaId;
    /** 名称 */
    private String name;
    /** 联系人 */
    private String contacts;
    /** 公司坐标 */
    private String coordinate;
    /** 门店的logo */
    private String logo;
    /** 门店信息说明 */
    private String info;
    /** 联系电话 */
    private String phone;
    /** 卫生许可证 */
    private String hygieneLicense;
    /** 营业执照 */
    private String businessLicense;
    /** 业务员id */
    private Integer salesmanId;

    @TableField(exist = false)
    private String slesmanPhone;
    @TableField(exist = false)
    private String slesmanName;

    @DateTimeFormat(pattern="yyyy-dd-MM mm:hh:ss")
    private Date enterDate;

    private Integer status;
    private LocalDateTime endDate;
    private LocalDateTime startDate;



    public String getSlesmanPhone() {
        return slesmanPhone;
    }

    public void setSlesmanPhone(String slesmanPhone) {
        this.slesmanPhone = slesmanPhone;
    }

    public String getSlesmanName() {
        return slesmanName;
    }

    public void setSlesmanName(String slesmanName) {
        this.slesmanName = slesmanName;
    }

    public Date getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(Date enterDate) {
        this.enterDate = enterDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：门店地理位置
     */
    public void setAddress(String address) {
        this.address = address;
    }
    /**
     * 获取：门店地理位置
     */
    public String getAddress() {
        return address;
    }
    /**
     * 设置：省
     */
    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }
    /**
     * 获取：省
     */
    public Integer getProvinceId() {
        return provinceId;
    }
    /**
     * 设置：市
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    /**
     * 获取：市
     */
    public Integer getCityId() {
        return cityId;
    }
    /**
     * 设置：区
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
    /**
     * 获取：区
     */
    public Integer getAreaId() {
        return areaId;
    }
    /**
     * 设置：名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 获取：名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置：联系人
     */
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
    /**
     * 获取：联系人
     */
    public String getContacts() {
        return contacts;
    }
    /**
     * 设置：公司坐标
     */
    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }
    /**
     * 获取：公司坐标
     */
    public String getCoordinate() {
        return coordinate;
    }
    /**
     * 设置：门店的logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }
    /**
     * 获取：门店的logo
     */
    public String getLogo() {
        return logo;
    }
    /**
     * 设置：门店信息说明
     */
    public void setInfo(String info) {
        this.info = info;
    }
    /**
     * 获取：门店信息说明
     */
    public String getInfo() {
        return info;
    }
    /**
     * 设置：联系电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * 获取：联系电话
     */
    public String getPhone() {
        return phone;
    }
    /**
     * 设置：卫生许可证
     */
    public void setHygieneLicense(String hygieneLicense) {
        this.hygieneLicense = hygieneLicense;
    }
    /**
     * 获取：卫生许可证
     */
    public String getHygieneLicense() {
        return hygieneLicense;
    }
    /**
     * 设置：营业执照
     */
    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }
    /**
     * 获取：营业执照
     */
    public String getBusinessLicense() {
        return businessLicense;
    }
    /**
     * 设置：业务员id
     */
    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }
    /**
     * 获取：业务员id
     */
    public Integer getSalesmanId() {
        return salesmanId;
    }
}
