package com.scj.business.controller;


import java.util.Arrays;
import java.util.List;

import com.scj.business.domain.BusinessDO;
import com.scj.business.service.BusinessService;
import com.scj.business.vo.BusinessQueryVO;
import com.scj.common.base.AbstractController;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.scj.common.utils.HttpContextUtils;
import com.scj.common.utils.Result;

/**
 * <pre>
 *
 * </pre>
 * <small> 2018-08-25 21:32:04 | Burning</small>
 */
@Controller
@RequestMapping("/scj/business")
public class BusinessController extends AbstractController {
	@Autowired
	private BusinessService businessService;

	@GetMapping()
	@RequiresPermissions("scj:business:business")
	String Business() {
		return "/business/business";
	}

	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("scj:business:business")
	public Result<Page<BusinessDO>> list() {
//        Wrapper<BusinessDO> wrapper = new EntityWrapper<BusinessDO>(businessDTO);
//		wrapper.like("");
//        Page<BusinessDO> page = businessService.selectPage(getPage(BusinessDO.class), wrapper);
		Page<BusinessDO> page = getPage(BusinessDO.class);

		BusinessQueryVO businessQueryVO = new BusinessQueryVO();

		businessQueryVO.setOffset(page.getOffset());
		businessQueryVO.setLimit(page.getLimit());
		String name = HttpContextUtils.getHttpServletRequest().getParameter("name");
		if (StringUtils.isNotBlank(name)) {
			businessQueryVO.setName(name);
		}
		String provinceId = HttpContextUtils.getHttpServletRequest().getParameter("provinceId");
		if (StringUtils.isNotBlank(provinceId)) {
			businessQueryVO.setProvinceId(Integer.parseInt(provinceId));
		}
		String cityId = HttpContextUtils.getHttpServletRequest().getParameter("cityId");
		if (StringUtils.isNotBlank(cityId)) {
			businessQueryVO.setCityId(Integer.parseInt(cityId));
		}
		String areaId = HttpContextUtils.getHttpServletRequest().getParameter("areaId");
		if (StringUtils.isNotBlank(areaId)) {
			businessQueryVO.setAreaId(Integer.parseInt(areaId));
		}

		List<BusinessDO> businessDOList = businessService.selectByPageSelf(businessQueryVO);
		page.setRecords(businessDOList);
		page.setTotal(businessService.countByPageSelf(businessQueryVO));
		return Result.ok(page);
	}

	@GetMapping("/add")
	@RequiresPermissions("scj:business:add")
	String add() {
		return "/business/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("scj:business:edit")
	String edit(@PathVariable("id") Integer id, Model model) {
		BusinessDO business = businessService.selectById(id);
		model.addAttribute("business", business);
		return "/business/edit";
	}

	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("scj:business:add")
	public Result<String> save(BusinessDO business) {
		businessService.insert(business);
		return Result.ok();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("scj:business:edit")
	public Result<String> update(BusinessDO business) {
		businessService.updateById(business);
		return Result.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ResponseBody
	@RequiresPermissions("scj:business:remove")
	public Result<String> remove(Integer id) {
		businessService.deleteById(id);
		return Result.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/batchRemove")
	@ResponseBody
	@RequiresPermissions("scj:business:batchRemove")
	public Result<String> remove(@RequestParam("ids[]") Integer[] ids) {
		businessService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}

	@GetMapping("/details/{id}")
	String details(@PathVariable("id") Integer id,Model model){
		BusinessDO business = businessService.selectBusinessDetails(id);
		model.addAttribute("business", business);
		return "/business/details";
	}

}
