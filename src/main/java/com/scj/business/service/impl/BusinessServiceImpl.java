package com.scj.business.service.impl;

import java.util.List;

import com.scj.business.dao.BusinessDao;
import com.scj.business.domain.BusinessDO;
import com.scj.business.service.BusinessService;
import com.scj.business.vo.BusinessQueryVO;
import com.scj.common.base.CoreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <pre>
 *
 * </pre>
 * <small> 2018-08-25 21:32:04 | Burning</small>
 */
@Service
public class BusinessServiceImpl extends CoreServiceImpl<BusinessDao, BusinessDO> implements BusinessService
{
    @Autowired
    BusinessDao businessDao;

    @Override
    public BusinessDO selectBusinessDetails(int id)
    {
        return businessDao.selectBusinessDetails(id);
    }
    @Override
    public List<BusinessDO> selectByPageSelf(BusinessQueryVO businessQueryVO) {
        return businessDao.selectByPageSelf(businessQueryVO);
    }

    @Override
    public Integer countByPageSelf(BusinessQueryVO businessQueryVO) {
        return businessDao.countByPageSelf(businessQueryVO);
    }
}
