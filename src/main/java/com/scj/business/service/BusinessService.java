package com.scj.business.service;


import java.util.List;

import com.scj.business.domain.BusinessDO;
import com.scj.business.vo.BusinessQueryVO;
import com.scj.common.base.CoreService;

/**
 *
 * <pre>
 *
 * </pre>
 * <small> 2018-08-25 21:32:04 | Burning</small>
 */
public interface BusinessService extends CoreService<BusinessDO> {
    BusinessDO selectBusinessDetails(int id);

    List<BusinessDO> selectByPageSelf(BusinessQueryVO businessQueryVO);

    Integer countByPageSelf(BusinessQueryVO businessQueryVO);
}
