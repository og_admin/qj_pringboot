package com.scj.staff.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.service.StaffService;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.Result;
import com.scj.staff.pojo.dto.StaffDTO;
import com.scj.staff.pojo.vo.StaffVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *    @author 侠梦  823547749@qq.com
 *  * @date 2018/8/26 20:38
 *  * @description 后台员工管理相关接口
 */
@RequestMapping("/scj/staff")
@Controller
public class BackendStaffController extends AbstractController {
    @Autowired
    private StaffService staffService;
    private String prefix = "staff";

    @GetMapping()
    @RequiresPermissions("scj:staff:staff")
    String Staff(){
        return prefix + "/staff";
    }

    @RequestMapping("list")
    @ResponseBody
    public Result<Page<StaffVo>> staffList(StaffDTO staffDto){
        Page<StaffVo> page = new Page<>();
        try {
             page = staffService.queryStaffList(staffDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok(page);
    }




}
