package com.scj.order;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.OrderDO;
import com.scj.api.pojo.vo.OrderSearchVO;
import com.scj.api.pojo.vo.OrderVO;
import com.scj.api.service.OrderService;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author kun
 * @DATETIME 2018/8/26 下午10:06
 * TODO
 */
@Controller("com.scj.order.controller.OrderController")
@RequestMapping("/scj/order")
public class OrderController extends AbstractController {
    @Autowired
    private OrderService orderService;
    @ResponseBody
    @GetMapping("/order")
    @RequiresPermissions("scj:order:order")
    public Result<Page<OrderVO>> list(OrderSearchVO searchVO){
        Page<OrderVO> page = orderService.selectByPage(getPage(OrderVO.class),searchVO);
        return Result.ok(page);

    }
}
