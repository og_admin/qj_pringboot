package com.scj.common.service;

import org.springframework.stereotype.Service;

import com.scj.common.base.CoreService;
import com.scj.common.domain.LogDO;

/**
 *
 */
@Service
public interface LogService extends CoreService<LogDO> {
    
}
