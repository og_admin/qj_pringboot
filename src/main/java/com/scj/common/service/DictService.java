package com.scj.common.service;

import java.util.List;

import com.scj.common.base.CoreService;
import com.scj.common.domain.DictDO;
import com.scj.dao.domain.UserDO;

/**
 * 数据字典
 */
public interface DictService extends CoreService<DictDO> {
    
    List<DictDO> listType();

    String getName(String type, String value);

    /**
     * 获取爱好列表
     * 
     * @return
     * @param userDO
     */
    List<DictDO> getHobbyList(UserDO userDO);

    /**
     * 获取性别列表
     * 
     * @return
     */
    List<DictDO> getSexList();
}
