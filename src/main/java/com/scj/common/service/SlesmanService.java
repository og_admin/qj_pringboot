package com.scj.common.service;

import com.scj.dao.domain.SlesmanDO;
import com.scj.common.base.CoreService;

/**
 * 
 * <pre>
 * 业务员表

 * </pre>
 * <small> 2018-08-26 14:30:56 | MiceLife</small>
 */
public interface SlesmanService extends CoreService<SlesmanDO> {
    
}
