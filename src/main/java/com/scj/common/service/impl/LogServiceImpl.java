package com.scj.common.service.impl;

import org.springframework.stereotype.Service;

import com.scj.common.base.CoreServiceImpl;
import com.scj.common.dao.LogDao;
import com.scj.common.domain.LogDO;
import com.scj.common.service.LogService;

/**
 *
 */
@Service
public class LogServiceImpl extends CoreServiceImpl<LogDao, LogDO> implements LogService {

}
