package com.scj.common.service.impl;

import org.springframework.stereotype.Service;

import com.scj.dao.dao.SlesmanDao;
import com.scj.dao.domain.SlesmanDO;
import com.scj.common.service.SlesmanService;
import com.scj.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 业务员表

 * </pre>
 * <small> 2018-08-26 14:30:56 | MiceLife</small>
 */
@Service
public class SlesmanServiceImpl extends CoreServiceImpl<SlesmanDao, SlesmanDO> implements SlesmanService {

}
