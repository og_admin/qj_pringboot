package com.scj.common.service;

import com.scj.common.base.CoreService;
import com.scj.common.domain.ConfigDO;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface ConfigService extends CoreService<ConfigDO> {
    ConfigDO getByKey(String k);

    String getValuByKey(String k);
    
    void updateKV(Map<String, String> kv);
    
    List<ConfigDO> findListByKvType(int kvType);
}
