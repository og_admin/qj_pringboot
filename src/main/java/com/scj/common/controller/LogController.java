package com.scj.common.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.scj.common.annotation.Log;
import com.scj.common.base.AbstractController;
import com.scj.common.domain.LogDO;
import com.scj.common.service.LogService;
import com.scj.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 日志
 */
@RequestMapping("/common/log")
@Controller
public class LogController extends AbstractController {
    @Autowired
    LogService logService;
    String prefix = "common/log";
    
    @Log("进入系统日志列表页面")
    @GetMapping()
    String log() {
        return prefix + "/log";
    }
    
    @Log("查询系统日志列表")
    @ResponseBody
    @GetMapping("/list")
    public Result<Page<LogDO>> list(LogDO logDTO) {
        // 查询列表数据
        Page<LogDO> page = logService.selectPage(getPage(LogDO.class), logService.convertToEntityWrapper("username", logDTO.getUsername(), "operation", logDTO.getOperation()));
        return Result.ok(page);
    }
    
    @Log("删除系统日志")
    @ResponseBody
    @PostMapping("/remove")
    Result<String> remove(Long id) {
        logService.deleteById(id);
        return Result.ok();
    }
    
    @Log("批量删除系统日志")
    @ResponseBody
    @PostMapping("/batchRemove")
    Result<String> batchRemove(@RequestParam("ids[]") Long[] ids) {
        logService.deleteBatchIds(Arrays.asList(ids));
        return Result.fail();
    }
}
