package com.scj.common.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 *
 * @param <T>
 */
public interface BaseDao<T> extends BaseMapper<T> {

}
