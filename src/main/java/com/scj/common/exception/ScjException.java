package com.scj.common.exception;

/**
 * 应用运行时异常，统一抛此类
 */
public class ScjException extends RuntimeException {

    private static final long serialVersionUID = 6403925731816439878L;

    public ScjException() {
        super();
    }

    public ScjException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ScjException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScjException(String message) {
        super(message);
    }

    public ScjException(Throwable cause) {
        super(cause);
    }

}
