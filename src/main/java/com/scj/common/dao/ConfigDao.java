package com.scj.common.dao;

import com.scj.common.base.BaseDao;
import com.scj.common.domain.ConfigDO;

/**
 * 
 *
 */
public interface ConfigDao extends BaseDao<ConfigDO> {

}
