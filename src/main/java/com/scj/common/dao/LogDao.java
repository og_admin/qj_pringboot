package com.scj.common.dao;

import com.scj.common.base.BaseDao;
import com.scj.common.domain.LogDO;

/**
 * 系统日志
 *
 */
public interface LogDao extends BaseDao<LogDO> {
}
