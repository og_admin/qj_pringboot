package com.scj.common.dao;

import java.util.List;

import com.scj.common.base.BaseDao;
import com.scj.common.domain.DictDO;

/**
 * 字典表
 *
 */
public interface DictDao extends BaseDao<DictDO>{

    List<DictDO> listType();
    
}
