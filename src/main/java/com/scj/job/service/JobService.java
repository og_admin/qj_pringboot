package com.scj.job.service;

import org.quartz.SchedulerException;

import com.scj.common.base.CoreService;
import com.scj.job.domain.TaskDO;


/**
 * 定时任务
 */
public interface JobService extends CoreService<TaskDO> {
	
	void initSchedule() throws SchedulerException;

	void changeStatus(Long jobId, String cmd) throws SchedulerException;

	void updateCron(Long jobId) throws SchedulerException;
}
