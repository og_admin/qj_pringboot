package com.scj.job.dao;

import com.scj.common.base.BaseDao;
import com.scj.job.domain.TaskDO;

/**
 *
 */
public interface TaskDao extends BaseDao<TaskDO> {
    
}
