package com.scj.oss.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scj.common.base.CoreServiceImpl;
import com.scj.common.config.ScjConfig;
import com.scj.common.utils.DateUtils;
import com.scj.common.utils.FileType;
import com.scj.oss.dao.FileDao;
import com.scj.oss.domain.FileDO;
import com.scj.oss.sdk.QiNiuOSSService;
import com.scj.oss.service.FileService;

/**
 *
 */
@Service
public class FileServiceImpl extends CoreServiceImpl<FileDao, FileDO> implements FileService {

    @Autowired
    private ScjConfig ifastConfig;
    @Autowired
    private QiNiuOSSService qiNiuOSS;

    @Override
    public String upload(byte[] uploadBytes, String fileName) {
        fileName = fileName.substring(0, fileName.indexOf(".")) + "-" + System.currentTimeMillis() + fileName.substring(fileName.indexOf("."));
        fileName = ifastConfig.getProjectName() + "/" + DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN_8)
                + "/" + fileName;
        String url = qiNiuOSS.upload(uploadBytes, fileName);
        FileDO sysFile = new FileDO(FileType.fileType(fileName), url, new Date());
        super.insert(sysFile);
        return url;
    }
}
