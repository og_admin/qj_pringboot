package com.scj.oss.dao;

import com.scj.common.base.BaseDao;
import com.scj.oss.domain.FileDO;

/**
 * 文件上传
 *
 */
public interface FileDao extends BaseDao<FileDO> {

}
