package com.scj.salesman.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.Result;
import com.scj.dao.domain.SlesmanDO;
import com.scj.common.service.SlesmanService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * <pre>
 * 业务员表

 * </pre>
 * <small> 2018-08-26 14:30:56 | MiceLife</small>
 */
@Controller
@RequestMapping("/scj/slesman")
public class SlesmanController extends AbstractController {
	@Autowired
	private SlesmanService slesmanService;
	
	@GetMapping()
	@RequiresPermissions("scj:slesman:slesman")
	String Slesman(){
	    return "scj/slesman/slesman";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("scj:slesman:slesman")
	public Result<Page<SlesmanDO>> list(SlesmanDO slesmanDTO){
        Wrapper<SlesmanDO> wrapper = new EntityWrapper<SlesmanDO>(slesmanDTO);
        Page<SlesmanDO> page = slesmanService.selectPage(getPage(SlesmanDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("scj:slesman:add")
	String add(){
	    return "scj/slesman/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("scj:slesman:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		SlesmanDO slesman = slesmanService.selectById(id);
		model.addAttribute("slesman", slesman);
	    return "scj/slesman/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("scj:slesman:add")
	public Result<String> save( SlesmanDO slesman){
		slesmanService.insert(slesman);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("scj:slesman:edit")
	public Result<String>  update( SlesmanDO slesman){
		slesmanService.updateById(slesman);
		return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("scj:slesman:remove")
	public Result<String>  remove( Integer id){
		slesmanService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("scj:slesman:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		slesmanService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}

    /**
     * 业务人员id和姓名
     */
    @GetMapping("/getSlesman")
    @ResponseBody
    public List<SlesmanDO> getSlesman(){
        Wrapper<SlesmanDO> wrapper = new EntityWrapper<SlesmanDO>();
		List<SlesmanDO> slesmanDOS = slesmanService.selectList(wrapper);
		return slesmanDOS;
    }
	
}
