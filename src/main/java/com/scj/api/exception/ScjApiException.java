package com.scj.api.exception;

import com.scj.common.exception.ScjException;

/**
 * API异常基类
 *
 */
public class ScjApiException extends ScjException {

    private static final long serialVersionUID = -4891641110275580161L;

    public ScjApiException() {
        super();
    }

    public ScjApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ScjApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScjApiException(String message) {
        super(message);
    }

    public ScjApiException(Throwable cause) {
        super(cause);
    }

}
