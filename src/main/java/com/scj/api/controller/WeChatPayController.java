package com.scj.api.controller;

import com.scj.api.service.WeChatPayService;
import com.scj.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author Toy
 * @date 2018/8/26 下午4:44
 */

@RestController
@RequestMapping("/api/weChat/")
public class WeChatPayController{

    @Autowired
    WeChatPayService weChatPayService;

    @GetMapping("getOpenId")
    public R getOpenId(@RequestParam("code")String code){
        return  weChatPayService.getOpenId(code);
    }

    @PostMapping ("doPay")
    public R doPay(@RequestParam Map<String, Object> params,HttpServletRequest request) {
            return  weChatPayService.doPay(params,request);
    }

    @PostMapping ("notify")
    public void callBack(HttpServletRequest request, HttpServletResponse response){
        weChatPayService.callBack(request,response);
    }

}
