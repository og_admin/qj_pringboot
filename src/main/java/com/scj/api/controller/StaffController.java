package com.scj.api.controller;

import com.scj.api.pojo.domain.StaffDo;
import com.scj.api.service.StaffService;
import com.scj.common.base.AbstractController;
import com.scj.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author runnable@sina.cn
 */
@RestController
@RequestMapping("/api/staff/")
public class StaffController extends AbstractController {

    @Autowired
    private StaffService service;

    @PostMapping("getStaffInfo")
    public R info(@RequestParam Map<String, Object> params) {
        if (!params.containsKey("name")) {
            return R.error("请传递员工姓名!");
        }

        StaffDo entity = service.queryInfo(params);
        if (entity == null) {
            return R.error("该员工不存在该公司!");
        }
        return R.ok().put("entity", entity);
    }
}
