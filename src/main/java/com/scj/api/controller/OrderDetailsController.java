package com.scj.api.controller;

import com.scj.api.pojo.domain.OderWithGoodsDO;
import com.scj.api.service.OrderService;
import com.scj.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tom on 2018/8/23.
 */
@RestController
@RequestMapping("/api/orderDetails/")
public class OrderDetailsController {
    @Autowired
    private OrderService orderService;
    @PostMapping("getDetails")
    public Result<?> getDetails(Integer orderId){
        if(orderId==null){
            return Result.build(1,"订单号不能为空");
        }
        OderWithGoodsDO res=orderService.getOrderDetails(orderId);
        if(res==null){
            return Result.build(1,"查无此订单");
        }
        return Result.ok(res);
    }
}
