package com.scj.api.controller;

import com.scj.api.pojo.dto.UserLoginDTO;
import com.scj.api.pojo.vo.CompanyParamVO;
import com.scj.api.pojo.vo.TokenVO;
import com.scj.api.service.CompanyService;
import com.scj.common.utils.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by tom on 2018/8/22.
 */

/**
 * @author tom
 */
@RestController
@RequestMapping("/api/company/")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    /**
     * 页面参数
     * provinceId; 省份id
     * cityId;城市id
     * areaId;地区id
     * keyWord; 关键词（公司名字）
     * 参数为可选
     * 不传返回全部，没有数据code为1
     *
     * @param companyParamVO
     * @return
     */
    @PostMapping("getCompanies")
    public Result<?> getCompanies(CompanyParamVO companyParamVO) {
        List<Map> companies = companyService.getCompanyNameId(companyParamVO);
        if (companies == null || companies.size() == 0) {
            return Result.fail();
        } else {
            return Result.ok(companies);
        }
    }
}
