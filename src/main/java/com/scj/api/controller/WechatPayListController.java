package com.scj.api.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.WechatPayListDO;
import com.scj.api.service.WechatPayListService;

import com.scj.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:18
 * @description 微信支付交易记录相关接口
 */
@Controller
@RequestMapping("/api/wechatPay")
public class WechatPayListController {
	@Autowired
	private WechatPayListService wechatPayListService;

	/**
	 *
	 * @author 奋斗的牛牛
	 * @date 2018/8/24 10:19
	 * @param page, rows, employeeId
	 * @description 根据用户ID分页查询支付记录信息
	 * @return com.scj.common.utils.Result<com.baomidou.mybatisplus.plugins.Page<com.scj.api.pojo.domain.WeixinPayListDO>>
	 */
	@GetMapping("/getTransRecordList")
	@ResponseBody
	public Result<Page<WechatPayListDO>> wechatPayList(int page,int rows,int employeeId){
	    return Result.ok(wechatPayListService.listWeixinPayByEmployeeId(page,rows,employeeId));
	}
	

}
