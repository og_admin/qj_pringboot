package com.scj.api.controller;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.scj.api.pojo.domain.CompSetmealRelevanceDO;
import com.scj.api.pojo.vo.CompSetmealRelevanceVO;
import com.scj.api.service.CompSetmealRelevanceService;
import com.scj.common.base.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.common.utils.Result;
/**
  * CompSetmealRelevanceController 前端控制器
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:37
  */
@Controller
@RequestMapping("/scj/compSetmealRelevance")
public class CompSetmealRelevanceController extends AbstractController {
	@Autowired
	private CompSetmealRelevanceService compSetmealRelevanceService;
	
	@GetMapping()
	@RequiresPermissions("scj:compSetmealRelevance:compSetmealRelevance")
	String CompSetmealRelevance(){
	    return "scj/compSetmealRelevance/compSetmealRelevance";
	}
	
	@ResponseBody
	@GetMapping("/list")
	//@RequiresPermissions("scj:compSetmealRelevance:compSetmealRelevance")
	public Result<Page<CompSetmealRelevanceDO>> list(CompSetmealRelevanceDO compSetmealRelevanceDTO){
        Wrapper<CompSetmealRelevanceDO> wrapper = new EntityWrapper<CompSetmealRelevanceDO>(compSetmealRelevanceDTO);
        Page<CompSetmealRelevanceDO> page = compSetmealRelevanceService.selectPage(getPage(CompSetmealRelevanceDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("scj:compSetmealRelevance:add")
	String add(){
	    return "scj/compSetmealRelevance/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("scj:compSetmealRelevance:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		CompSetmealRelevanceDO compSetmealRelevance = compSetmealRelevanceService.selectById(id);
		model.addAttribute("compSetmealRelevance", compSetmealRelevance);
	    return "scj/compSetmealRelevance/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("scj:compSetmealRelevance:add")
	public Result<String> save( CompSetmealRelevanceDO compSetmealRelevance){
		compSetmealRelevanceService.insert(compSetmealRelevance);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("scj:compSetmealRelevance:edit")
	public Result<String>  update( CompSetmealRelevanceDO compSetmealRelevance){
		compSetmealRelevanceService.updateById(compSetmealRelevance);
		return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("scj:compSetmealRelevance:remove")
	public Result<String>  remove( Integer id){
		compSetmealRelevanceService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("scj:compSetmealRelevance:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		compSetmealRelevanceService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}

	/* 查询首页数据(带分页) */
	@RequestMapping(value = "/getIndexDataList")
	@ResponseBody
	public Object getIndexDataList(CompSetmealRelevanceVO compSetmealRelevanceVO, @RequestParam Map<String, Object> param) {
		return Result.ok(compSetmealRelevanceService.selectCompSetmealRelevanceListPage(compSetmealRelevanceVO,param));
	}

	/** 获取对应餐菜谱记录 */
	@RequestMapping(value = "/getRecipeRecord")
	@ResponseBody
	public Object getRecipeRecord(CompSetmealRelevanceVO compSetmealRelevanceVO, @RequestParam Map<String, Object> param) {
		return Result.ok(compSetmealRelevanceService.getRecipeRecord(compSetmealRelevanceVO,param));
	}
}
