package com.scj.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.scj.api.pojo.vo.SerachDepartmentVo;
import com.scj.api.service.DepartmentService;
import com.scj.common.utils.Result;

@Controller
@RequestMapping("/api/department")
public class DepartMentController {
	
	private final static Logger logger =  LoggerFactory.getLogger(DepartMentController.class);
	
	@Autowired
	private DepartmentService departmentService;
	
	/**
	 * api参数：公司id

		返回，部门名称，部门id
	 * @param res
	 * @param companyId
	 * @return
	 */
	@RequestMapping("/find")
	@ResponseBody
	public Result<?> serachDepartment(HttpServletResponse res,Integer companyId) {
		res.setHeader( "Access-Control-Allow-Origin","*");
		logger.info("companyId="+companyId);
		
		Result<List<SerachDepartmentVo>> response = departmentService.serachDepartment(companyId);
		
		logger.info("responseMsg="+response.getMsg());
		return response;
		
	}
}
