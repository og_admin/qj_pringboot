package com.scj.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scj.api.pojo.vo.AppUserInfoVo;
import com.scj.api.service.UserInfoService;
import com.scj.common.utils.Result;

/**
 * creat by langkai on 2018/8/23.
 *
 * @author Administrator
 */

@RestController
@RequestMapping("/api/User/")
public class ApiGetUserInfoController {

    /**
     * 接口参数：
     * openid : 用户ID
     */

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("getCompanies")
    public Result<?> getCompanies(AppUserInfoVo appUserInfoVo) {
        String openid = appUserInfoVo.getOpenid();
        if (openid != "" && openid != null) {
            List<AppUserInfoVo> userInfo = userInfoService.getUserInfoByOpenid(openid);
            if (userInfo == null || userInfo.size() == 0) {
                return Result.fail();
            } else {
                return Result.ok(userInfo);
            }
        } else {
            return Result.fail();
        }
    }

}
