package com.scj.api.controller;


import com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO;
import com.scj.api.service.CompSetmealVarietyDishesRelevanceService;
import com.scj.common.utils.Result;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:17
 * @description 套餐和菜品相关接口
 */
@Controller
@RequestMapping("/api/compDishesRelevance")
public class CompSetmealVarietyDishesRelevanceController{

  @Autowired
  private CompSetmealVarietyDishesRelevanceService compSetmealVarietyDishesRelevanceService;

  /**
   *
   * @author 奋斗的牛牛
   * @date 2018/8/24 10:18
   * @param comTcId
   * @description 根据套餐id获取菜品信息
   * @return com.scj.common.utils.Result<java.util.List<com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO>>
   */
  @GetMapping("/listByComTcId/{comTcId}")
  @ResponseBody
  public Result<List<CompSetmealVarietyDishesRelevanceDO>>
  listCompSetmealVarietyDishesRelevanceByComTcId(
      @PathVariable("comTcId") Integer comTcId) {
    List<CompSetmealVarietyDishesRelevanceDO> list = compSetmealVarietyDishesRelevanceService
        .listCompSetmealVarietyDishesRelevanceByComTcId(comTcId);
    return Result.ok(list);
  }


}
