package com.scj.api.controller;


import java.util.Arrays;

import com.scj.common.base.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.OrderDO;
import com.scj.api.service.OrderService;
import com.scj.common.utils.Result;

/**
 * 
 * <pre>
 * 订单表
 * </pre>
 * <small> 2018-08-22 22:48:35 | gacl</small>
 */
@Controller
@RequestMapping("/scj/order")
public class OrderController extends AbstractController {
	@Autowired
	private OrderService orderService;
	
	@GetMapping()
	@RequiresPermissions("scj:order:order")
	String Order(){
	    return "scj/order/order";
	}

	@ResponseBody
	@GetMapping("/getOrderList")
	@RequiresPermissions("scj:order:order")
	public Result<Page<OrderDO>> list(OrderDO orderDTO){
       	Wrapper<OrderDO> wrapper = new EntityWrapper<OrderDO>(orderDTO);
		wrapper.eq("employee_id", orderDTO.getEmployeeId());
		//wrapper.last("LIMIT "+pg+",10");
        Page<OrderDO> page = orderService.selectPage(getPage(OrderDO.class),wrapper);
        System.out.println(page);
        return Result.ok(page);

	}
	
	@GetMapping("/add")
	@RequiresPermissions("scj:order:add")
	String add(){
	    return "scj/order/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("scj:order:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		OrderDO order = orderService.selectById(id);
		model.addAttribute("order", order);
	    return "scj/order/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("scj:order:add")
	public Result<String> save( OrderDO order){
		orderService.insert(order);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("scj:order:edit")
	public Result<String>  update( OrderDO order){
		orderService.updateById(order);
		return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("scj:order:remove")
	public Result<String>  remove( Integer id){
		orderService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("scj:order:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		orderService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
