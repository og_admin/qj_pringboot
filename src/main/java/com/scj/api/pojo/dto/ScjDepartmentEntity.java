package com.scj.api.pojo.dto;

import java.io.Serializable;

public class ScjDepartmentEntity implements Serializable {
    private Integer id;

    private String name;

    private Integer parentId;

    private String departmentHeads;

    private String telephone;

    private Integer companyId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getDepartmentHeads() {
        return departmentHeads;
    }

    public void setDepartmentHeads(String departmentHeads) {
        this.departmentHeads = departmentHeads == null ? null : departmentHeads.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}