package com.scj.api.pojo.vo;

import com.scj.api.pojo.domain.OrderDO;
import lombok.Data;

/**
 * @Author kun
 * @DATETIME 2018/8/26 下午10:18
 * TODO
 */
@Data
public class OrderVO extends OrderDO {
    private String companyName;
    private String employeeName;
    private String employeeCompanyName;
    private String sunDay;
    private String monDay;
    private String cost;
    private int payStatus;
}
