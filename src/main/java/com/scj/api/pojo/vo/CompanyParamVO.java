package com.scj.api.pojo.vo;

import org.omg.CORBA.PRIVATE_MEMBER;

/**
 * Created by tom on 2018/8/22.
 */
public class CompanyParamVO {
    private Long provinceId;
    private Long cityId;
    private Long areaId;
    private String keyWord;

    @Override
    public String toString() {
        return "CompanyParamVO{" +
                "provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", areaId=" + areaId +
                ", keyWord='" + keyWord + '\'' +
                '}';
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
