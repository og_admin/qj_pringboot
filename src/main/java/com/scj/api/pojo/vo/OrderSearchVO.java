package com.scj.api.pojo.vo;

import lombok.Data;

/**
 * @Author kun
 * @DATETIME 2018/8/26 下午10:35
 * TODO
 */
@Data
public class OrderSearchVO {
    private String keyword;
    private Integer status;
    private Integer comId;
}
