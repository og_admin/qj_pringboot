package com.scj.api.pojo.vo;

/**
 * 查找部门名称和部门id 
 * @author Annazh
 *
 */
public class SerachDepartmentVo {
	
	private Integer id;

    private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
    
    
}
