package com.scj.api.pojo.vo;


/**
 * Created by langkai on 2018/8/22.
 */
public class AppUserInfoVo {

    private  String openid; //微信openid
    private  String name; //员工名称
    private  String departmentName; //部门名称
    private  String workNumber; //工号
    private  String companyName; //公司名称
    private  String headImgUrl; //微信头像地址


    @Override
    public String toString() {
        return "AppUserInfoVo [openid=" + openid + ", name=" + name
                + ", departmentName=" + departmentName + ", workNumber="
                + workNumber + ", companyName=" + companyName + ", headImgUrl="
                + headImgUrl + "]";
    }


    public String getOpenid() {
        return openid;
    }
    public void setOpenid(String openid) {
        this.openid = openid;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDepartmentName() {
        return departmentName;
    }
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public String getWorkNumber() {
        return workNumber;
    }
    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }
    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getHeadImgUrl() {
        return headImgUrl;
    }
    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }


}

