package com.scj.api.pojo.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;


/**
  * 查询首页数据VO实体类
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:38
  */
 @TableName("scj_comp_setmeal_relevance")
public class CompSetmealRelevanceVO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /** 主键 */
    @TableId
    private Integer id;
    /** 名称 */
    @TableField("relevance_name")
    private String relevanceName;
    /** 公司id */
    @TableField("com_id")
    private Integer comId;
    /** 套餐id */
    @TableField("taocan_id")
    private Integer taocanId;
    /** 开启状态 */
    @TableField("open_state")
    private Integer openState;
    /** 添加时间 */
    @TableField("createtime")
    private Date createtime;
    /** 图片 */
    @TableField("imgurl")
    private String imgurl;
    /** 简介信息 */
    @TableField("desinfo")
    private String desinfo;
    /** 序号 */
    @TableField("listsore")
    private Integer listsore;
    /** 用餐类型（1早餐/2午餐/3晚餐/4夜宵） */
    @TableField("type")
    private Integer type;
    /** 添加入 */
    @TableField("createuser_id")
    private Integer createuserId;
    /** 记录状态 */
    @TableField("state")
    private Integer state;
    /** 公司名称 */
    @TableField("com_name")
    private String comName;
    /** 部门名称 */
    @TableField("dept_name")
    private String deptName;
    /** 员工名称 */
    @TableField("staff_name")
    private String staffName;
    /** 员工工号 */
    @TableField("work_number")
    private String workNumber;
    /** 菜名 */
    @TableField("dishes_name")
    private String dishesName;
    /** 菜图片路径 */
    @TableField("dishes_img_url")
    private String dishesImgUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelevanceName() {
        return relevanceName;
    }

    public void setRelevanceName(String relevanceName) {
        this.relevanceName = relevanceName;
    }

    public Integer getComId() {
        return comId;
    }

    public void setComId(Integer comId) {
        this.comId = comId;
    }

    public Integer getTaocanId() {
        return taocanId;
    }

    public void setTaocanId(Integer taocanId) {
        this.taocanId = taocanId;
    }

    public Integer getOpenState() {
        return openState;
    }

    public void setOpenState(Integer openState) {
        this.openState = openState;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getDesinfo() {
        return desinfo;
    }

    public void setDesinfo(String desinfo) {
        this.desinfo = desinfo;
    }

    public Integer getListsore() {
        return listsore;
    }

    public void setListsore(Integer listsore) {
        this.listsore = listsore;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCreateuserId() {
        return createuserId;
    }

    public void setCreateuserId(Integer createuserId) {
        this.createuserId = createuserId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getDishesName() {
        return dishesName;
    }

    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }

    public String getDishesImgUrl() {
        return dishesImgUrl;
    }

    public void setDishesImgUrl(String dishesImgUrl) {
        this.dishesImgUrl = dishesImgUrl;
    }
}
