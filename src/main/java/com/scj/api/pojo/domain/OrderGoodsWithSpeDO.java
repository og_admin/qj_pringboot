package com.scj.api.pojo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by tom on 2018/8/23.
 */
@Getter
@Setter
public class OrderGoodsWithSpeDO extends OrderGoodsDO{
    private CompSetmealVarietyDishesRelevanceDO compSetmealVarietyDishesRelevanceDO;
}
