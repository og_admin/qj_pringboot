package com.scj.api.pojo.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;



/**
 * 
 * <pre>
 * 订单表
 * </pre>
 * <small> 2018-08-22 22:48:35 | gacl</small>
 */
 @TableName("scj_order")
public class OrderDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /**  */
    @TableId
    private Integer id;
    /** 公司id */
    @TableField("com_id")
    private Integer comId;
    @TableField("order_no")
    private Integer orderNo;
    /** 订单总金额商家id */
    @TableField("business_id")
    private Integer businessId;
    /** 订单总金额 */
    @TableField("total_amount")
    private BigDecimal totalAmount;
    /** meal_supplement-公司补贴费用 */
    @TableField("meal_supplement")
    private BigDecimal mealSupplement;
    /** supplement_fee-补加费 */
    @TableField("supplement_fee")
    private BigDecimal supplementFee;
    /** settlement_id-结算id */
    @TableField("settlement_id")
    private Integer settlementId;
    /** 下单时间 */
    @TableField("order_time")
    private Date orderTime;
    /** 员工id */
    @TableField("employee_id")
    private Integer employeeId;
    /** 用餐状态 */
    @TableField("dining_status")
    private Integer diningStatus;
    /** 支付方式，默认就是微信 */
    @TableField("pay_type")
    private Integer payType;
    /** 周一日期 */
    @TableField("sunday_date")
    private Date mondayDate;
    /** 周日日期 */
    @TableField("sunday_date")
    private Date sundayDate;
    /**  */
    @TableField("employee_id")
    private Integer merchantSettlementId;
    /** 收获地址 */
    @TableField("harvest_address")
    private String harvestAddress;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：公司id
     */
    public void setComId(Integer comId) {
        this.comId = comId;
    }
    /**
     * 获取：公司id
     */
    public Integer getComId() {
        return comId;
    }
    /**
     * 设置：订单总金额商家id
     */
    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }
    /**
     * 获取：订单总金额商家id
     */
    public Integer getBusinessId() {
        return businessId;
    }
    /**
     * 设置：订单总金额
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    /**
     * 获取：订单总金额
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
    /**
     * 设置：meal_supplement-公司补贴费用
     */
    public void setMealSupplement(BigDecimal mealSupplement) {
        this.mealSupplement = mealSupplement;
    }
    /**
     * 获取：meal_supplement-公司补贴费用
     */
    public BigDecimal getMealSupplement() {
        return mealSupplement;
    }
    /**
     * 设置：supplement_fee-补加费
     */
    public void setSupplementFee(BigDecimal supplementFee) {
        this.supplementFee = supplementFee;
    }
    /**
     * 获取：supplement_fee-补加费
     */
    public BigDecimal getSupplementFee() {
        return supplementFee;
    }
    /**
     * 设置：settlement_id-结算id
     */
    public void setSettlementId(Integer settlementId) {
        this.settlementId = settlementId;
    }
    /**
     * 获取：settlement_id-结算id
     */
    public Integer getSettlementId() {
        return settlementId;
    }
    /**
     * 设置：下单时间
     */
    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }
    /**
     * 获取：下单时间
     */
    public Date getOrderTime() {
        return orderTime;
    }
    /**
     * 设置：员工id
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
    /**
     * 获取：员工id
     */
    public Integer getEmployeeId() {
        return employeeId;
    }
    /**
     * 设置：用餐状态
     */
    public void setDiningStatus(Integer diningStatus) {
        this.diningStatus = diningStatus;
    }
    /**
     * 获取：用餐状态
     */
    public Integer getDiningStatus() {
        return diningStatus;
    }
    /**
     * 设置：支付方式，默认就是微信
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }
    /**
     * 获取：支付方式，默认就是微信
     */
    public Integer getPayType() {
        return payType;
    }
    /**
     * 设置：周一日期
     */
    public void setMondayDate(Date mondayDate) {
        this.mondayDate = mondayDate;
    }
    /**
     * 获取：周一日期
     */
    public Date getMondayDate() {
        return mondayDate;
    }
    /**
     * 设置：周日日期
     */
    public void setSundayDate(Date sundayDate) {
        this.sundayDate = sundayDate;
    }
    /**
     * 获取：周日日期
     */
    public Date getSundayDate() {
        return sundayDate;
    }
    /**
     * 设置：
     */
    public void setMerchantSettlementId(Integer merchantSettlementId) {
        this.merchantSettlementId = merchantSettlementId;
    }
    /**
     * 获取：
     */
    public Integer getMerchantSettlementId() {
        return merchantSettlementId;
    }
    /**
     * 设置：收获地址
     */
    public void setHarvestAddress(String harvestAddress) {
        this.harvestAddress = harvestAddress;
    }
    /**
     * 获取：收获地址
     */
    public String getHarvestAddress() {
        return harvestAddress;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }
}
