package com.scj.api.pojo.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;



/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-23 15:51:44 | gacl</small>
 */
 @TableName("scj_wechat_pay_list")
public class WechatPayListDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /**  */
    @TableId
    private Integer id;
    /**  */
    @TableField("order_number")
    private Integer orderNumber;
    /**  */
    @TableField("flow_number")
    private Integer flowNumber;
    /**  */
    @TableField("money")
    private BigDecimal money;
    /**  */
    @TableField("pay_date")
    private Date payDate;
    /**  */
    @TableField("pay_state")
    private Integer payState;
    /**  */
    @TableField("state")
    private Integer state;
    /**  */
    @TableField("pay_opt_type")
    private Integer payOptType;
    /**  */
    @TableField("completion_time")
    private Date completionTime;
    /**  */
    @TableField("merchant_id")
    private Integer merchantId;
    /** 用户id */
    @TableField("employee_id")
    private Integer employeeId;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：
     */
    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }
    /**
     * 获取：
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }
    /**
     * 设置：
     */
    public void setFlowNumber(Integer flowNumber) {
        this.flowNumber = flowNumber;
    }
    /**
     * 获取：
     */
    public Integer getFlowNumber() {
        return flowNumber;
    }
    /**
     * 设置：
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    /**
     * 获取：
     */
    public BigDecimal getMoney() {
        return money;
    }
    /**
     * 设置：
     */
    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }
    /**
     * 获取：
     */
    public Date getPayDate() {
        return payDate;
    }
    /**
     * 设置：
     */
    public void setPayState(Integer payState) {
        this.payState = payState;
    }
    /**
     * 获取：
     */
    public Integer getPayState() {
        return payState;
    }
    /**
     * 设置：
     */
    public void setState(Integer state) {
        this.state = state;
    }
    /**
     * 获取：
     */
    public Integer getState() {
        return state;
    }
    /**
     * 设置：
     */
    public void setPayOptType(Integer payOptType) {
        this.payOptType = payOptType;
    }
    /**
     * 获取：
     */
    public Integer getPayOptType() {
        return payOptType;
    }
    /**
     * 设置：
     */
    public void setCompletionTime(Date completionTime) {
        this.completionTime = completionTime;
    }
    /**
     * 获取：
     */
    public Date getCompletionTime() {
        return completionTime;
    }
    /**
     * 设置：
     */
    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }
    /**
     * 获取：
     */
    public Integer getMerchantId() {
        return merchantId;
    }
    /**
     * 设置：用户id
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
    /**
     * 获取：用户id
     */
    public Integer getEmployeeId() {
        return employeeId;
    }
}
