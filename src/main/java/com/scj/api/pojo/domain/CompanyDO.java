package com.scj.api.pojo.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;

/**
 * Created by tom on 2018/8/22.
 */
@TableName("scj_company")
public class CompanyDO {
    private Long id;
    private Long provinceId;
    private Long cityId;
    private Long areaId;
    private String name;
    private Integer state;
    private String phone;
    @DateTimeFormat(pattern="yyyy-mm-dd")
    private String startDate;
    @DateTimeFormat(pattern="yyyy-mm-dd")
    private String endDate;
    private String contacts;
    private String coordinate;
    private String logo;
    private String comInfo;
    private String businessLicense;
    private Integer salesmanId;
    private BigDecimal mealSupplement;
    private String address;
    @TableField(exist = false)
    private String salesmanDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getComInfo() {
        return comInfo;
    }

    public void setComInfo(String comInfo) {
        this.comInfo = comInfo;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public BigDecimal getMealSupplement() {
        return mealSupplement;
    }

    public void setMealSupplement(BigDecimal mealSupplement) {
        this.mealSupplement = mealSupplement;
    }

    public String getSalesmanDetails() {
        return salesmanDetails;
    }

    public void setSalesmanDetails(String salesmanDetails) {
        this.salesmanDetails = salesmanDetails;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
