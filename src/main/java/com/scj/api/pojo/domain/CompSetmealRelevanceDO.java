package com.scj.api.pojo.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


/**
  * CompSetmealRelevanceDO 实体类
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:38
  */
 @TableName("scj_comp_setmeal_relevance")
public class CompSetmealRelevanceDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /** 名称 */
    @TableId
    private Integer id;
    /**  */
    @TableField("name")
    private String name;
    /** 公司id */
    @TableField("com_id")
    private Integer comId;
    /** 套餐id */
    @TableField("taocan_id")
    private Integer taocanId;
    /** 开启状态 */
    @TableField("open_state")
    private Integer openState;
    /** 添加时间 */
    @TableField("createtime")
    private Date createtime;
    /** 图片 */
    @TableField("imgurl")
    private String imgurl;
    /** 简介信息 */
    @TableField("desinfo")
    private String desinfo;
    /** 序号 */
    @TableField("listsore")
    private Integer listsore;
    /** 用餐类型（早餐/午餐/晚餐/夜宵） */
    @TableField("type")
    private Integer type;
    /** 添加入 */
    @TableField("createuser_id")
    private Integer createuserId;
    /** 记录状态 */
    @TableField("state")
    private Integer state;

    /**
     * 设置：名称
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：名称
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 获取：
     */
    public String getName() {
        return name;
    }
    /**
     * 设置：公司id
     */
    public void setComId(Integer comId) {
        this.comId = comId;
    }
    /**
     * 获取：公司id
     */
    public Integer getComId() {
        return comId;
    }
    /**
     * 设置：套餐id
     */
    public void setTaocanId(Integer taocanId) {
        this.taocanId = taocanId;
    }
    /**
     * 获取：套餐id
     */
    public Integer getTaocanId() {
        return taocanId;
    }
    /**
     * 设置：开启状态
     */
    public void setOpenState(Integer openState) {
        this.openState = openState;
    }
    /**
     * 获取：开启状态
     */
    public Integer getOpenState() {
        return openState;
    }
    /**
     * 设置：添加时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    /**
     * 获取：添加时间
     */
    public Date getCreatetime() {
        return createtime;
    }
    /**
     * 设置：图片
     */
    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
    /**
     * 获取：图片
     */
    public String getImgurl() {
        return imgurl;
    }
    /**
     * 设置：简介信息
     */
    public void setDesinfo(String desinfo) {
        this.desinfo = desinfo;
    }
    /**
     * 获取：简介信息
     */
    public String getDesinfo() {
        return desinfo;
    }
    /**
     * 设置：序号
     */
    public void setListsore(Integer listsore) {
        this.listsore = listsore;
    }
    /**
     * 获取：序号
     */
    public Integer getListsore() {
        return listsore;
    }
    /**
     * 设置：用餐类型（早餐/午餐/晚餐/夜宵）
     */
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 获取：用餐类型（早餐/午餐/晚餐/夜宵）
     */
    public Integer getType() {
        return type;
    }
    /**
     * 设置：添加入
     */
    public void setCreateuserId(Integer createuserId) {
        this.createuserId = createuserId;
    }
    /**
     * 获取：添加入
     */
    public Integer getCreateuserId() {
        return createuserId;
    }
    /**
     * 设置：记录状态
     */
    public void setState(Integer state) {
        this.state = state;
    }
    /**
     * 获取：记录状态
     */
    public Integer getState() {
        return state;
    }
}
