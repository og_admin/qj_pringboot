package com.scj.api.pojo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by tom on 2018/8/23.
 */
@Getter
@Setter
public class OderWithGoodsDO extends OrderDO{

    private List<OrderGoodsWithSpeDO> orderGoods;
}
