package com.scj.api.pojo.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 
 * <pre>
 * 公司套餐与菜品关系
 * </pre>
 * <small> 2018-08-23 13:09:46 | gacl</small>
 */
@TableName("scj_comp_setmeal_variety_dishes_relevance")
public class CompSetmealVarietyDishesRelevanceDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /**  */
    @TableId
    private Integer id;
    /** 公司套餐id */
    @TableField("com_tc_id")
    private Integer comTcId;
    /** 菜品id */
    @TableField("dishes_id")
    private Integer dishesId;
    /** -商家id */
    @TableField("business_id")
    private Integer businessId;
    /** 菜品名称 */
    @TableField("dishes_name")
    private String dishesName;
    /** 菜品图片 */
    @TableField("dishes_img_url")
    private String dishesImgUrl;
    /** 公司菜品补贴费用 */
    @TableField("com_subsidy_cost")
    private BigDecimal comSubsidyCost;
    /** 状态 */
    @TableField("state")
    private Integer state;
    /** 添加时间 */
    @TableField("cratetime")
    private Date cratetime;
    /** 添加人 */
    @TableField("person_id")
    private Integer personId;
    /** 价格 */
    @TableField("price")
    private BigDecimal price;
    /** 原价格 */
    @TableField("original_price")
    private BigDecimal originalPrice;
    /** 销量 */
    @TableField("sales_volume")
    private Integer salesVolume;
    /** 菜品描述 */
    @TableField("description_dishes")
    private String descriptionDishes;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：公司套餐id
     */
    public void setComTcId(Integer comTcId) {
        this.comTcId = comTcId;
    }
    /**
     * 获取：公司套餐id
     */
    public Integer getComTcId() {
        return comTcId;
    }
    /**
     * 设置：菜品id
     */
    public void setDishesId(Integer dishesId) {
        this.dishesId = dishesId;
    }
    /**
     * 获取：菜品id
     */
    public Integer getDishesId() {
        return dishesId;
    }
    /**
     * 设置：-商家id
     */
    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }
    /**
     * 获取：-商家id
     */
    public Integer getBusinessId() {
        return businessId;
    }
    /**
     * 设置：菜品名称
     */
    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }
    /**
     * 获取：菜品名称
     */
    public String getDishesName() {
        return dishesName;
    }
    /**
     * 设置：菜品图片
     */
    public void setDishesImgUrl(String dishesImgUrl) {
        this.dishesImgUrl = dishesImgUrl;
    }
    /**
     * 获取：菜品图片
     */
    public String getDishesImgUrl() {
        return dishesImgUrl;
    }
    /**
     * 设置：公司菜品补贴费用
     */
    public void setComSubsidyCost(BigDecimal comSubsidyCost) {
        this.comSubsidyCost = comSubsidyCost;
    }
    /**
     * 获取：公司菜品补贴费用
     */
    public BigDecimal getComSubsidyCost() {
        return comSubsidyCost;
    }
    /**
     * 设置：状态
     */
    public void setState(Integer state) {
        this.state = state;
    }
    /**
     * 获取：状态
     */
    public Integer getState() {
        return state;
    }
    /**
     * 设置：添加时间
     */
    public void setCratetime(Date cratetime) {
        this.cratetime = cratetime;
    }
    /**
     * 获取：添加时间
     */
    public Date getCratetime() {
        return cratetime;
    }
    /**
     * 设置：添加人
     */
    public void setPersonId(Integer personId) {
        this.personId = personId;
    }
    /**
     * 获取：添加人
     */
    public Integer getPersonId() {
        return personId;
    }
    /**
     * 设置：价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    /**
     * 获取：价格
     */
    public BigDecimal getPrice() {
        return price;
    }
    /**
     * 设置：原价格
     */
    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }
    /**
     * 获取：原价格
     */
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }
    /**
     * 设置：销量
     */
    public void setSalesVolume(Integer salesVolume) {
        this.salesVolume = salesVolume;
    }
    /**
     * 获取：销量
     */
    public Integer getSalesVolume() {
        return salesVolume;
    }
    /**
     * 设置：菜品描述
     */
    public void setDescriptionDishes(String descriptionDishes) {
        this.descriptionDishes = descriptionDishes;
    }
    /**
     * 获取：菜品描述
     */
    public String getDescriptionDishes() {
        return descriptionDishes;
    }
}
