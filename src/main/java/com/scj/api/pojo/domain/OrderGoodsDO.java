package com.scj.api.pojo.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;


/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-23 15:37:24 | gacl</small>
 */
@Getter
@Setter
 @TableName("scj_order_goods")
public class OrderGoodsDO implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
    /**  */
    @TableId
    private Integer id;
    /** 公司id */
    private Integer companyId;
    /** 商家id */
    private Integer businessId;
    /** 员工id */
    private Integer employeeId;
    /** 订单id */
    private Integer orderId;
    /** 对应的商品id */
    private Integer goodsId;
    /** 数量 */
    private Integer number;
    /** 备注说明 */
    private String remarks;
    /** 规格id */
    private Integer specificationId;

    /**
     * 餐次类型(早餐/晚餐/午餐)
     */
    private Integer type;
    /**
     * 用餐日期
     */
    private Date dinnerDate;


}
