package com.scj.api.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.scj.api.pojo.vo.AppUserInfoVo;

/**
 * @author 修复规范
 */
public interface UserInfoService extends IService<AppUserInfoVo> {

    /** 获取员工的信息 */
	List<AppUserInfoVo> getUserInfoByOpenid(String openid);
}
