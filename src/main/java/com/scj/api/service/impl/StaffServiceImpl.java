package com.scj.api.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.scj.api.dao.StaffDao;
import com.scj.api.pojo.domain.StaffDo;
import com.scj.api.service.StaffService;
import com.scj.api.service.UserService;
import com.scj.staff.pojo.dto.StaffDTO;
import com.scj.staff.pojo.vo.StaffVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 员工
 *
 * @author runnable@sina.cn
 */
@Service("staffService")
public class StaffServiceImpl extends ServiceImpl<StaffDao, StaffDo> implements StaffService {

    @Autowired
    private StaffDao staffDao;

    public static Pattern phonePattern = Pattern.compile("\\d{1,11}");

    @Override
    public StaffDo queryInfo(Map<String, Object> params) {
        return baseMapper.queryStaff(params);
    }

    @Override
    public Page<StaffVo> queryStaffList(StaffDTO staffDto) {
        String phoneOrName = staffDto.getPhoneOrName();
        if(!StringUtils.isEmpty(phoneOrName)){
            if(phonePattern.matcher(phoneOrName).matches()){
                staffDto.setPhone(phoneOrName);
            }else{
                staffDto.setName(phoneOrName);
            }
        }
        List<StaffVo> datas = staffDao.queryStaffList(staffDto);
        int total = staffDao.queryStaffListCount(staffDto);
        Page<StaffVo>  page = new Page<>();
        page.setRecords(datas);
        page.setTotal(total);
        page.setCurrent(staffDto.getPageNo());
        page.setSize(staffDto.getPageSize());
        return page;
    }
}
