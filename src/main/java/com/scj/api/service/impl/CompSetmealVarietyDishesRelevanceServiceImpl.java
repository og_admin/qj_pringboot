package com.scj.api.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.scj.api.dao.CompSetmealVarietyDishesRelevanceDao;
import com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO;
import com.scj.api.service.CompSetmealVarietyDishesRelevanceService;
import com.scj.common.base.CoreServiceImpl;
import java.util.List;
import org.springframework.stereotype.Service;


/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:21
 * @description 公司套餐与餐品关系服务类
 */
@Service
public class CompSetmealVarietyDishesRelevanceServiceImpl extends
    CoreServiceImpl<CompSetmealVarietyDishesRelevanceDao, CompSetmealVarietyDishesRelevanceDO> implements
    CompSetmealVarietyDishesRelevanceService {

  /**
   *
   * @author 奋斗的牛牛
   * @date 2018/8/24 10:22
   * @param comTcId
   * @description 根据套餐ID查询相关菜品
   * @return java.util.List<com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO>
   */
  @Override
  public List<CompSetmealVarietyDishesRelevanceDO> listCompSetmealVarietyDishesRelevanceByComTcId(
      int comTcId) {
    return selectList(new EntityWrapper<CompSetmealVarietyDishesRelevanceDO>().andNew("com_tc_id={0}",comTcId));
  }
}
