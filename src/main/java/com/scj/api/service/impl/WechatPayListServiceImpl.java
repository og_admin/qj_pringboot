package com.scj.api.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.dao.WechatPayListDao;
import com.scj.api.pojo.domain.WechatPayListDO;
import com.scj.api.service.WechatPayListService;
import com.scj.common.base.CoreServiceImpl;
import org.springframework.stereotype.Service;



/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:23
 * @description 微信交易记录服务类
 */
@Service
public class WechatPayListServiceImpl extends
    CoreServiceImpl<WechatPayListDao, WechatPayListDO> implements
    WechatPayListService {

  /**
   *
   * @author 奋斗的牛牛
   * @date 2018/8/24 10:23
   * @param page, row, employeeId
   * @description 根据用户ID查询相关交易记录
   * @return com.baomidou.mybatisplus.plugins.Page<com.scj.api.pojo.domain.WeixinPayListDO>
   */
  @Override
  public Page<WechatPayListDO> listWeixinPayByEmployeeId(int page, int row, int employeeId) {
    Wrapper weixinWrapper = new EntityWrapper<WechatPayListDO>().andNew("employee_id={0}",employeeId);
    Page<WechatPayListDO> pageContions= new Page<>(page,row);
    Page<WechatPayListDO> pages = selectPage(pageContions,weixinWrapper);
    return pages;
  }
}
