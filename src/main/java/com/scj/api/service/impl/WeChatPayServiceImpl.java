package com.scj.api.service.impl;


import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConfigImpl;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.scj.api.config.WXConst;
import com.scj.api.service.WeChatPayService;
import com.scj.api.service.WechatPayListService;
import com.scj.api.util.UUIDUtil;
import com.scj.api.util.UrlUtil;
import com.scj.common.utils.R;
import net.sf.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Toy
 * @date 2018/8/26 下午4:47
 */
@Service("weChatPayService")
public class WeChatPayServiceImpl implements WeChatPayService {

    @Autowired
    private WechatPayListService wechatPayListService;

    @Override
    public R doPay(Map<String, Object> params, HttpServletRequest request) {


        /************************插入商品订单及商品详情*********************/

        /************************插入商品订单及商品详情*********************/

        String rechargeId = UUIDUtil.get32UUID();
        String openId="otMye4ljrpr38DoksEzn6mA2lat4";
        String totalFee = "1";

        WXPay wxpay = null;
        try {
            WXPayConfigImpl config = WXPayConfigImpl.getInstance();
            wxpay = new WXPay(config);
        } catch (Exception e) {
            return R.error("微信参数加载失败!");
        }
        String nonce_str = WXPayUtil.generateNonceStr();
        Map<String, String> map = new HashMap<String, String>();
        map.put("body", WXConst.MSG_BODY);//商品描述
        map.put("mch_id", WXConst.MCH_ID);//商户平台id
        map.put("appid", WXConst.APP_ID);//公众号id
        map.put("nonce_str", nonce_str);//随机字符串
        map.put("notify_url", WXConst.NOTIFY_URL);//异步回调api
//        String requestIp=CusAccessObjectUtil.getIpAddress(request);
//        map.put("spbill_create_ip",requestIp);//支付ip
        map.put("out_trade_no",rechargeId);//商品订单号
        map.put("total_fee",totalFee);//真实金额
        map.put("trade_type", "JSAPI");//JSAPI、h5调用
        map.put("openid", openId);//支付用户openid
        try {
            Map<String, String> backMap = wxpay.unifiedOrder(map);
            System.out.println(backMap);
            Map<String, String> resultMap = new HashMap<>();
            if(backMap.get("result_code").equals("SUCCESS")) {
                String time = Long.toString(System.currentTimeMillis());
                resultMap.put("appId", WXConst.APP_ID);
                resultMap.put("timeStamp", time);
                resultMap.put("nonceStr", nonce_str);
                resultMap.put("package", "prepay_id=" + backMap.get("prepay_id"));
                resultMap.put("signType", "MD5");
                String sign = WXPayUtil.generateSignature(resultMap, WXConst.SIGN_KEY);
                resultMap.put("paySign", sign);
                resultMap.put("rechargeId", rechargeId);

                /************************插入预支付订单*********************/


                /************************插入预支付订单*********************/

                return R.ok().put("resultMap",resultMap);

            }else if(backMap.get("result_code").equals("FAIL")){
                return R.error("下单失败,请检查参数!");
            }
            else
            {
                return R.error("程序异常!");
            }
        } catch (Exception e) {
            return R.error("微信请求预支付失败!");
        }
    }


    @Override
    public R getOpenId(String code) {
        String params = "appid=" + WXConst.APP_ID + "&secret=" + WXConst.APP_SECRET + "&js_code=" + code + "&grant_type=" + WXConst.GRANT_TYPE;
        String jsonResult =UrlUtil.sendGet(WXConst.CODETOSESSION_URL+"?"+params);
        JSONObject jsonObject = JSONObject.fromObject(jsonResult);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if(jsonObject.containsKey("errcode")){
            return R.error("获取失败!");
        }
        try {
            resultMap.put("session_key", jsonObject.get("session_key"));
            resultMap.put("unionId", jsonObject.get("unionid"));
            resultMap.put("openId", jsonObject.get("openid"));
            return R.ok(resultMap);
        } catch (Exception e) {
            return R.error("程序异常!");
        }
    }

    @Override
    public void callBack(HttpServletRequest request, HttpServletResponse response) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        String FEATURE = null;
        Map<String,String> backMap = new HashMap<String,String>();

        Document doc= null;
        try {
            FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
            dbf.setFeature(FEATURE, true);
            FEATURE = "http://xml.org/sax/features/external-general-entities";
            dbf.setFeature(FEATURE, false);
            FEATURE = "http://xml.org/sax/features/external-parameter-entities";
            dbf.setFeature(FEATURE, false);
            FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
            dbf.setFeature(FEATURE, false);
            dbf.setXIncludeAware(false);
            dbf.setExpandEntityReferences(false);
            DocumentBuilder safebuilder = dbf.newDocumentBuilder();
            InputStream is= request.getInputStream();
            doc = ocumentParse(safebuilder.parse(is));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Element rootElt = doc.getRootElement();
            List<Element> list = rootElt.elements();
            for (Element element : list) {  //遍历节点
                backMap.put(element.getName(), element.getText());
            }

        String returnCode = "";
        String returnMsg = "";
        System.out.println("call back by weixin ~!");
        try {
            if(backMap.containsKey("return_code")&&backMap.get("return_code").equals("SUCCESS")) {
                String returnSign = backMap.get("sign");
                if(returnSign==""||returnSign==null){
                    returnCode = "FAIL";
                    returnMsg = "SIGN IS NULL";
                }else {
                    backMap.remove("sign");
                    String checkSign = WXPayUtil.generateSignature(backMap, WXConst.SIGN_KEY, WXPayConstants.SignType.MD5);
                    if (checkSign.equals(returnSign)) {
                        returnCode = "SUCCESS";
                        returnMsg = "SIGN SUCCESS";
                        String rechargeId = backMap.get("out_trade_no");
                        double paidValue = Integer.parseInt(backMap.get("cash_fee"))/100.00;

                        /************************回调成功业务处理*********************/





                        /************************回调成功业务处理*********************/

                    } else {
                        returnCode = "FAIL";
                        returnMsg = "SIGN FAILED";
                    }
                }
            }else{
                returnCode = backMap.get("return_code");
                returnMsg = backMap.get("return_msg");
            }
            StringBuffer paraBuffer = new StringBuffer();
            paraBuffer.append("<xml>\n");
            paraBuffer.append("<return_code><![CDATA["+returnCode+"]]></return_code>\n");
            paraBuffer.append("<return_msg><![CDATA[" +returnMsg+"]]></return_msg>\n");
            paraBuffer.append("</xml>");
            PrintWriter out = new PrintWriter(response.getOutputStream());
            out.print(paraBuffer.toString());
            out.flush();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Order Error Log"+e.getMessage());
        }finally {
            try {
                if (response.getOutputStream() != null) {
                    response.getOutputStream().close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                System.out.println("Order Close Error Log"+e2.getMessage());
            }
        }
    }


    public   static   Document   ocumentParse(org.w3c.dom.Document   doc)   throws   Exception   {
        if   (doc   ==   null)   {
            return   (null);
        }
        org.dom4j.io.DOMReader   xmlReader   =   new   org.dom4j.io.DOMReader();
        return   (xmlReader.read(doc));
    }


}
