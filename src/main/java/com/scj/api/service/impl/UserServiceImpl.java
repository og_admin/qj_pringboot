package com.scj.api.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.scj.api.config.JWTConfig;
import com.scj.api.dao.AppUserDao;
import com.scj.api.exception.ScjApiException;
import com.scj.api.pojo.domain.AppUserDO;
import com.scj.api.pojo.vo.TokenVO;
import com.scj.api.service.UserService;
import com.scj.api.util.JWTUtil;
import com.scj.common.base.CoreServiceImpl;
import com.scj.common.config.CacheConfiguration;
import com.scj.common.config.ScjConfig;
import com.scj.common.type.EnumErrorCode;
import com.scj.common.utils.SpringContextHolder;

import java.util.Map;

/**
 *
 */
@Service
public class UserServiceImpl extends CoreServiceImpl<AppUserDao, AppUserDO> implements UserService {
	/** Holder for lazy-init */
	private static class Holder {
		static final JWTConfig jwt = SpringContextHolder.getBean(ScjConfig.class).getJwt();
		static final Cache logoutTokens = CacheConfiguration.dynaConfigCache("tokenExpires", 0, jwt.getRefreshTokenExpire(), 1000);
		static {
			JWTUtil.mykey = jwt.getUserPrimaryKey();
		}
	}

	@Override
    public TokenVO getToken(String uname, String passwd) {
        AppUserDO user = selectOne(new EntityWrapper<AppUserDO>().eq("uname", uname));
        if (null == user || !user.getPasswd().equals(passwd)) {
            throw new ScjApiException(EnumErrorCode.apiAuthorizationLoginFailed.getCodeStr());
        }
        return createToken(user);
    }

    @Override
	public boolean verifyToken(String token, boolean refresh) {
    	String userId = null; AppUserDO user = null;
    	return StringUtils.isNotBlank(token)
    			&& (userId=JWTUtil.getUserId(token))!=null
    			&& notLogout(token)
    			&& (user=refresh?selectOne(new EntityWrapper<AppUserDO>().eq("uname", userId)):selectById(userId))!=null
    			&& (refresh ? JWTUtil.verify(token, user.getUname(), user.getId()+user.getPasswd()) : JWTUtil.verify(token, userId, user.getUname()+user.getPasswd()));
	}

	@Override
	public TokenVO refreshToken(String uname, String refreshToken) {
		AppUserDO user = null;
		if(StringUtils.isNotBlank(refreshToken)
				&& uname.equals(JWTUtil.getUserId(refreshToken))
				&& notLogout(refreshToken)
				&& (user=selectOne(new EntityWrapper<AppUserDO>().eq("uname", uname)))!=null
				&& JWTUtil.verify(refreshToken, user.getUname(), user.getId()+user.getPasswd())
				&& Holder.logoutTokens.putIfAbsent(refreshToken, null)==null) {
			return createToken(user);
		}
    	throw new ScjApiException(EnumErrorCode.apiAuthorizationInvalid.getCodeStr());
	}

	@Override
	public Boolean logoutToken(String token, String refreshToken) {
		Boolean expire = Boolean.FALSE; String userId = null, uname = null; AppUserDO user = null;
		if(StringUtils.isNotBlank(token)
				&& (userId=JWTUtil.getUserId(token))!=null
				&& Holder.logoutTokens.get(token)==null
				&& (user=selectById(userId))!=null
				&& JWTUtil.verify(token, userId, user.getUname()+user.getPasswd())
				&& Holder.logoutTokens.putIfAbsent(token, null)==null) {
			expire = Boolean.TRUE;
		}
		if(StringUtils.isNotBlank(refreshToken)
				&& (uname=JWTUtil.getUserId(refreshToken))!=null
				&& Holder.logoutTokens.get(refreshToken)==null
				&& (user!=null || (user=selectOne(new EntityWrapper<AppUserDO>().eq("uname", uname)))!=null)
				&& JWTUtil.verify(refreshToken, user.getUname(), user.getId()+user.getPasswd())
				&& Holder.logoutTokens.putIfAbsent(refreshToken, null)==null) {
			expire = Boolean.TRUE;
		}
		return expire;
	}

	private TokenVO createToken(AppUserDO user) {
        TokenVO vo = new TokenVO();
        vo.setToken(JWTUtil.sign(user.getId() + "", user.getUname() + user.getPasswd(), Holder.jwt.getExpireTime()));
        vo.setRefleshToken(JWTUtil.sign(user.getUname(), user.getId() + user.getPasswd(), Holder.jwt.getExpireTime()));
        vo.setTokenExpire(Holder.jwt.getExpireTime());
        vo.setRefreshTokenExpire(Holder.jwt.getRefreshTokenExpire());
        return vo;
	}
	
	private boolean notLogout(String token) {
		if(Holder.logoutTokens.get(token)!=null)
			throw new ScjApiException(EnumErrorCode.apiAuthorizationLoggedout.getMsg());
		return true;
	}
}
