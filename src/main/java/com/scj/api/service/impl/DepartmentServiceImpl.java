package com.scj.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scj.api.dao.ScjDepartmentEntityDao;
import com.scj.api.pojo.dto.ScjDepartmentEntity;
import com.scj.api.pojo.vo.SerachDepartmentVo;
import com.scj.api.service.DepartmentService;
import com.scj.common.utils.Result;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	
	@Autowired
	private ScjDepartmentEntityDao scjDepartmentEntityDao;

	@Override
	public Result<List<SerachDepartmentVo>> serachDepartment(Integer companyId) {
		Result<List<SerachDepartmentVo>> result = null;
		List<SerachDepartmentVo> list = scjDepartmentEntityDao.findByCompanyId(companyId);
		if(list == null) {
			return result.fail();
		}else {
			return result.ok(list);
		}
		//return result;
	}

}
