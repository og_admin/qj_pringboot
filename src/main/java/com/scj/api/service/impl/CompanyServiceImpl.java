package com.scj.api.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.scj.api.dao.CompanyDao;
import com.scj.api.pojo.domain.CompanyDO;
import com.scj.api.pojo.vo.CompanyParamVO;
import com.scj.api.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by alien on 2018/8/22.
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyDao,CompanyDO> implements CompanyService {
    @Autowired
    protected CompanyDao appCompanyDao;

    @Override
    public List<Map> getCompanyNameId(CompanyParamVO companyParamVO) {
        return appCompanyDao.selectComNameId(companyParamVO);
    }
}
