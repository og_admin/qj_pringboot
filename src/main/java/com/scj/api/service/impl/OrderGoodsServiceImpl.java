package com.scj.api.service.impl;

import com.scj.api.dao.OrderGoodsDao;
import com.scj.api.pojo.domain.OrderGoodsDO;
import com.scj.api.service.OrderGoodsService;
import org.springframework.stereotype.Service;


import com.scj.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-23 15:37:24 | gacl</small>
 */
@Service
public class OrderGoodsServiceImpl extends CoreServiceImpl<OrderGoodsDao, OrderGoodsDO> implements OrderGoodsService {

}
