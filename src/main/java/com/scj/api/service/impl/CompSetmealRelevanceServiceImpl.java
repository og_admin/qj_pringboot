package com.scj.api.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.scj.api.dao.CompSetmealRelevanceDao;
import com.scj.api.pojo.domain.CompSetmealRelevanceDO;
import com.scj.api.pojo.vo.CompSetmealRelevanceVO;
import com.scj.api.service.CompSetmealRelevanceService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scj.common.base.CoreServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
  * CompSetmealRelevanceServiceImpl service实现类
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:39
  */
@Service("compSetmealRelevanceService")
public class CompSetmealRelevanceServiceImpl extends CoreServiceImpl<CompSetmealRelevanceDao, CompSetmealRelevanceDO> implements CompSetmealRelevanceService {

    @Override
    public Object selectCompSetmealRelevanceListPage(@Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO,@RequestParam Map<String, Object> param) {
        // 创建分页对象
        Page<CompSetmealRelevanceVO> pages = new Page<CompSetmealRelevanceVO>(1,10);
        // 判断记录数
        if (param.get("limit") != null && Integer.valueOf(param.get("limit").toString()) > 0) pages.setSize((Integer.valueOf(param.get("limit").toString())));
        else pages.setSize(10);
        // 判断页数
        if (param.get("page") != null && Integer.valueOf(param.get("page").toString()) > 0) pages.setCurrent((Integer.valueOf(param.get("page").toString())));
        else pages.setCurrent(1);
        // 获取数据
        List<CompSetmealRelevanceVO> list = baseMapper.selectCompSetmealRelevanceListPage(pages , compSetmealRelevanceVO);
        JSONArray jsonArray_breakfast = new JSONArray();// 早餐
        JSONArray jsonArray_lunch = new JSONArray();// 午餐
        JSONArray jsonArray_dinner = new JSONArray();// 晚餐
        JSONArray jsonArray_nightingale = new JSONArray(); // 夜宵
        // 循环拼接不同类型的数据
        for (CompSetmealRelevanceVO compSetmealRelevances:list) {
            if(compSetmealRelevances.getType()==1)
                jsonArray_breakfast.add(compSetmealRelevances);
            else if(compSetmealRelevances.getType()==2)
                jsonArray_lunch.add(compSetmealRelevances);
            else if(compSetmealRelevances.getType()==3)
                jsonArray_dinner.add(compSetmealRelevances);
            else if(compSetmealRelevances.getType()==4)
                jsonArray_nightingale.add(compSetmealRelevances);
        }
        JSONObject allData = new JSONObject();
        allData.put("1",jsonArray_breakfast);
        allData.put("2",jsonArray_lunch);
        allData.put("3",jsonArray_dinner);
        allData.put("4",jsonArray_nightingale);
        return allData;
    }

    @Override
    public Object getRecipeRecord(@Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO,@RequestParam Map<String, Object> param) {
        // 创建分页对象
        Page<CompSetmealRelevanceVO> pages = new Page<CompSetmealRelevanceVO>(1,10);
        // 判断记录数
        if (param.get("limit") != null && Integer.valueOf(param.get("limit").toString()) > 0) pages.setSize((Integer.valueOf(param.get("limit").toString())));
        else pages.setSize(10);
        // 判断页数
        if (param.get("page") != null && Integer.valueOf(param.get("page").toString()) > 0) pages.setCurrent((Integer.valueOf(param.get("page").toString())));
        else pages.setCurrent(1);

        return baseMapper.getRecipeRecord(pages , compSetmealRelevanceVO);
    }
}
