package com.scj.api.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.scj.api.dao.OrderDao;
import com.scj.api.pojo.domain.OderWithGoodsDO;

import com.scj.api.pojo.vo.OrderSearchVO;
import com.scj.api.pojo.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scj.api.pojo.domain.OrderDO;
import com.scj.api.service.OrderService;

import java.util.List;


/**
 * 
 * <pre>
 * 订单表
 * </pre>
 * <small> 2018-08-22 22:48:35 | gacl</small>
 */
/**
 * @author tom
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderDO> implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Override
    public OderWithGoodsDO getOrderDetails(Integer orderId) {
        return orderDao.selectOrderWithGoods(orderId);
    }

    @Override
    public Page<OrderVO> selectByPage(Page<OrderVO> page, OrderSearchVO searchVO) {
        return page.setRecords(orderDao.selectByPage(page, searchVO));
    }
}
