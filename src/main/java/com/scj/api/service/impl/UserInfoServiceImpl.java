package com.scj.api.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scj.api.dao.AppUserInfoDao;
import com.scj.api.pojo.vo.AppUserInfoVo;
import com.scj.api.service.UserInfoService;

/**
 * @date 2018年8月23日21:05:54
 * @author Runnable@sina.cn
 * @remark 修复规范定义
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<AppUserInfoDao, AppUserInfoVo> implements UserInfoService {

    @Override
    public List<AppUserInfoVo> getUserInfoByOpenid(String openid) {
        return baseMapper.selectUserInfo(openid);
    }
}
