package com.scj.api.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.OderWithGoodsDO;
import com.scj.api.pojo.domain.OrderDO;

import com.baomidou.mybatisplus.service.IService;
import com.scj.api.pojo.vo.OrderSearchVO;
import com.scj.api.pojo.vo.OrderVO;

import java.util.List;

/**
 * 
 * <pre>
 * 订单表
 * </pre>
 * <small> 2018-08-22 22:48:35 | gacl</small>
 */


public interface OrderService extends IService<OrderDO> {
    /**
     * @author tom
     */
    OderWithGoodsDO getOrderDetails(Integer orderId);

    Page<OrderVO> selectByPage(Page<OrderVO> page, OrderSearchVO searchVO);

}
