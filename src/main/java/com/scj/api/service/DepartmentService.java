package com.scj.api.service;

import java.util.List;
import com.scj.api.pojo.vo.SerachDepartmentVo;
import com.scj.common.utils.Result;


public interface DepartmentService {
	
	public Result<List<SerachDepartmentVo>> serachDepartment(Integer companyId);

}
