package com.scj.api.service;

import com.scj.common.utils.R;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author Toy
 * @date 2018/8/26 下午4:47
 */
public interface WeChatPayService {
    R doPay(Map<String, Object> params, HttpServletRequest request);

    void callBack(HttpServletRequest request, HttpServletResponse response);

    R getOpenId(String code);
}
