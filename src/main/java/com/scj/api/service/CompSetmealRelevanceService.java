package com.scj.api.service;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.baomidou.mybatisplus.service.IService;
import com.scj.api.pojo.domain.CompSetmealRelevanceDO;
import com.scj.api.pojo.vo.CompSetmealRelevanceVO;
import com.scj.common.base.CoreService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
  * CompSetmealRelevanceService service层
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:38
  */
public interface CompSetmealRelevanceService extends IService<CompSetmealRelevanceDO> {

    /**
     * 首页基础数据 分页查询
     * @param param
     * @return
     */
    Object selectCompSetmealRelevanceListPage(@Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO, @RequestParam Map<String, Object> param);

    /**
     * 获取对应餐菜谱记录
     */
    Object getRecipeRecord(@Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO, @RequestParam Map<String, Object> param);

}
