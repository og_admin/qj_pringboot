package com.scj.api.service;


import com.scj.api.pojo.domain.OrderGoodsDO;
import com.scj.common.base.CoreService;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-23 15:37:24 | gacl</small>
 */
public interface OrderGoodsService extends CoreService<OrderGoodsDO> {
    
}
