package com.scj.api.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.scj.api.pojo.domain.StaffDo;
import com.scj.staff.pojo.dto.StaffDTO;
import com.scj.staff.pojo.vo.StaffVo;

import java.util.Map;

/**
 * @author runnable@sina.cn
 */
public interface StaffService extends IService<StaffDo> {

    /**
     *  根据参数查询员工信息
     * @param params Mapper 参数
     * @return
     */
    StaffDo queryInfo(Map<String, Object> params);

    /**
     * 员工列表
     * @param staffDto
     * @return
     */
    Page<StaffVo> queryStaffList(StaffDTO staffDto);
}
