package com.scj.api.service;


import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.WechatPayListDO;
import com.scj.common.base.CoreService;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:21
 * @description 微信支付交易记录
 */
public interface WechatPayListService extends CoreService<WechatPayListDO> {
  Page<WechatPayListDO> listWeixinPayByEmployeeId(int page,int row,int employeeId);
}
