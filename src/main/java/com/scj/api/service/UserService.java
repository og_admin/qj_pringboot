package com.scj.api.service;

import com.scj.api.pojo.domain.AppUserDO;
import com.scj.api.pojo.vo.TokenVO;
import com.scj.common.base.CoreService;

import java.util.Map;

/**
 *
 */
public interface UserService extends CoreService<AppUserDO> {
    /** 申请token */
    TokenVO getToken(String uname, String passwd) ;
    /** 刷新token */
    TokenVO refreshToken(String uname, String refreshToken);
    /** 检查token是否有效：未超时、未注销*/
    boolean verifyToken(String token, boolean refresh);
    /** 注销token */
    Boolean logoutToken(String token, String refreshToken);

}
