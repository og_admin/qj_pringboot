package com.scj.api.service;

import com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO;
import com.scj.common.base.CoreService;
import java.util.List;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:21
 * @description  公司套餐与餐品关系
 */
public interface CompSetmealVarietyDishesRelevanceService extends
    CoreService<CompSetmealVarietyDishesRelevanceDO> {
  List<CompSetmealVarietyDishesRelevanceDO> listCompSetmealVarietyDishesRelevanceByComTcId(
      int comTcId);
}
