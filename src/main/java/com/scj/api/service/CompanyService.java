package com.scj.api.service;

import com.baomidou.mybatisplus.service.IService;
import com.scj.api.pojo.domain.CompanyDO;
import com.scj.api.pojo.vo.CompanyParamVO;

import java.util.List;
import java.util.Map;

/**
 * Created by tom on 2018/8/22.
 */

/**
 * @author tom
 */
public interface CompanyService  extends IService<CompanyDO> {
    List<Map> getCompanyNameId(CompanyParamVO companyParamVO);
}
