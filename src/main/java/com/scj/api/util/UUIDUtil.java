package com.scj.api.util;

import java.util.UUID;

/**
 * @author Toy
 * @date 2018/8/26 下午6:37
 */
public class UUIDUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String get32UUID() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
        return uuid;
    }

    public static void main(String[] args) {
        System.out.println(UUIDUtil.getUUID());
    }
}

