package com.scj.api.dao;

import java.util.List;

import com.scj.api.pojo.dto.ScjDepartmentEntity;
import com.scj.api.pojo.vo.SerachDepartmentVo;

public interface ScjDepartmentEntityDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ScjDepartmentEntity record);

    int insertSelective(ScjDepartmentEntity record);

    ScjDepartmentEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ScjDepartmentEntity record);

    int updateByPrimaryKey(ScjDepartmentEntity record);
    
    //通过companyId查找部门id和部门名称
    List<SerachDepartmentVo> findByCompanyId(Integer companyId);
}