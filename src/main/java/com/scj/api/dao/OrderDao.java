package com.scj.api.dao;


import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.OderWithGoodsDO;
import com.scj.api.pojo.vo.OrderSearchVO;
import com.scj.api.pojo.vo.OrderVO;
import com.scj.common.base.BaseDao;
import com.scj.api.pojo.domain.OrderDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * <pre>
 * 订单表
 * </pre>
 * <small> 2018-08-22 22:48:35 | gacl</small>
 */
public interface OrderDao extends BaseDao<OrderDO> {
    OderWithGoodsDO selectOrderWithGoods(Integer orderId);

    List<OrderVO> selectByPage(Page<OrderVO> page, @Param("searchVO") OrderSearchVO searchVO);
}
