package com.scj.api.dao;


import com.scj.api.pojo.domain.CompSetmealVarietyDishesRelevanceDO;
import com.scj.common.base.BaseDao;
import java.util.List;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:20
 * @description 公司套餐与菜品关系
 */
public interface CompSetmealVarietyDishesRelevanceDao extends
    BaseDao<CompSetmealVarietyDishesRelevanceDO> {
  List<CompSetmealVarietyDishesRelevanceDO> listCompSetmealVarietyDishesRelevanceByComTcId(
      int comTcId);
}
