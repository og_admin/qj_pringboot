package com.scj.api.dao;

import com.scj.api.pojo.domain.AppUserDO;
import com.scj.common.base.BaseDao;

import java.util.List;

/**
 *
 */
public interface AppUserDao extends BaseDao<AppUserDO> {

    List<AppUserDO> selectUserInfo(String openid);
}
