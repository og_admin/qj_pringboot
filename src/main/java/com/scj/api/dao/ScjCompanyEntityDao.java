package com.scj.api.dao;

import com.scj.api.pojo.dto.ScjCompanyEntity;

public interface ScjCompanyEntityDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ScjCompanyEntity record);

    int insertSelective(ScjCompanyEntity record);

    ScjCompanyEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ScjCompanyEntity record);

    int updateByPrimaryKey(ScjCompanyEntity record);
}