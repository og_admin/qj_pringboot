package com.scj.api.dao;


import com.scj.api.pojo.domain.WechatPayListDO;
import com.scj.common.base.BaseDao;

/**
 *
 * @author 奋斗的牛牛
 * @date 2018/8/24 10:20
 * @description 微信支付交易记录
 */
public interface WechatPayListDao extends BaseDao<WechatPayListDO> {

}
