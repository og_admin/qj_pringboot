package com.scj.api.dao;


import com.scj.api.pojo.domain.OrderGoodsDO;
import com.scj.common.base.BaseDao;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-08-23 15:37:24 | gacl</small>
 */
public interface OrderGoodsDao extends BaseDao<OrderGoodsDO> {

}
