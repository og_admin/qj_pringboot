package com.scj.api.dao;


import com.scj.api.pojo.domain.CompanyDO;
import com.scj.api.pojo.vo.CompanyParamVO;
import com.scj.common.base.BaseDao;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface CompanyDao extends BaseDao<CompanyDO> {
    List<Map> selectComNameId(CompanyParamVO paras);
}
