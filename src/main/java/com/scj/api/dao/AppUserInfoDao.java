package com.scj.api.dao;

import java.util.List;

import com.scj.api.pojo.domain.AppUserDO;
import com.scj.api.pojo.vo.AppUserInfoVo;
import com.scj.common.base.BaseDao;

/**
 * @author 修复规范
 */
public interface AppUserInfoDao extends BaseDao<AppUserInfoVo> {
	/**
	 *
	 * @param openid
	 * @return
	 */
	List<AppUserInfoVo> selectUserInfo(String openid);
}
