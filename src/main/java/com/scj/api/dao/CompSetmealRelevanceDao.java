package com.scj.api.dao;


import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.scj.api.pojo.domain.CompSetmealRelevanceDO;
import com.scj.api.pojo.vo.CompSetmealRelevanceVO;
import com.scj.common.base.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
  * CompSetmealRelevanceDao mapper接口
  * @author wujiaxin
  * @email 747903314@qq.com
  * @date 2018/8/24 10:37
  */
public interface CompSetmealRelevanceDao extends BaseDao<CompSetmealRelevanceDO> {

    /**
     * 首页基础数据 分页查询
     */
    List<CompSetmealRelevanceVO> selectCompSetmealRelevanceListPage(Pagination page, @Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO);


    /**
     * 获取对应餐菜谱记录
     */
    List<CompSetmealRelevanceVO> getRecipeRecord(Pagination page, @Param("compSetmealRelevanceVO") CompSetmealRelevanceVO compSetmealRelevanceVO);
}
