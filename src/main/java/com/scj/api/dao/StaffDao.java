package com.scj.api.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.scj.api.pojo.domain.StaffDo;
import com.scj.staff.pojo.dto.StaffDTO;
import com.scj.staff.pojo.vo.StaffVo;

import java.util.List;
import java.util.Map;

/**
 * @author runnable@sina.cn
 */
public interface StaffDao extends BaseMapper<StaffDo> {
    /**
     * 根据参数查询员工信息
     * @param params 参数
     * @return 员工对象
     */
    StaffDo queryStaff(Map<String,Object> params);

    List<StaffVo> queryStaffList(StaffDTO staffDto);

    int queryStaffListCount(StaffDTO staffDto);
}
