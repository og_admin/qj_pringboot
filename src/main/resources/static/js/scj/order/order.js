var prefix = "/scj/order"
$(function () {
    load();
    loadCompany();
});
function loadCompany() {
    var html = '<option value="-1">全部</option>';
    $.ajax({
        url : "/scj/company/list",
        success : function(repData) {
            //加载数据
            var data=repData.data.records;
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
            }
            $("#company").append(html);
            // $("#company").chosen({
            //     maxHeight : 200
            // });

        }
    });
}

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method: 'get', // 服务器数据的请求方式 get or post
                url: prefix + "/order", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize: 'outline',
                toolbar: '#exampleToolbar',
                striped: true, // 设置为true会有隔行变色效果
                dataType: "json", // 服务器返回的数据类型
                pagination: true, // 设置为true会在底部显示分页条
                singleSelect: false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize: 10, // 如果设置了分页，每页数据条数
                pageNumber: 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns: false, // 是否显示内容下拉框（选择显示的列）
                sidePagination: "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType: "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams: function (params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber: params.pageNumber,
                        pageSize: params.pageSize,
                        status:$('#status').val(),
                        keyword:$('#keyword').val(),
                        comId:$('#company').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler: function (res) {
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns: [


                    {
                        field: 'orderNo',
                        align: 'center',
                        title: '订单编号'
                    },

                    {
                        field: 'companyName',
                        align: 'center',
                        title: '公司'
                    },

                    {
                        field: 'employeeName',
                        align: 'center',
                        title: '员工'
                    },

                    {
                        field: 'employeeCompanyName',
                        align: 'center',
                        title: '公司'
                    },

                    {
                        field: 'totalAmount',
                        align: 'center',
                        title: '总金额'
                    },

                    {
                        field: 'totalAmount',
                        align: 'center',
                        title: '应付'
                    },

                    {
                        field: "cost",
                        align: 'center',
                        title: '实付',
                        formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }
                    },
                    {
                        field: 'supplementFee',
                        align: 'center',
                        title: '餐补'
                        // formatter: function (value, row, index) {
                        //     return value.substring(0,10);
                        // }
                    },

                    {
                        field: 'orderTime',
                        align: 'center',
                        title: '下单时间',

                    },
                    {
                        field: 'monDay',
                        align: 'center',
                        title: '周一',
                        formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }
                    },
                    {
                        field: 'sunDay',
                        align: 'center',
                        title: '周日',
                        formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }
                    },
                    {
                        field: 'payStatus',
                        align: 'center',
                        title: '状态',

                        formatter: function (value, row, index) {
                            if(value==1){
                                return "已支付";
                            }
                            else{
                                return "未支付";
                            }
                        }
                    },

                    {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter: function (value, row, index) {
                            var e = '<a class="btn btn-sm btn-success" href="#" mce_href="#" title="查看详情" onclick="edit(\''
                                + row.id
                                + '\')">详情</a> ';
                            return e ;
                        }
                    }]
            });
}



function reLoad() {
    var area=$('#chosen-area option:selected').val();
    var city=$('#chosen-city option:selected').val();
    var province=$('#chosen-province option:selected').val();
    var opt = {
        query : {
            areaId : area,
            cityId : city,
            provinceId : province,
            name : $('#searchName').val()
            // contacts:$('#searchName').val(),
            // phone:$('#searchName').val()
        }
    };
    $('#exampleTable').bootstrapTable('refresh',opt);
}

