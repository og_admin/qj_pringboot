$().ready(function () {
    validateRule();
});

$.validator.setDefaults({
    submitHandler: function () {
        update();
    }
});

function update() {
    $.ajax({
        cache: true,
        type: "POST",
        url: "/scj/company/update",
        data: $('#signupForm').serialize(),// 你的formid
        async: false,
        error: function (request) {
            parent.layer.alert("Connection error");
        },
        success: function (data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                parent.reLoad();
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);

            } else {
                parent.layer.alert(data.msg)
            }

        }
    });

}

function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        ignore: [],
        rules: {
            name: {
                required: true,
                maxlength: 255
            },
            logo: {
                required: true,
                maxlength: 255
            },
            businessLicense: {
                required: true,
                maxlength: 255
            },
            startDate: {
                required: true
            },
            endDate: {
                required: true
            },
            contacts: {
                required: true,
                maxlength: 255
            },
            phone: {
                required: true,
                digits: true,
                minlength: 6,
                maxlength: 255
            },
            provinceId: {
                required: true,
                maxlength: 11
            },
            cityId: {
                required: true,
                maxlength: 11
            },
            areaId: {
                required: true,
                maxlength: 11
            },
            salesmanId: {
                required: true,
                maxlength: 11
            },
            mealSupplement: {
                required: true,
                number: true,
                maxlength: 10
            },
            comInfo: {
                required: true,
                maxlength: 512
            },
            address: {
                required: true,
                maxlength: 255
            },
            coordinate: {
                required: true,
                maxlength: 255
            }


        },
        messages: {
            name: {
                required: icon + "请输入公司名称"
            },
            logo: {
                required: icon + "请上传公司logo"
            },
            businessLicense: {
                required: icon + "请上传公司营业执照"
            },
            startDate: {
                required: icon + "请选择开始日期"
            },
            endDate: {
                required: icon + "请选择结束日期"
            },
            contacts: {
                required: icon + "请输入公司联系人"
            },
            phone: {
                required: icon + "请输入联系电话"
            },
            provinceId: {
                required: icon + "请选择省份"
            },
            cityId: {
                required: icon + "请选择城市"
            },
            areaId: {
                required: icon + "请选择区"
            },
            salesmanId: {
                required: icon + "请选择业务员"
            },
            mealSupplement: {
                required: icon + "请输入餐补"
            },
            comInfo: {
                required: icon + "请输入公司信息说明"
            },
            address: {
                required: icon + "请输入公司详细地址"
            },
            coordinate: {
                required: icon + "请输入公司坐标"
            }

        }
    });
}

//初始化
$(function () {
    var log = $("#logo").val();
    var businessLicense = $("#businessLicense").val();
    var salesmanId = $("#salesmanId2").val();
    var areaId = $("#areaId2").val();
    var provinceId = $("#provinceId2").val();
    var cityId = $("#cityId2").val();
    // var cityId = $("#cityId").val();
    // var areaId = $("#areaId").val();
    // var salesmanId = $("#salesmanId").val();
    $("#test1").html("");
    $("#test2").html("");
    $("#test1").css({
        "background-image": "url(" + log + ")",
        "background-size": "100% 100%"
    });
    $("#test2").css({
        "background-image": "url(" + businessLicense + ")",
        "background-size": "100% 100%"
    });


    //省份初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=1',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(provinceId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#provinceId").append(html);
            $("#provinceId").chosen({
                maxHeight: 200
            });

        }
    });

    //城市初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=110000',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(cityId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#cityId").append(html);
            $("#cityId").chosen({
                maxHeight: 200
            });

        }
    });

    //区初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=110100',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(areaId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#areaId").append(html);
            $("#areaId").chosen({
                maxHeight: 200
            });

        }
    });

    //业务员列表
    $.ajax({
        url: '/scj/slesman/getSlesman',
        success: function (data) {
            //加载数据
            var html = "";
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                if(salesmanId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].name + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
                }
            }
            $("#salesmanId").append(html);
            $("#salesmanId").chosen({
                maxHeight: 200
            });

        }
    });
});

//点击事件
$('#provinceId').on('change', function (e, params) {
    var city = $('#provinceId option:selected').val();
    var html = "";
    $("#cityId").html("");
    $("#areaId").html("");
    $("#areaId").append(html);
    $.ajax({
        url: '/scj/area/getByParentId?parentId=' + city,
        success: function (data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
            }
            $("#cityId").append(html);
            $("#cityId").chosen("destroy").init();
            $("#cityId").chosen({
                maxHeight: 200
            });
            $("#areaId").chosen("destroy").init();
            $("#areaId").chosen({
                maxHeight: 200
            });
        }
    });
});

$('#cityId').on('change', function (e, params) {
    var html = "";
    $("#areaId").html("");
    var area = $('#cityId option:selected').val();
    $.ajax({
        url: '/scj/area/getByParentId?parentId=' + area,
        success: function (data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
            }
            $("#areaId").append(html);
            $("#areaId").chosen("destroy").init();
            $("#areaId").chosen({
                maxHeight: 200
            });
        }
    });
});

//公司logo上传
layui.use('upload', function () {
    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem: '#test1', //绑定元素
        url: '/common/sysFile/upload', //上传接口
        size: 1000,
        accept: 'file',
        done: function (r) {
            layer.msg(r.msg);
            $("#test1").css({
                "background-image": "url(" + r.data + ")",
                "background-size": "100% 100%"
            });
            $("#logo").val(r.data);
        },
        error: function (r) {
            layer.msg(r.msg);
        }
    });
});

//营业执照上传
layui.use('upload', function () {
    var upload = layui.upload;
    //执行实例
    var uploadInst = upload.render({
        elem: '#test2', //绑定元素
        url: '/common/sysFile/upload', //上传接口
        size: 1000,
        accept: 'file',
        done: function (r) {
            layer.msg(r.msg);
            $("#test2").css({
                "background-image": "url(" + r.data + ")",
                "background-size": "100% 100%"
            });
            $("#businessLicense").val(r.data);
        },
        error: function (r) {
            layer.msg(r.msg);
        }
    });
});
