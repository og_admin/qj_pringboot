var prefix = "/scj/company"
$(function () {
    load();
});
function selectLoad() {
    var html = "";
    $.ajax({
        url : '/scj/area/getByParentId',
        success : function(data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
            }
            $("#chosen-province").append(html);
            $("#chosen-province").chosen({
                maxHeight : 200
            });

        }
    });
}

//点击事件
$('#chosen-province').on('change', function(e, params) {
    var city=$('#chosen-province option:selected').val();
    var html = "<option selected=\"selected\" disabled=\"disabled\"  style='display: none' value=''></option>";
    $("#chosen-city").html("");
    $("#chosen-area").html("");
    $("#chosen-area").append(html);
    $.ajax({
        url : '/scj/area/getByParentId?parentId='+city,
        success : function(data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
            }
            $("#chosen-city").append(html);
            $("#chosen-city").chosen("destroy").init();
            $("#chosen-city").chosen({
                maxHeight : 200
            });
            $("#chosen-area").chosen("destroy").init();
            $("#chosen-area").chosen({
                maxHeight : 200
            });
        }
    });

    var opt = {
        query : {
            provinceId : city,
            name : $('#searchName').val()
        }
    };
    $('#exampleTable').bootstrapTable('refresh', opt);
});

$('#chosen-city').on('change', function(e, params) {
    var html = "<option selected=\"selected\" disabled=\"disabled\"  style='display: none' value=''></option>";
    $("#chosen-area").html("");
    var area=$('#chosen-city option:selected').val();
    $.ajax({
        url : '/scj/area/getByParentId?parentId='+area,
        success : function(data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
            }
            $("#chosen-area").append(html);
            $("#chosen-area").chosen("destroy").init();
            $("#chosen-area").chosen({
                maxHeight : 200
            });
        }
    });

    $("#chosen-area").html("");
    var opt = {
        query : {
            cityId:area,
            name : $('#searchName').val()
        }
    };
    $('#exampleTable').bootstrapTable('refresh', opt);
});

$('#chosen-area').on('change', function(e, params) {
    var areaId=$('#chosen-area option:selected').val();

    var opt = {
        query : {
            areaId:areaId,
            name : $('#searchName').val()
        }
    };
    $('#exampleTable').bootstrapTable('refresh', opt);
});
function load() {
    selectLoad();
    $('#exampleTable')
        .bootstrapTable(
            {
                method: 'get', // 服务器数据的请求方式 get or post
                url: prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize: 'outline',
                toolbar: '#exampleToolbar',
                striped: true, // 设置为true会有隔行变色效果
                dataType: "json", // 服务器返回的数据类型
                pagination: true, // 设置为true会在底部显示分页条
                singleSelect: false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize: 10, // 如果设置了分页，每页数据条数
                pageNumber: 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns: false, // 是否显示内容下拉框（选择显示的列）
                sidePagination: "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParamsType: "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams: function (params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber: params.pageNumber,
                        pageSize: params.pageSize
                        // name:$('#searchName').val(),
                        // username:$('#searchName').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                responseHandler: function (res) {
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns: [
                    {
                        checkbox: true
                    },

                    {
                        field: 'logo',
                        align: 'center',
                        title: '公司logo',
                        formatter: function (value, row, index) {
                            var a="<image style='width: 100px;height: 100px;' src='"+value+"'></image>"
                            return a;
                        }
                    },

                    {
                        field: 'name',
                        align: 'center',
                        title: '名称'
                    },

                    {
                        field: 'businessLicense',
                        align: 'center',
                        title: '营业执照',
                        formatter: function (value, row, index) {
                            var a="<image  style='width: 100px;height: 100px;' src='"+value+"'></image>"
                            return a;
                        }
                    },

                    {
                        field: 'contacts',
                        align: 'center',
                        title: '公司联系人'
                    },

                    {
                        field: 'phone',
                        align: 'center',
                        title: '联系电话'
                    },

                    {
                        field: 'salesmanDetails',
                        align: 'center',
                        title: '业务员'
                    },

                    {
                        field: "startDate",
                        align: 'center',
                        title: '入驻日期',
                        formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }
                    },

                    {
                        field: 'endDate',
                        align: 'center',
                        title: '结束日期',
                        formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }
                    },

                    {
                        field: 'state',
                        align: 'center',
                        title: '状态',
                        formatter: function (value, row, index) {
                            if(value==1){
                                return "正常";
                            }
                            else{
                                return "暂停";
                            }
                        }
                    },
                    {
                        title: '操作',
                        field: 'id',
                        align: 'center',
                        formatter: function (value, row, index) {
                            var e = '<a class="btn btn-sm btn-success" href="#" mce_href="#" title="查看详情" onclick="detail(\''
                                + row.id
                                + '\')">查看详情</a> ';
                            var d = '<a class="btn btn-sm btn-success" href="#" mce_href="#" title="编辑信息" onclick="edit(\''
                                + row.id
                                + '\')">编辑信息</a> <br> <br>';
                            var f = '<a class="btn btn-sm btn-success" href="#" title="查看菜单"  mce_href="#" onclick="edit(\''
                                + row.id
                                + '\')">查看菜单</a> ';
                            var g = '<a class="btn btn-sm btn-success" href="#" title="查看订单"  mce_href="#" onclick="edit(\''
                                + row.id
                                + '\')">查看订单</a> ';
                            return e + d+f+g;
                        }
                    }]
            });
}



function reLoad() {
    var area=$('#chosen-area option:selected').val();
    var city=$('#chosen-city option:selected').val();
    var province=$('#chosen-province option:selected').val();
    var opt = {
        query : {
            areaId : area,
            cityId : city,
            provinceId : province,
            name : $('#searchName').val()
            // contacts:$('#searchName').val(),
            // phone:$('#searchName').val()
        }
    };
    $('#exampleTable').bootstrapTable('refresh',opt);
}

function add() {
    layer.open({
        type: 2,
        title: '增加',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['1100px', '720px'],
        content: prefix + '/add' // iframe的url
    });
}

function detail(id) {
    layer.open({
        type: 2,
        title: '编辑',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['1100px', '720px'],
        content: prefix + '/detail/' + id // iframe的url
    });
}

function edit(id) {
    layer.open({
        type: 2,
        title: '编辑',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['1100px', '720px'],
        content: prefix + '/edit/' + id // iframe的url
    });
}

function remove(id) {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消']
    }, function () {
        $.ajax({
            url: prefix + "/remove",
            type: "post",
            data: {
                'id': id
            },
            success: function (r) {
                if (r.code == 0) {
                    layer.msg(r.msg);
                    reLoad();
                } else {
                    layer.msg(r.msg);
                }
            }
        });
    })
}

//清空搜索项
function clearData() {
    $("#chosen-city").html("<option selected=\"selected\" disabled=\"disabled\"  style='display: none' value=''></option>");
    $("#chosen-area").html("<option selected=\"selected\" disabled=\"disabled\"  style='display: none' value=''></option>");
    $("#chosen-province").html("<option selected=\"selected\" disabled=\"disabled\"  style='display: none' value=''></option>");
    $("#chosen-area").chosen("destroy").init();
    $("#chosen-city").chosen("destroy").init();
    $("#chosen-province").chosen("destroy").init();
    $('#searchName').val("");
    selectLoad();
    reLoad();
}

function resetPwd(id) {
}

function batchRemove() {
    var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
    if (rows.length == 0) {
        layer.msg("请选择要删除的数据");
        return;
    }
    layer.confirm("确认要删除选中的'" + rows.length + "'条数据吗?", {
        btn: ['确定', '取消']
        // 按钮
    }, function () {
        var ids = new Array();
        // 遍历所有选择的行数据，取每条数据对应的ID
        $.each(rows, function (i, row) {
            ids[i] = row['id'];
        });
        $.ajax({
            type: 'POST',
            data: {
                "ids": ids
            },
            url: prefix + '/batchRemove',
            success: function (r) {
                if (r.code == 0) {
                    layer.msg(r.msg);
                    reLoad();
                } else {
                    layer.msg(r.msg);
                }
            }
        });
    }, function () {

    });
}