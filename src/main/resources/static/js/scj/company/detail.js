//初始化
$(function () {
    var log = $("#logo").val();
    var businessLicense = $("#businessLicense").val();
    var salesmanId = $("#salesmanId2").val();
    var areaId = $("#areaId2").val();
    var provinceId = $("#provinceId2").val();
    var cityId = $("#cityId2").val();
    $("#test1").html("");
    $("#test2").html("");
    $("#test1").css({
        "background-image": "url(" + log + ")",
        "background-size": "100% 100%"
    });
    $("#test2").css({
        "background-image": "url(" + businessLicense + ")",
        "background-size": "100% 100%"
    });


    //省份初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=1',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(provinceId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#provinceId").append(html);
            $("#provinceId").chosen({
                maxHeight: 200
            });

        }
    });

    //城市初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=110000',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(cityId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#cityId").append(html);
            $("#cityId").chosen({
                maxHeight: 200
            });

        }
    });

    //区初始化
    $.ajax({
        url: '/scj/area/getByParentId?parentId=110100',
        success: function (data) {
            //加载数据
            var html = "";
            for (var i = 0; i < data.length; i++) {
                if(areaId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].areaname + '</option>'
                }
            }
            $("#areaId").append(html);
            $("#areaId").chosen({
                maxHeight: 200
            });

        }
    });

    //业务员列表
    $.ajax({
        url: '/scj/slesman/getSlesman',
        success: function (data) {
            //加载数据
            var html = "";
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                if(salesmanId==data[i].id){
                    html += '<option selected="selected" value="' + data[i].id + '">' + data[i].name + '</option>'
                }
                else{
                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
                }
            }
            $("#salesmanId").append(html);
            $("#salesmanId").chosen({
                maxHeight: 200
            });

        }
    });
});