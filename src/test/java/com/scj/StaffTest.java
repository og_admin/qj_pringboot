package com.scj;

import com.scj.api.pojo.domain.StaffDo;
import com.scj.api.service.StaffService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StaffTest {

    @Autowired
    private StaffService service;

    @Test
    public void info() {
        Map<String, Object> params = new HashMap<>();
        params.put("name", "张三");
        StaffDo staffDo = service.queryInfo(params);
        System.err.println(staffDo);
    }
}